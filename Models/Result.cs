﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Collections;

namespace ECMToolbox.DocumentView.AutoPopulation.Models
{
    public class Result
    {
        private int total = 0;

        private object data;

        public object Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }
        public int Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }
        public Message Messages { get; set; } = new Message();

        public string Stack { get; set; }

        //public DBSession Session { get; set; } = new DBSession();
        public object Session { get; set; } = new object();

        private bool IsList(object o)
        {
            if (o == null) return false;
            return o is IList &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }
    }
}