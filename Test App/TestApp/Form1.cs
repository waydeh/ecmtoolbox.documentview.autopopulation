﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ECMToolbox.Demos.DocumentView.AutoPopulation.Classes;
using ECMToolbox.Demos.DocumentView.AutoPopulation.Models;
using ECMToolbox.Demos.DocumentView.AutoPopulation.Controllers;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //Diagnostics.DebugMsg("button1_Click starting");

            //Settings.LoadJsonSettings();

            //Diagnostics.DebugMsg("button1_Click ending");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Diagnostics.DebugMsg("button1_Click starting");
            try
            {
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();

                if (settings.autoPops.Length > 0)
                {
                    //-- set test values sample vendor auto pop
                    AutoPop autoPop = settings.autoPops[0];
                    autoPop.wfKeyFields[0].tableFieldValue = txtVendorNumber.Text;

                    AutoPopController autoPopController = new AutoPopController();
                    Result result = autoPopController.GetAutoPop(autoPop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
            Diagnostics.DebugMsg("button1_Click ending");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Diagnostics.DebugMsg("button2_Click starting");
            try
            {
                Settings settings = new Settings();
                settings.LoadJsonSettings();
                //settings.autoPops = Global.autoPops;

                if (settings.autoPops.Length > 0)
                {
                    //-- set test values sample invoice auto pop
                    AutoPop autoPop = settings.autoPops[1];
                    autoPop.wfKeyFields[0].tableFieldValue = txtInvoiceNumber.Text;
                    autoPop.wfKeyFields[1].tableFieldValue = txtInvoiceDate.Text;

                    Dbconnection dbConnection = settings.dbConnections.FirstOrDefault(x => x.dbConnName == autoPop.dbConnName);
                    if (dbConnection != null)
                    {
                        AutoPopController autoPopController = new AutoPopController();
                        Result result = autoPopController.GetAutoPop(autoPop);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
            Diagnostics.DebugMsg("button2_Click ending");
        }
    }
}
