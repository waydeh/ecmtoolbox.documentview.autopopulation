﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECMToolbox.Demos.DocumentView.AutoPopulation.Models;
using ECMToolbox.Demos.DocumentView.AutoPopulation.DataBase;
using ECMToolbox.Demos.DocumentView.AutoPopulation.Classes;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace ECMToolbox.Demos.DocumentView.AutoPopulation.Controllers
{
    public class AutoPopController
    {
        public Result GetAutoPop(AutoPop autoPop)
        {
            Result result = new Result();
            try
            {
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();

                Dbconnection dbConnection = settings.dbConnections.FirstOrDefault(x => x.dbConnName == autoPop.dbConnName);
                DataTable dt = GetAutoPopData(autoPop, dbConnection.ConnectionString());
                if (dt.Rows.Count == 0)
                {
                    result.Data = "";
                    result.Total = 0;
                }
                else
                {
                    List<FieldMapping> fieldMappings = LoadResult(autoPop, dt);
                    result.Data = fieldMappings;
                    result.Total = fieldMappings.Count;
                }
            }
            catch (Exception ex)
            {
                result.Messages.ErrorMessages.Add(ex.Message);
            }
            return result;
        }

        private List<FieldMapping> LoadResult(AutoPop autoPop, DataTable dt)
        {
            List<FieldMapping> fieldMappings = new List<FieldMapping>();
            for (int i = 0; i < autoPop.fieldMappings.Length; i++)
            {
                FieldMapping newFM = new FieldMapping();
                DataRow dr = dt.Rows[i];
                newFM.toWFFieldName = autoPop.fieldMappings[i].toWFFieldName;
                newFM.toWFFieldValue = dr[autoPop.fieldMappings[i].fromTableFieldName].ToString();
                newFM.fromTableFieldName = autoPop.fieldMappings[i].fromTableFieldName;
                fieldMappings.Add(newFM);
            }
            return fieldMappings;
        }

        private DataTable GetAutoPopData(AutoPop autoPop, string dbConn)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                conn.Open();
                using (SqlCommand com = new SqlCommand())
                {
                    //-- iterate through key fields
                    string sql = "select * from " + autoPop.dbTableName + " where ";
                    foreach (WFKeyField wfKeyField in autoPop.wfKeyFields)
                    {
                        sql = sql + wfKeyField.tableFieldName + " = @" + wfKeyField.tableFieldName;
                        if (wfKeyField.@operator != "")
                            sql = sql + " " + wfKeyField.@operator + " ";
                        com.Parameters.AddWithValue("@" + wfKeyField.tableFieldName, wfKeyField.tableFieldValue);
                    }
                    com.CommandText = sql;
                    com.Connection = conn;

                    SqlDataReader dr = com.ExecuteReader();
                    dt.Load(dr);
                }
                conn.Close();
            }
            return dt;
        }
    }
}