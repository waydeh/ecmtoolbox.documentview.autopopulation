﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ECMToolbox.Demos.DocumentView.AutoPopulation.Classes
{
    public class Diagnostics
    {
        public static void DebugMsg(string msg)
        {
            if (Global.testMode)
            {
                using (StreamWriter writer = new StreamWriter(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "_debug.txt"), true))
                {
                    string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    writer.WriteLine(now + " : " + msg);
                }
            }
        }
    }
}
