﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECMToolbox.Demos.DocumentView.AutoPopulation.Models
{
    public class Message
    {
        public List<string> SuccessMessages { get; set; } = new List<string>();
        public List<string> InfoMessages { get; set; } = new List<string>();
        public List<string> InfoPopupMessages { get; set; } = new List<string>();
        public List<string> FailureMessages { get; set; } = new List<string>();
        public List<string> ErrorMessages { get; set; } = new List<string>();

        public Message()
        {
        }

        public bool HasErrorMessages
        {
            get
            {
                if (ErrorMessages.Count == 0) return false;
                else return true;
            }
        }
        public bool HasNoErrors
        {
            get
            {
                if (ErrorMessages.Count == 0) return true;
                else return false;
            }
        }
    }


}