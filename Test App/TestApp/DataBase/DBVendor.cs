﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECMToolbox.Demos.DocumentView.AutoPopulation.DataBase
{
    [Table("Vendors")]
    public class DBVendor
    {
        public string VendorName { get; set; }
        public string VendorNumber { get; set; }
    }
}