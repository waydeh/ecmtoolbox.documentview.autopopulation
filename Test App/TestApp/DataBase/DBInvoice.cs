﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECMToolbox.Demos.DocumentView.AutoPopulation.DataBase
{
    [Table("Invoices")]
    public class DBInvoice
    {
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal InoviceAmount { get; set; }
    }
}
