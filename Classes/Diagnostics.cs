﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ECMToolbox.DocumentView.AutoPopulation.Classes
{
    public class Diagnostics
    {
        public static void DebugMsg(string msg)
        {
            //if (Global.testMode)
            //{
                string path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/bin"), System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "_debug.txt");
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    writer.WriteLine(now + " : " + msg);
                }
            //}
        }
    }
}
