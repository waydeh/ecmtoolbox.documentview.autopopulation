﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace ECMToolbox.DocumentView.AutoPopulation.Classes
{
    public static class Global
    {
        public static bool testMode { get; set; }
    }

    public class AXUDL
    {
        public string wfFieldName { get; set; }
        public string dbConnName { get; set; }
        public string axAppName { get; set; }
        public string axFieldName { get; set; }
    }

    public class AutoPop
    {
        public string wfFieldName { get; set; }
        public string dbConnName { get; set; }
        public string dbTableName { get; set; }
        public WFKeyField[] wfKeyFields { get; set; }
        public FieldMapping[] fieldMappings { get; set; }
    }
    public class WFKeyField
    {
        public string wfFieldName { get; set; }
        public string tableFieldName { get; set; }
        public string tableFieldValue { get; set; }
        public string @operator { get; set; }
    }
    public class FieldMapping
    {
        public string fromTableFieldName { get; set; }
        public string toWFFieldName { get; set; }
        public string toWFFieldValue { get; set; }
    }

    public class Dbconnection
    {
        public string dbConnName { get; set; }
        public string dataSource { get; set; }
        public string initialCatalog { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public bool passEncrypted { get; set; }

        public string ConnectionString()
        {
            return "Data Source=" + dataSource + ";Initial Catalog=" + initialCatalog + ";User ID=" + userName + ";Password=" + Encryption.PasswordDecryptReversible(password, Hash.hash);
        }
    }

    public class Hash
    {
        public static string hash { get; } = "Q#qrg3#%8";
    }

    public class Settings
    {
        private string SETTINGS_FILE_NAME = "";
        public bool justEncryptDBStrings { get; set; }
        public Dbconnection[] dbConnections { get; set; }
        public bool testMode { get; set; }
        public string submitButton { get; set; }
        public AXUDL[] axUDLs { get; set; }
        public AutoPop[] autoPops { get; set; }

        public Settings LoadJsonSettings()
        {
            Diagnostics.DebugMsg("Settings.LoadJsonSettings starting...");
            
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            SETTINGS_FILE_NAME = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/bin"), thisAssembly.GetName().Name + "_Settings.txt");
            Diagnostics.DebugMsg("Settings.LoadJsonSettings settings file : " + SETTINGS_FILE_NAME);

            bool updateSettingsFile = false;
            Settings settings;
            using (StreamReader r = new StreamReader(SETTINGS_FILE_NAME))
            {
                string json = r.ReadToEnd();
                settings = JsonConvert.DeserializeObject<Settings>(json);

                foreach (Dbconnection dbConnection in settings.dbConnections)
                {
                    if (!dbConnection.passEncrypted)
                    {
                        dbConnection.password = Encryption.PasswordEncryptReversible(dbConnection.password, Hash.hash);
                        dbConnection.passEncrypted = true;
                        updateSettingsFile = true;
                    }
                    //-- update the autopop dbconn
                    //autoPops.SingleOrDefault(item => item.dbConnName == dbConnection.dbConnName).dbConn = dbConnection.ConnectionString();
                }
                Global.testMode = settings.testMode;
            }
            if (settings.autoPops == null)
            {
                Diagnostics.DebugMsg("Settings.LoadJsonSettings autopops is null...");
            }
            else
            {
                Diagnostics.DebugMsg("Settings.LoadJsonSettings autopops is not null...");
                Diagnostics.DebugMsg("Settings.LoadJsonSettings autopops count : " + settings.autoPops.Count().ToString());
                Diagnostics.DebugMsg("Settings.LoadJsonSettings autopops len : " + settings.autoPops.Length.ToString());
            }
            if (settings.axUDLs == null)
            {
                Diagnostics.DebugMsg("Settings.LoadJsonSettings axUDLs is null...");
            }
            else
            {
                Diagnostics.DebugMsg("Settings.LoadJsonSettings axUDLs is not null...");
                Diagnostics.DebugMsg("Settings.LoadJsonSettings axUDLs count : " + settings.axUDLs.Count().ToString());
                Diagnostics.DebugMsg("Settings.LoadJsonSettings axUDLs len : " + settings.axUDLs.Length.ToString());
            }
            if (updateSettingsFile)
            {
                //File.Move(settingsFileName, settingsFileName + Guid.NewGuid().ToString() + ".old");
                File.Delete(SETTINGS_FILE_NAME);
                using (StreamWriter file = File.CreateText(SETTINGS_FILE_NAME))
                {
                    file.Write(JValue.Parse(JsonConvert.SerializeObject(settings)).ToString(Formatting.Indented));
                }
            }
            Diagnostics.DebugMsg("Settings.LoadJsonSettings ending...");
            return settings;
        }
    }
}
