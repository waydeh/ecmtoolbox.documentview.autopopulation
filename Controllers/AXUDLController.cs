﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using CASO.AX.Net;
using System.Configuration;
using ECMToolbox.DocumentView.AutoPopulation.Models;
using ECMToolbox.DocumentView.AutoPopulation.Classes;

namespace ECMToolbox.DocumentView.AutoPopulation.Controllers
{
    public class AXUDLController : ApiController
    {
        [Route("AXUDL/GetAll")]
        [HttpPost]
        public Result GetAll()
        {
            Diagnostics.DebugMsg("AXUDLController.GetAll : starting...");
            Result result = new Result();
            try
            {
                //-- load the settings
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();
                result.Data = settings.axUDLs;
                result.Total = settings.axUDLs.Length;
            }
            catch (Exception ex)
            {
                Diagnostics.DebugMsg("Error in AXUDLController.GetAll : " + ex.Message);
                result.Messages.ErrorMessages.Add(ex.Message);
            }
            Diagnostics.DebugMsg("AXUDLController.GetAll : ending...");
            return result;
        }


        public class NameValuePair
        {
            public string Name { get; set; } = "";
            public string Value { get; set; } = "";
        }

        public class AXUDL
        {
            public string wfFieldName { get; set; }
            public string dbConnName { get; set; }
            public string axAppName { get; set; }
            public string axFieldName { get; set; }
        }
        [Route("AXUDL/GetUDL")]
        [HttpPost]
        public Result GetUDL([FromBody] AXUDL axUDL)
        {
            Diagnostics.DebugMsg("AXUDLController.GetUDL : starting...");
            Result result = new Result();
            try
            {
                List<NameValuePair> ReturnValues = new List<NameValuePair>();

                //-- load the settings to get db conn string
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();
                Dbconnection dbConnection = settings.dbConnections.FirstOrDefault(x => x.dbConnName == axUDL.dbConnName);

                ApplicationXtender AX = new ApplicationXtender(dbConnection.ConnectionString());
                AXApplication APP = AX.Applications[axUDL.axAppName];
                List<string> values = APP.Fields[axUDL.axFieldName].UserDefinedListValues;

                foreach (string value in values)
                {
                    NameValuePair returnValue = new NameValuePair();
                    returnValue.Value = value;
                    returnValue.Name = value;
                    ReturnValues.Add(returnValue);
                }

                result.Data = ReturnValues;
                result.Total = ReturnValues.Count();
            }
            catch (Exception exLogin)
            {
                result.Messages.ErrorMessages.Add(exLogin.Message);
            }
            Diagnostics.DebugMsg("AXUDLController.Get : ending...");
            return result;
        }
    }
}