﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECMToolbox.DocumentView.AutoPopulation.Models;
using ECMToolbox.DocumentView.AutoPopulation.Classes;
using System.Web.Http;

namespace ECMToolbox.DocumentView.AutoPopulation.Controllers
{
    public class SubmitButtonController : ApiController
    {
        [Route("SubmitButton/Get")]
        [HttpPost]
        public Result Get()
        {
            Diagnostics.DebugMsg("SubmitButton.Get : starting...");
            Result result = new Result();
            try
            {
                //-- load the settings
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();
                result.Data = settings.submitButton;
                result.Total = 1;
            }
            catch (Exception ex)
            {
                Diagnostics.DebugMsg("Error in SubmitButton.Get : " + ex.Message);
                result.Messages.ErrorMessages.Add(ex.Message);
            }
            Diagnostics.DebugMsg("SubmitButton.Get : ending...");
            return result;
        }
    }
}