﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECMToolbox.DocumentView.AutoPopulation.Models;
using ECMToolbox.DocumentView.AutoPopulation.Classes;
using System.Web.Http;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace ECMToolbox.DocumentView.AutoPopulation.Controllers
{
    public class AutoPopController : ApiController
    {
        [Route("AutoPop/GetAll")]
        [HttpPost]
        public Result GetAll()
        {
            Diagnostics.DebugMsg("AutoPopController.GetAll : starting...");
            Result result = new Result();
            try
            {
                //-- load the settings
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();
                result.Data = settings.autoPops;
                result.Total = settings.autoPops.Length;
            }
            catch (Exception ex)
            {
                Diagnostics.DebugMsg("Error in AutoPopController.GetAll : " + ex.Message);
                result.Messages.ErrorMessages.Add(ex.Message);
            }
            Diagnostics.DebugMsg("AutoPopController.GetAll : ending...");
            return result;
        }

        [Route("AutoPop/GetAutoPop")]
        [HttpPost]
        public Result GetAutoPop([FromBody] AutoPop autoPop)
        {
            Diagnostics.DebugMsg("GetAutoPop starting...");
            Result result = new Result();
            try
            {
                Settings settings = new Settings();
                settings = settings.LoadJsonSettings();

                Diagnostics.DebugMsg("GetAutoPop loaded settings...");

                Dbconnection dbConnection = settings.dbConnections.FirstOrDefault(x => x.dbConnName == autoPop.dbConnName);
                DataTable dt = GetAutoPopData(autoPop, dbConnection.ConnectionString());
                Diagnostics.DebugMsg("GetAutoPop dt.Rows.Count : " + dt.Rows.Count.ToString());

                if (dt.Rows.Count == 0)
                {
                    result.Data = "";
                    result.Total = 0;
                    Diagnostics.DebugMsg("GetAutoPop no recs found...");
                }
                else
                {
                    if (dt.Rows.Count > 1)
                    {
                        //-- return error
                        Diagnostics.DebugMsg("GetAutoPop rec found : " + dt.Rows.Count.ToString() + " : returning error");
                        result.Messages.ErrorMessages.Add("Match n Merge found more than one matching record.");
                    }
                    else
                    {
                        List<FieldMapping> fieldMappings = LoadResult(autoPop, dt);
                        result.Data = fieldMappings;
                        result.Total = fieldMappings.Count;
                        Diagnostics.DebugMsg("GetAutoPop Total : " + fieldMappings.Count.ToString());
                    }
                }
                Diagnostics.DebugMsg("GetAutoPop ending...");
            }
            catch (Exception ex)
            {
                result.Messages.ErrorMessages.Add(ex.Message);
            }
            return result;
        }

        private List<FieldMapping> LoadResult(AutoPop autoPop, DataTable dt)
        {
            Diagnostics.DebugMsg("LoadResult starting...");
            List<FieldMapping> fieldMappings = new List<FieldMapping>();
            Diagnostics.DebugMsg("LoadResult autoPop.Length : " + autoPop.fieldMappings.Length.ToString());
            Diagnostics.DebugMsg("LoadResult autoPop.count : " + autoPop.fieldMappings.Count().ToString());

            for (int i = 0; i < autoPop.fieldMappings.Count(); i++)
            {
                Diagnostics.DebugMsg("LoadResult i : " + i.ToString());
                FieldMapping newFM = new FieldMapping();
                DataRow dr = dt.Rows[0];
                newFM.toWFFieldName = autoPop.fieldMappings[i].toWFFieldName;
                newFM.toWFFieldValue = dr[autoPop.fieldMappings[i].fromTableFieldName].ToString();
                newFM.fromTableFieldName = autoPop.fieldMappings[i].fromTableFieldName;
                fieldMappings.Add(newFM);
            }
            Diagnostics.DebugMsg("LoadResult fieldMappings.Count : " + fieldMappings.Count());
            Diagnostics.DebugMsg("LoadResult ending...");
            return fieldMappings;
        }

        private DataTable GetAutoPopData(AutoPop autoPop, string dbConn)
        {
            Diagnostics.DebugMsg("GetAutoPopData starting...");
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(dbConn))
            {
                conn.Open();
                using (SqlCommand com = new SqlCommand())
                {
                    //-- iterate through key fields
                    string sql = "select * from " + autoPop.dbTableName + " where ";
                    foreach (WFKeyField wfKeyField in autoPop.wfKeyFields)
                    {
                        sql = sql + wfKeyField.tableFieldName + " = @" + wfKeyField.tableFieldName;
                        if (wfKeyField.@operator != "")
                            sql = sql + " " + wfKeyField.@operator + " ";
                        com.Parameters.AddWithValue("@" + wfKeyField.tableFieldName, wfKeyField.tableFieldValue);
                        Diagnostics.DebugMsg("GetAutoPopData wfKeyField.tableFieldName : " + wfKeyField.tableFieldName + " : wfKeyField.tableFieldValue : " + wfKeyField.tableFieldValue);
                    }
                    com.CommandText = sql;
                    Diagnostics.DebugMsg("GetAutoPopData sql : " + sql);
                    com.Connection = conn;

                    SqlDataReader dr = com.ExecuteReader();
                    dt.Load(dr);
                }
                conn.Close();
            }
            Diagnostics.DebugMsg("GetAutoPopData rec count : " + dt.Rows.Count.ToString());
            Diagnostics.DebugMsg("GetAutoPopData ending...");
            return dt;
        }
    }
}