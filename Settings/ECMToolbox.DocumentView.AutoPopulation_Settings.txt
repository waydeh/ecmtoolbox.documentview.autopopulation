﻿{
  "justEncryptDBStrings": false,
  "dbConnections": [
    {
      "dbConnName": "AutoPopExample",
      "dataSource": "trumbulldev",
      "initialCatalog": "ECMToolbox_AutoPop_Demo",
      "userName": "sysop",
      "password": "X$cm2016",
      "passEncrypted": false
    }
  ],
  "testMode": true,
  "submitButton" : "Go To",
  "axUDLs": [
	{
		"wfFieldName": "Invoice Number",
        "dbConnName": "AutoPopExample",
		"axAppName" : "AUTOPOPDEMO", 
		"axFieldName" : "INVOICE NUMBER"
	}
  ],
  "autoPops": [
    {
      "wfFieldName": "Vendor Number",
      "dbConnName": "AutoPopExample",
      "dbTableName": "Vendors",
      "wfKeyFields": [
        {
          "wfFieldName": "Vendor Number",
          "tableFieldName": "VendorNumber",
          "tableFieldValue": "",
          "operator": ""
        }
      ],
      "fieldMappings": [
        {
          "fromTableFieldName": "VendorName",
          "toWFFieldName": "Vendor Name",
          "toWFFieldValue": ""
        }
      ]
    },
    {
      "wfFieldName": "Invoice Number",
      "dbConnName": "AutoPopExample",
      "dbTableName": "Invoices",
      "wfKeyFields": [
        {
          "wfFieldName": "Invoice Number",
          "tableFieldName": "InvoiceNumber",
          "tableFieldValue": "",
          "operator": "and"
        },
        {
          "wfFieldName": "Invoice Date",
          "tableFieldName": "InvoiceDate",
          "tableFieldValue": "",
          "operator": ""
        }
      ],
      "fieldMappings": [
        {
          "fromTableFieldName": "InvoiceAmount",
          "toWFFieldName": "Invoice Amount",
          "toWFFieldValue": ""
        }
      ]
    }
  ]
}