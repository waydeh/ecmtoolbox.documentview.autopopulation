﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Data.Entity;

namespace ECMToolbox.Demos.DocumentView.AutoPopulation.DataBase
{
    public class DBContext : DbContext
    {
        public DBContext(DbConnection conn) : base(conn, true)
        {

        }

        public DBContext(string ConnectionString) : base(ConnectionString)
        {

        }

        //public DBContext() : base("name=MemberCareDemo")
        //{

        //}
    }
}