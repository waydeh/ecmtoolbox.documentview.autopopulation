﻿
function ECMTRegisterListBoxes() {
    var listBoxes = $('.listbox');
    for (var i = 0; i < listBoxes.length; i = i + 1) {
        $(listBoxes[i]).delegate('div', 'click', function (event) {
            var target = event.currentTarget;
            $(target).parent().children().removeClass('selected');
            $(target).addClass('selected');
        });
    }    
}

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function GetParentPage() {
    var parent_page = null;
    try {
        parent_page = window.parent;
    }
    catch (e) { }
    if (parent_page == null) {
        try {
            parent_page = window.top;
        }
        catch (e) { }
    }
    return parent_page;
}

//----------------------------------------------
//-- notifier object
//----------------------------------------------
//--
function notifier() {
    this.messages = null;

    this.notificationType = new notificationTypes;

    this.iconType = new iconTypes;
    this.attentionType = new attentionTypes;
}

notifier.prototype.showAlert = function (title, message, notificationType, attentionType) {

    var notificationMessage = '';

    //-- is message plural
    if (message !== null && typeof message === 'object') {

        //-- message is a list of messages
        $.each(message, function (i, item) {
            if (notificationMessage == "") {
                notificationMessage = item;
            }
            else {
                notificationMessage = notificationMessage + "\r\f" + item;
            }
        });
    }
    else {
        notificationMessage = message;
    }

    ECMNotification(notificationMessage, notificationType);
    ////-- which icon to display
    //var icon = '';
    //var attention = '';
    //switch (notificationType) {
    //    case this.notificationType.failure:
    //        icon = this.iconType.failure;
    //        attention = this.attentionType.rubberBand;
    //        break;
    //    case this.notificationType.error:
    //        icon = this.iconType.error;
    //        attention = this.attentionType.rubberBand;
    //        break;
    //    case this.notificationType.info:
    //        icon = this.iconType.info;
    //        attention = this.attentionType.swing;
    //        break;
    //    case this.notificationType.success:
    //        icon = this.iconType.success;
    //        attention = this.attentionType.tada;
    //        break;
    //}

    //var notification = new PNotify({
    //    title: title,
    //    text: message,
    //    //styling: 'fontawesome',
    //    icon: icon,
    //    type: notificationType,
    //    //hide: false,
    //    buttons: {
    //        closer: true
    //    }
    //});

    ////-- do we have an attention parm
    //if (attentionType !== null && typeof attentionType === 'string') {
    //    notification.attention(attentionType);
    //}
    //else {
    //    //-- default
    //    notification.attention(attention);
    //}
}
//--
//----------------------------------------------

//----------------------------------------------
//-- notification types
//----------------------------------------------
//--
function notificationTypes() {
    this.success = 0;
    this.info = 1;
    this.failure = 2;
    this.error = 3;
    this.infoPopup = 4;
}
var _notificationTypes = new notificationTypes;
//--
//----------------------------------------------

//----------------------------------------------
//-- icon types
//----------------------------------------------
//--
function iconTypes() {
    this.info = 'icon-info22';
    this.error = 'icon-blocked';
    this.success = 'icon-checkmark3';
}
//--
//----------------------------------------------


//----------------------------------------------
//-- attention types
//----------------------------------------------
//--
function attentionTypes() {
    this.bounce = 'bounce';
    this.flash = 'flash';
    this.pulse = 'pulse';
    this.rubberBand = 'rubberBand';
    this.shake = 'shake';
    this.swing = 'swing';
    this.tada = 'tada';
    this.wobble = 'wobble';
    this.jello = 'jello';
}
//--
//----------------------------------------------


var notificationCounter = 0;
function ECMNotification(sMessage, iType, sStack) {
    try{
        if (sMessage == "Your Session is Invalid or Has Timed Out.  Please Login Again.") {
            try{
                try{
                    window.parent.SessionTimedOut();
                }
                catch(ex){
                    SessionTimedOut();
                }
            }
            catch(e){
            }
        }
    }
    catch(exc){}

    if (iType === undefined || iType == null || iType < _notificationTypes.success || iType > _notificationTypes.infoPopup) iType = _notificationTypes.info;

    //-- notify type
    if (iType == _notificationTypes.success || iType == _notificationTypes.info || iType == _notificationTypes.failure) {
        if (notificationCounter == 0) {
            $(document.body).append('<div id="divNotification" class="notification-container"></div>');
        }
        notificationCounter = notificationCounter + 1;
        var item = $('<div class="notification-item ' + (iType == _notificationTypes.success ? 'notification-success' : (iType == _notificationTypes.info ? 'notification-info' : 'notification-failure')) + '" style="display:none;">' + HtmlEncode(sMessage) + '</div>');
        $('#divNotification').append(item);
        item.fadeIn('slow', function () {
            $(this).delay(5000).fadeOut('slow', function () { 
                $(this).remove(); 
                notificationCounter = notificationCounter - 1; 
                if (notificationCounter == 0) $('#divNotification').remove();
            });
        });        
    }

    //-- error popup 
    if (iType == _notificationTypes.error) {
        //$(document.body).append('<div id="err_window_modal" class="ecmwindowmodal" style="z-index:9997 !important;"></div>');
        //var err_Window = $('<div id="divErrorWindow" class="error-window closed"></div>');
        //var err_header = $('<div><i class="fa fa-exclamation-triangle"></i>Error<div>');
        //var err_message = $('<div>' + HtmlEncode(sMessage) + '<div>');
        //var err_buttonsdiv = $('<div><button class="btn btn-primary" style="float:right;" onclick="err_Close_Window(this);">Close</button><div>');
        
        //$(document.body).append(err_Window);
        //err_Window.append(err_header);
        //err_Window.append(err_message);
        //err_Window.append(err_buttonsdiv);
        //if (!(sStack === undefined || sStack == null || sStack == '')) {
        //    var err_stack = $('<div>' + HtmlEncode(sStack) + '<div>');
        //    var err_show_hide_stack_btn = $('<button class="btn btn-default" style="float:left;" onclick="err_Show_Stack(this);">Show Details</button>');
        //    err_Window.append(err_stack);
        //    err_buttonsdiv.append(err_show_hide_stack_btn);
        //}
        var modalDiv = $('<div id="err_window_modal" class="ecmwindowmodal" style="z-index:9996 !important;"></div>');
        //The blocking iframe is a transparent iframe that will block out activexcontrols underneith it on the z axis
        var backgroundblockingiframe = $('<iframe id="modalBlockingIframe" style="left: 0px; position: absolute; top: 0px; width:100%; height:100%; z-index:9997;" src="about:blank" frameBorder="0" scrolling="no"></iframe>')
        backgroundblockingiframe.css('filter', 'progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)');
        $(modalDiv).append(backgroundblockingiframe)
        $(document.body).append(modalDiv);
        var err_Window = $('<div id="divErrorWindow" class="error-window closed"></div>');
        var err_header = $('<div><i class="fa fa-exclamation-triangle"></i>Error<div>');
        var err_message = $('<div>' + HtmlEncode(sMessage) + '<div>');
        var err_buttonsdiv = $('<div><button class="btn btn-primary" style="float:right;" onclick="err_Close_Window(this);">Close</button><div>');

        $(document.body).append(err_Window);
        err_Window.append(err_header);
        err_Window.append(err_message);
        err_Window.append(err_buttonsdiv);
        if (!(sStack === undefined || sStack == null || sStack == '')) {
            var err_stack = $('<div>' + HtmlEncode(sStack) + '<div>');
            var err_show_hide_stack_btn = $('<button class="btn btn-default" style="float:left;" onclick="err_Show_Stack(this);">Show Details</button>');
            err_Window.append(err_stack);
            err_buttonsdiv.append(err_show_hide_stack_btn);
        }
    }

    //-- info popup type
    if (iType == _notificationTypes.infoPopup) {
        //$(document.body).append('<div id="err_window_modal" class="ecmwindowmodal" style="z-index:9997 !important;"></div>');
        //var err_Window = $('<div id="divErrorWindow" class="error-window closed"></div>');
        //var err_header = $('<div style="color:#dbad08 !important"><i class="fa fa-exclamation-circle"></i>Item cannot be edited<div>');
        //var err_message = $('<div>' + sMessage + '<div>');
        //var err_buttonsdiv = $('<div><button class="btn btn-primary" style="float:right;" onclick="err_Close_Window(this);">Close</button><div>');

        //$(document.body).append(err_Window);
        //err_Window.append(err_header);
        //err_Window.append('<div>' + sMessage.replace(/\r\n/g, '<br /><br />') + '<div>');
        //err_Window.append(err_buttonsdiv);
        var modalDiv = $('<div id="err_window_modal" class="ecmwindowmodal" style="z-index:9996 !important;"></div>');
        //The blocking iframe is a transparent iframe that will block out activexcontrols underneith it on the z axis
        var backgroundblockingiframe = $('<iframe id="modalBlockingIframe" style="left: 0px; position: absolute; top: 0px; width:100%; height:100%; z-index:9997;" src="about:blank" frameBorder="0" scrolling="no"></iframe>')
        backgroundblockingiframe.css('filter', 'progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)');
        $(modalDiv).append(backgroundblockingiframe)
        $(document.body).append(modalDiv);
        var err_Window = $('<div id="divErrorWindow" class="error-window closed"></div>');
        var err_header = $('<div style="color: #337ab7 !important;"><i class="fa fa-question-circle"></i>Oops...<div>');
        var err_message = $('<div><div>');
        err_message.html(sMessage);
        var err_buttonsdiv = $('<div><button class="btn btn-primary" style="float:right;" onclick="err_Close_Window(this);">Close</button><div>');

        $(document.body).append(err_Window);
        err_Window.append(err_header);
        err_Window.append(err_message);
        err_Window.append(err_buttonsdiv);
        if (!(sStack === undefined || sStack == null || sStack == '')) {
            var err_stack = $('<div>' + HtmlEncode(sStack) + '<div>');
            var err_show_hide_stack_btn = $('<button class="btn btn-default" style="float:left;" onclick="err_Show_Stack(this);">Show Details</button>');
            err_Window.append(err_stack);
            err_buttonsdiv.append(err_show_hide_stack_btn);
        }
    }

    //if (iType == 4) {
    //    $(document.body).append('<div id="err_window_modal" class="ecmwindowmodal" style="z-index:9997 !important;"></div>');
    //    var err_Window = $('<div id="divErrorWindow" class="error-window closed"></div>');
    //    var err_header = $('<div style="color: #337ab7 !important;"><i class="fa fa-question-circle"></i>Oops...<div>');
    //    var err_message = $('<div><div>');
    //    err_message.html(sMessage);
    //    var err_buttonsdiv = $('<div><button class="btn btn-primary" style="float:right;" onclick="err_Close_Window(this);">Close</button><div>');

    //    $(document.body).append(err_Window);
    //    err_Window.append(err_header);
    //    err_Window.append(err_message);
    //    err_Window.append(err_buttonsdiv);
    //    if (!(sStack === undefined || sStack == null || sStack == '')) {
    //        var err_stack = $('<div>' + HtmlEncode(sStack) + '<div>');
    //        var err_show_hide_stack_btn = $('<button class="btn btn-default" style="float:left;" onclick="err_Show_Stack(this);">Show Details</button>');
    //        err_Window.append(err_stack);
    //        err_buttonsdiv.append(err_show_hide_stack_btn);
    //    }
    //}

}
function err_Show_Stack(button) {
    var btnText = $(button).text();
    if (btnText == 'Show Details') {
        $(button).text('Hide Details');
        $(button).parent().parent().removeClass('closed');
        $(button).parent().parent().addClass('open');
    }
    else {
        $(button).text('Show Details');
        $(button).parent().parent().removeClass('open');
        $(button).parent().parent().addClass('closed');
    }
}

function err_Close_Window(button) {
    $('#err_window_modal').remove();
    $(button).parent().parent().remove();
}

function HtmlEncode(value) {
    return $('<div/>').text(value).html();
}

function HtmlDecode(value) {
    return $('<div/>').html(value).text();
}

function ShowWaitSpinner(sMessage, divTarget) {
    var waitDiv = $('<div id="divWait" style="left:0px; right:0px; top:0px; bottom:0px; display:none;"><div>');
    var spinner = $('<div class="spinnerBox" style="z-index:9902"><div><div><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div><div>' + HtmlEncode(sMessage) + '</div></div></div>');
    waitDiv.append('<div id="divSpinnerModal" class="spinnerModal" style="z-index:9901;"></div>')
    waitDiv.append(spinner);
    if (divTarget == null) {
        waitDiv.css('position', 'fixed');
        $(document.body).append(waitDiv);
    }
    else {
        waitDiv.css('position', 'absolute');
        $('#'+divTarget).append(waitDiv);
    }
    waitDiv.fadeIn(300);
}

function HideWaitSpinner(divTarget) {
    if (divTarget == null) {
        var targetwaitdiv = $('body > #divWait');
        targetwaitdiv.children().fadeOut(300, function () { targetwaitdiv.remove(); });
    }
    else {
        var targetwaitdiv = $('#'+divTarget+' > #divWait');
        targetwaitdiv.children().fadeOut(300, function () { targetwaitdiv.remove(); });
    }    
}

function FormatStandardDate(d, format) {
    var month = d.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day = d.getDate();
    day = (day < 10 ? "0" : "") + day;
    var year = d.getFullYear().toString();
    var hour = d.getHours();    
    var minute = d.getMinutes();
    minute = (minute < 10 ? "0" : "") + minute;    
    var mod = "AM";
    if(hour >= 12){
        mod = "PM";
        hour = hour - 12;
    }
    if (hour == 0) hour = 12;
    hour = (hour < 10 ? "0" : "") + hour
    if (format == null) {
        return month + "/" + day + "/" + year + " " + hour + ":" + minute + " " + mod;
    }
    else {
        format = format.replace('MM', month);
        format = format.replace('dd', day);
        format = format.replace('yyyy', year);
        format = format.replace('yy', year.substring(2));
        format = format.replace('hh', hour);
        format = format.replace('mm', minute);
        format = format.replace('tt', mod);
        return format;
    }
}

function ValidateNumber(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 109, 189]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function ValidateInteger(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 109, 189]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function KendoGridRemoveAltColumns() {
    $('tr').removeClass('k-alt');
}

function printer() {
    this.lineCounter = 0;
    this.pageBreakAfter = 9999;
    this.content = '';
    this.cssPaths = [];
    this.header = '';
    this.footer = '';
}
printer.prototype.pagination = function () {
    this.lineCounter = this.lineCounter + 1;
    if (this.lineCounter > this.pageBreakAfter) {
        this.lineCounter = 0;
        return true;
    }
    else {
        return false;
    }
}
printer.prototype.empty = function () {
    this.lineCounter = 0;
    this.content = '';
    this.cssPaths = [];
}
printer.prototype.print = function () {

    var w = window.open();
    w.document.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + "\n");
    w.document.write("<html>" + "\n");
    w.document.write("<head>" + "\n");
    w.document.write("<meta charset='utf-8'>" + "\n");

    // Add the style sheets
    w.document.write('<style type="text/css">' + '\n');
    w.document.write('@media print {' + '\n');
    w.document.write('thead { display: table-header-group; }' + '\n');
    w.document.write('tfoot { display: table-footer-group; }' + '\n');
    w.document.write('.page-break	{ display: block; page-break-before: always; }' + '\n');
    w.document.write('}' + '\n');
    w.document.write('thead { display: table-header-group; }' + '\n');
    w.document.write('tfoot { display: table-footer-group; }' + '\n');
    w.document.write('</style>' + '\n');

    for (i in this.cssPaths) {
        w.document.write('<link rel="stylesheet" href="' + this.cssPaths[i] + '">' + '\n');
    }

    // Close the head
    w.document.write('</head><body>' + '\n');

    w.document.write('<table>' + '\n');
    w.document.write('\t' + '<thead>' + '\n');
    w.document.write('\t\t' + '<tr><td style="width:100%;">' + this.header + '</td></tr>' + '\n');
    w.document.write('\t' + '</thead>' + '\n');
    w.document.write('\t' + '<tbody>' + '\n');
    w.document.write('\t\t' + '<tr><td>' + '\n');

    w.document.write(this.content);

    w.document.write('\t\t' + '</td></tr>' + '\n');
    w.document.write('\t' + '</tbody>' + '\n');
    w.document.write('\t' + '<tfoot>' + '\n');
    w.document.write('\t\t' + '<tr><td>' + this.footer + '</td></tr>' + '\n');
    w.document.write('\t' + '</tfoot>' + '\n');
    w.document.write('</table>' + '\n');

    w.document.write('<script type="text/javascript">function closeme(){window.close();}setTimeout(closeme,50);window.print();</script></body></html>');
    //w.document.write('<script type="text/javascript">function closeme(){window.close();}</script></body></html>');
    w.document.close();
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function isIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        //return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        //var rv = ua.indexOf('rv:');
        //return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        //return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        return true;
    }

    // other browser
    return false;
}