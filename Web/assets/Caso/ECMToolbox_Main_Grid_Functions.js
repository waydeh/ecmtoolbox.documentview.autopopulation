﻿
var grid = null;
var ColumnsSetup = false;
var DataLoaded = false;
var GridData = null;
var CurrentlyDisplayedWorkflow = null;
var CurrentlyDisplayedQueue = null;
var Columns = null;

function SetupGrid() {
    if (grid == null) {
        grid = $("#workItemsGrid").ECMToolboxVirtualGrid({
            showItemCounterOnScroll: true
        });
        grid.on('columnResized', GridColumnResized)
        grid.on('selectedItemsChanged', SelectedGridItemsChanged);
        grid.on('rowDoubleClick', GridItemDoubleClicked);
        grid.on('rowRightClicked', GridContextMenu);
        grid.on('gridSorted', GridSorted);
        grid.on('headerMenuOpened', ShowHeaderMenu);
        grid.on('columnsReordered', GridColumnsReordered);
        grid.on('gridFilterChanged', GridFilterChanged);
        grid.on('gridDataUpdated', GridDataUpdated);
    }
}

function LoadGrid() {
    try{
        SelectedWorkItems = [];
        //Asyncronously load grid data while building columns
        ColumnsSetup = false;
        DataLoaded = false;
        LoadGridData();
            
        var hasFilters = false;
        if (CurrentlyDisplayedWorkflow != selectedWorkflowID || CurrentlyDisplayedQueue != selectedQueueID) {
            SelectedWorkItems = [];

            CurrentlyDisplayedWorkflow = selectedWorkflowID;
            CurrentlyDisplayedQueue = selectedQueueID;
        

            var clientSettingColumns = null;
            var filter = null;
            var sort = null;
            if (selectedQueueID == -2) {
                Columns = WorkflowQueueColumns[selectedWorkflowID].GroupBox;
                try { clientSettingColumns = ClientSettings.GridSettings[selectedWorkflowID].GroupBox; } catch (e) { }
                try { filter = ClientSettings.GridSettings[selectedWorkflowID].GroupBoxFilters; } catch (e) { }
                try { sort = ClientSettings.GridSettings[selectedWorkflowID].GroupBoxSort; } catch (e) { }
            }
            else if (selectedQueueID == -1) {
                Columns = WorkflowQueueColumns[selectedWorkflowID].Inbox
                try { clientSettingColumns = ClientSettings.GridSettings[selectedWorkflowID].Inbox; } catch (e) { }
                try { filter = ClientSettings.GridSettings[selectedWorkflowID].InboxFilters; } catch (e) { }
                try { sort = ClientSettings.GridSettings[selectedWorkflowID].InboxSort; } catch (e) { }
            }
            else {
                Columns = WorkflowQueueColumns[selectedWorkflowID].Queues[selectedQueueID];
                try { clientSettingColumns = ClientSettings.GridSettings[selectedWorkflowID].Queues[selectedQueueID]; } catch (e) { }
                try { filter = ClientSettings.GridSettings[selectedWorkflowID].QueueFilters[selectedQueueID]; } catch (e) { }
                try { sort = ClientSettings.GridSettings[selectedWorkflowID].QueueSorts[selectedQueueID]; } catch (e) { }

            }

            var disableGridPersistance = !(ClientSettings === undefined || ClientSettings == null || ClientSettings.DisableGridPersistance === undefined || ClientSettings.DisableGridPersistance == null || ClientSettings.DisableGridPersistance == false);
            if (clientSettingColumns != null && !disableGridPersistance) {
                Columns = CombineColumnsWithClientSettings(Columns, clientSettingColumns);
            }
            
            //Create Select Type Column and append to the beginning of the grid
            var selectColumn = { type: 'select', width: 30, attributes: { style: 'text-align:center;' } };
            Columns.splice(0, 0, selectColumn);
                        
            grid.SetColumns(Columns);
            if (!disableGridPersistance && sort != null) {
                grid.SetSort(sort, false);
            }
            if (!disableGridPersistance && filter != null) {
                grid.SetFilters(filter, false);
                hasFilters = true;
            }
        }
        
        if (DataLoaded) {
            SetGridData();
            DetermineActiveFunctions(null);
        }
        else {
            ColumnsSetup = true;
        }
    }
    catch (ex) {
        ECMNotification(ex, 3);
        HideWaitSpinner();
    }
}

function LoadGridData() {   
    ShowWaitSpinner('Loading...', 'wfContent')
    //Api/GetGridItems/{sessionID}/{workflowID}/{queueID}
    $.ajax({
        url: "Api/GetGridItems/" + sessionID + "/" + selectedWorkflowID + "/" + selectedQueueID,
        type: "Get",
        async: true,
        //converters: {
        //    "text json": function (data) {
        //        return $.parseJSON(data, true);
        //    }
        //},
        success: function (data) {
            try {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    ECMNotification(msg, 3, stack);
                }
                else {
                    var results = data.Data;
                    IsWorkflowManager = results.IsWorkflowManager;
                    GridData = results.Data;
                    QueuePermissions = results.QueuePermissions;
                    QueueButtons = results.QueueButtons;

                    if (ColumnsSetup) {
                        SetGridData();
                        DetermineActiveFunctions(null);
                    }
                    else {
                        DataLoaded = true;
                    }
                }
            }
            catch (ex) {
                HideWaitSpinner('wfContent');
                ECMNotification(ex, 3);
            }
        },
        error: function (msg) {
            ECMNotification(msg.statusText + ': ' + msg.responseText, 3);
            HideWaitSpinner('wfContent');
        }
    });
}

function SetGridData() {
    grid.SetData(GridData);
    HideWaitSpinner('wfContent');
}

function GridColumnResized(e, args) {
    var userColumns = [];
    var counter = 0;
    for (var i = 0; i < args.columns.length; i = i + 1) {
        if (!args.columns[i].hidden) {
            userColumns[counter] = {
                field: args.columns[i].field,
                width: args.columns[i].width
            }
            counter++;
        }
    }
    UpdateGridSettings(userColumns);
}

function SelectedGridItemsChanged(e, args) {
    SelectedWorkItems = args.SelectedItems;
    DetermineActiveFunctions();
}

function GridItemDoubleClicked(e, args) {
    SelectedWorkItems = [args.Item];
    DetermineActiveFunctions();
    Open(true);
}

function GridContextMenu(e, args) {
    SelectedWorkItems = args.SelectedItems;
    DetermineActiveFunctions();
    ShowContextMenu(args.x, args.y);
}

function GridSorted(e, args) {
    UpdateSortSettings(args);
}

function GridFilterChanged(e, args) {
    UpdateFilterSettings(args);
}

function GridColumnsReordered(e, args) {
    var userColumns = [];
    var counter = 0;
    for (var i = 0; i < args.columns.length; i = i + 1) {
        if (!args.columns[i].hidden) {
            userColumns[counter] = {
                field: args.columns[i].field,
                width: args.columns[i].width
            }
            counter++;
        }
    }
    UpdateGridSettings(userColumns);
}

function ShowHeaderMenu(e, args, x, y) {

}

function GridDataUpdated(e){
    grid.find('[data-toggle="tooltip"]').tooltip({ container: 'body' });
}



function LoadSetupIcons() {
    var icons = null;
    $.ajax({
        url: "GetWorkflowPresetIcons/" + sessionID + "/" + selectedWorkflowID,
        type: "Get",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            try {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    ECMNotification(msg, _notificationTypes.error, data.Stack);
                }
                else {
                    icons = data.Data;
                }
            }
            catch (e) {
            }
        },
        error: function (msg) {
            ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
        }
    });
    return icons;
}


function CombineColumnsWithClientSettings(cols, clicols) {
    var finalCols = [];
    for (var i = 0; i < clicols.length; i = i + 1) {
        if (clicols[i] != null) {
            var clicol = clicols[i];
            for (var iCol = 3; iCol < cols.length; iCol = iCol + 1) {
                if (!cols[iCol].hidden && cols[iCol].field == clicol.field) {
                    cols[iCol].width = clicol.width;
                    cols[iCol].copied = true;
                    finalCols[finalCols.length] = cols[iCol];
                    break;
                }
            }
        }
    }    
    for (var iCol = 3; iCol < cols.length; iCol = iCol + 1) {
        if (cols[iCol].copied != true) {
            finalCols[finalCols.length] = cols[iCol];
        }
        cols[iCol].copied = null;
    }
    return finalCols;
}

function ColumnsReordered(evt) {
    var userColumns = GetGridColumnData(evt.sender.columns);
    var movedCol = userColumns.splice(evt.oldIndex, 1);
    userColumns.splice(evt.newIndex, 0, movedCol[0]);
    UpdateGridSettings(userColumns);    
}

function ColumnsResized(evt) {
    var userColumns = GetGridColumnData(evt.sender.columns);    
    UpdateGridSettings(userColumns);
}

function GetGridColumnData(columns) {
    var colData = [];
    var counter = 0;
    for (var i = 0; i < columns.length; i = i + 1) {
        if (!columns[i].hidden) {
            colData[counter] = {
                field: columns[i].field,
                width: columns[i].width
            }
            counter++;
        }
    }
    return colData;
}

function UpdateGridSettings(userColumns) {
    if (ClientSettings === undefined || ClientSettings == null) ClientSettings = {};
    if (ClientSettings.GridSettings === undefined || ClientSettings.GridSettings == null) ClientSettings.GridSettings = [];
    if (ClientSettings.GridSettings[selectedWorkflowID] === undefined || ClientSettings.GridSettings[selectedWorkflowID] == null) {
        ClientSettings.GridSettings[selectedWorkflowID] = { Inbox: [], GroupBox: [], Queues: [] };
    }
    if (selectedQueueID == -2) {
        ClientSettings.GridSettings[selectedWorkflowID].GroupBox = userColumns;
    }
    else if (selectedQueueID == -1) {
        ClientSettings.GridSettings[selectedWorkflowID].Inbox = userColumns;
    }
    else {
        ClientSettings.GridSettings[selectedWorkflowID].Queues[selectedQueueID] = userColumns;
    }
    
    SaveClientSettings();
}

function UpdateFilterSettings(args) {
    var filters = args.filters;
    if (ClientSettings === undefined || ClientSettings == null) ClientSettings = {};
    if (ClientSettings.GridSettings === undefined || ClientSettings.GridSettings == null) ClientSettings.GridSettings = [];
    if (ClientSettings.GridSettings[selectedWorkflowID] === undefined || ClientSettings.GridSettings[selectedWorkflowID] == null) {
        ClientSettings.GridSettings[selectedWorkflowID] = { Inbox: [], GroupBox: [], Queues: [] };
    }
    if (selectedQueueID == -2) {
        ClientSettings.GridSettings[selectedWorkflowID].GroupBoxFilters = filters;
    }
    else if (selectedQueueID == -1) {
        ClientSettings.GridSettings[selectedWorkflowID].InboxFilters = filters;
    }
    else {
        if (ClientSettings.GridSettings[selectedWorkflowID].QueueFilters === undefined || ClientSettings.GridSettings[selectedWorkflowID].QueueFilters== null) {
            ClientSettings.GridSettings[selectedWorkflowID].QueueFilters = [];
        }
        ClientSettings.GridSettings[selectedWorkflowID].QueueFilters[selectedQueueID] = filters;
    }

    if (filters.length > 0) {
        $('#gridFiltersClear').show();
        //-- add filter message to divpageheadercontent
        if (typeof (Storage) !== "undefined") {
            var spaces = '&nbsp;'.repeat(50);
            $('#divPageHeaderContent').html(localStorage.getItem("lastBreadCrum") + spaces + 'FILTER IS ON');
        }
    }
    else {
        $('#gridFiltersClear').hide();
        //-- trim the filter message from divpageheadercontent
        if (typeof (Storage) !== "undefined") {
            $('#divPageHeaderContent').html(localStorage.getItem("lastBreadCrum"));
        }
    }
    SaveClientSettings();
}

//-- added 2019-03-28 to support .repeat in ie11
if (!String.prototype.repeat) {
    String.prototype.repeat = function (count) {
        'use strict';
        if (this == null)
            throw new TypeError('can\'t convert ' + this + ' to object');

        var str = '' + this;
        // To convert string to integer.
        count = +count;
        // Check NaN
        if (count != count)
            count = 0;

        if (count < 0)
            throw new RangeError('repeat count must be non-negative');

        if (count == Infinity)
            throw new RangeError('repeat count must be less than infinity');

        count = Math.floor(count);
        if (str.length == 0 || count == 0)
            return '';

        // Ensuring count is a 31-bit integer allows us to heavily optimize the
        // main part. But anyway, most current (August 2014) browsers can't handle
        // strings 1 << 28 chars or longer, so:
        if (str.length * count >= 1 << 28)
            throw new RangeError('repeat count must not overflow maximum string size');

        var maxCount = str.length * count;
        count = Math.floor(Math.log(count) / Math.log(2));
        while (count) {
            str += str;
            count--;
        }
        str += str.substring(0, maxCount - str.length);
        return str;
    }
}

function UpdateSortSettings(sort) {
    if (ClientSettings === undefined || ClientSettings == null) ClientSettings = {};
    if (ClientSettings.GridSettings === undefined || ClientSettings.GridSettings == null) ClientSettings.GridSettings = [];
    if (ClientSettings.GridSettings[selectedWorkflowID] === undefined || ClientSettings.GridSettings[selectedWorkflowID] == null) {
        ClientSettings.GridSettings[selectedWorkflowID] = { Inbox: [], GroupBox: [], Queues: [] };
    }
    if (selectedQueueID == -2) {
        ClientSettings.GridSettings[selectedWorkflowID].GroupBoxSort = sort;
    }
    else if (selectedQueueID == -1) {
        ClientSettings.GridSettings[selectedWorkflowID].InboxSort = sort;
    }
    else {
        if (ClientSettings.GridSettings[selectedWorkflowID].QueueSorts === undefined || ClientSettings.GridSettings[selectedWorkflowID].QueueSorts == null) {
            ClientSettings.GridSettings[selectedWorkflowID].QueueSorts = [];
        }
        ClientSettings.GridSettings[selectedWorkflowID].QueueSorts[selectedQueueID] = sort;
    }
    SaveClientSettings();
}


