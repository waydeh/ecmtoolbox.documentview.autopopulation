﻿
var WorkItemDataReturned = null;
var CommentsAndHistoryReturned = null;
var SubmitReturned = null;
var WorkItemVisibleReturned = null;

//$(document).ready(function () {

    window.addEventListener('message', function (event) {
        var eventOrigin = event.origin;
        try {
            var msg = JSON.parse(event.data);
            if (msg === undefined || msg == null || msg.MessageType === undefined || msg.MessageType == null || msg.MessageType.trim() == '')
                throw "Message Format Incorrect.  Message does not contain MessageType";

            switch (msg.MessageType.toUpperCase()) {
                case 'ECMT_GETWORKITEMDATA_RETURN':
                    if (WorkItemDataReturned != null) {
                        WorkItemDataReturned(ConvertFieldDates(msg.WorkItemData));
                    }                   
                    break;
                case 'ECMT_GETHISTORYANDCOMMENTS_RETURN':
                    if (CommentsAndHistoryReturned != null) {
                        CommentsAndHistoryReturned(ConvertCommentsAndHistoryDates(msg.CommentsAndHistory));
                    }
                    break;
                case 'ECMT_ADDCOMMENT_RETURN':
                    if (CommentsAndHistoryReturned != null) {
                        CommentsAndHistoryReturned(ConvertCommentsAndHistoryDates(msg.CommentsAndHistory));
                    }
                    break;
                case 'ECMT_SUBMIT_RETURN':
                    if (SubmitReturned != null) {
                        SubmitReturned(msg.Data);
                    }
                    break;
                case 'ECMT_WORKITEM_VISIBLE':
                    if (WorkItemVisibleReturned != null) {
                        var parsedData = parse_query_string(window.location.search.substring(1));
                        var WorkInstanceID = parseInt(parsedData.WorkInstanceID);
                        if (msg.WorkInstanceID == WorkInstanceID) {
                            WorkItemVisibleReturned();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        catch (ex) {
            ECMNotification('Invalid PostMessage Received: ' + ex);
        }
    });
//});

function ConvertFieldDates(_WorkItemData) {
    _WorkItemData.WorkflowStartTime = new Date(_WorkItemData.WorkflowStartTime);
    _WorkItemData.InstanceStartTime = new Date(_WorkItemData.InstanceStartTime);
    _WorkItemData.OwnershipStartTime = new Date(_WorkItemData.OwnershipStartTime);
    
    for (var i = 0; i < _WorkItemData.Fields.length; i = i + 1) {
        var field = _WorkItemData.Fields[i];
        if (field.FieldType == 1) { 
            //Date
            if (field.Value === undefined) field.Value = null;
            else {
                field.Value = field.Value.substring(5, 7) + "/" + field.Value.substring(8, 10) + "/" + field.Value.substring(0, 4);
            }
        }
        if (field.FieldType == 2) {
            //DateTime
            if (field.Value === undefined) field.Value = null;
            else {
                field.Value = Date(field.Value);
            }
        }
    }

    return _WorkItemData;
}

function ConvertCommentsAndHistoryDates(commentsAndHistory) {
    for (var i = 0; i < commentsAndHistory.Comments.length; i = i + 1) {
        commentsAndHistory.Comments[i].Comment_Date = new Date(commentsAndHistory.Comments[i].Comment_Date);
    }
    for (var i = 0; i < commentsAndHistory.History.length; i = i + 1) {
        var histItem = commentsAndHistory.History[i];
        histItem.Event_Date = new Date(histItem.Event_Date);
        if (histItem.IsInstance) {
            for (var j = 0; j < histItem.Events.length; j = j + 1) {
                histItem.Events[j].Event_Date = new Date(histItem.Events[j].Event_Date);
            }
        }
    }
    return commentsAndHistory;
}

function ECMT_LoadWorkItemData(Completed_Function) {
    WorkItemDataReturned = Completed_Function;
    var parsedData =parse_query_string(window.location.search.substring(1));    
    var WorkInstanceID = parseInt(parsedData.WorkInstanceID);
    var msg = {
        MessageType: 'ECMT_GETWORKITEMDATA',
        WorkInstanceID: WorkInstanceID
    };
    var jsonData = JSON.stringify(msg);
    try{
        window.parent.postMessage(jsonData, "*");
    }
    catch (e) {
        window.opener.postMessage(jsonData, "*");
    }
}

function ECMT_LoadCommentsAndHistory(WorkflowID, WorkItemID, Completed_Function) {
    CommentsAndHistoryReturned = Completed_Function;
    var msg = {
        MessageType: 'ECMT_GETHISTORYANDCOMMENTS',
        workflowID: WorkflowID,
        itemID: WorkItemID
    };
    var jsonData = JSON.stringify(msg);
    try {
        window.parent.postMessage(jsonData, "*");
    }
    catch (e) {
        window.opener.postMessage(jsonData, "*");
    }
}

function ECMT_AddComment(WorkflowID, WorkItemID, WorkInstanceID, Comment, Completed_Function) {
    CommentsAndHistoryReturned = Completed_Function;
    var msg = {
        MessageType: 'ECMT_ADDCOMMENT',
        WorkflowID: WorkflowID,
        WorkItemID: WorkItemID,
        WorkInstanceID: WorkInstanceID,
        comment: Comment
    };
    var jsonData = JSON.stringify(msg);
    try {
        window.parent.postMessage(jsonData, "*");
    }
    catch (e) {
        window.opener.postMessage(jsonData, "*");
    }
}

function ECMT_Submit(WorkflowID, WorkItemID, WorkInstanceID, Data, Completed_Function) {
    SubmitReturned = Completed_Function;
    var msg = {
        MessageType: 'ECMT_SUBMIT',
        WorkflowID:WorkflowID,
        WorkItemID:WorkItemID,
        WorkInstanceID: WorkInstanceID,
        data: Data
    };
    var jsonData = JSON.stringify(msg);
    try {
        window.parent.postMessage(jsonData, "*");
    }
    catch (e) {
        window.opener.postMessage(jsonData, "*");
    }
}

function ECMT_Close() {
    var msg = {
        MessageType: 'ECMT_CLOSE'
    };
    var jsonData = JSON.stringify(msg);
    try {
        window.parent.postMessage(jsonData, "*");
    }
    catch (e) {
        window.opener.postMessage(jsonData, "*");
    }
}

function ECMT_WorkItem_Visible(Completed_Function) {
    WorkItemVisibleReturned = Completed_Function;
}


function parse_query_string(query) {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var key = decodeURIComponent(pair[0]);
        var value = decodeURIComponent(pair[1]);
        // If first entry with this name
        if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === "string") {
            var arr = [query_string[key], decodeURIComponent(value)];
            query_string[key] = arr;
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
    }
    return query_string;
}