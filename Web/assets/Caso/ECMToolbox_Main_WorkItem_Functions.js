﻿
var WorkItemDataStorage = [];

var PreloadItems = true;

var QueuedItemIDs = null;
function OpenWorkItem(showIFrame) {
    firstDoc = true;
    WorkItemDataStorage = [];
    if (showIFrame == null) { showIFrame = true; }
    try {
        PreloadItems = ClientSettings.Prefetch
    }
    catch (e) {
        PreloadItems = false;
    }
    var itemID = QueuedItemIDs[0];
    $.ajax({
        url: "Api/OpenWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + itemID,
        type: "GET",
        async: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, '');
            }
            else {
                if (data.Messages.InfoMessages.length > 0) {
                    HideWaitSpinner();
                    ECMNotification(data.Messages.InfoMessages[0], 2);
                }
                else {
                    if (!(data.Messages.InfoPopupMessages === undefined) && !(data.Messages.InfoPopupMessages == null) && data.Messages.InfoPopupMessages.length > 0) {
                        HideWaitSpinner();
                        ECMNotification(data.Messages.InfoPopupMessages[0], 4);
                    }
                    else {
                        var workItemData = data.Data;
                        WorkItemDataStorage.push(workItemData);
                        var itemurl = workItemData.WorkItemPageURL + (workItemData.WorkItemPageURL.indexOf('?') != -1 ? '&WorkInstanceID=' + workItemData.WorkInstanceID : '?WorkInstanceID=' + workItemData.WorkInstanceID);
                        if (showIFrame) {
                            SetNoMenu();
                            SetIframeContent(itemurl);
                            ShowIframe();
                        }

                        HideWaitSpinner();
                    }
                }
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}
var preloading = false;
function PreloadWorkItemData(workInstanceID) {
    preloading = true;
    if (!PreloadItems || ClientSettings.PickNext != 4) return;
    if (QueuedItemIDs.length >= 1) {

        SetBackgroundIframeContent('about:blank');

        setTimeout(function () {
            var itemID = QueuedItemIDs[1];
            $.ajax({
                url: "Api/OpenWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + itemID,
                type: "GET",
                async: true,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data.Messages.ErrorMessages.length > 0) {
                        var msg = data.Messages.ErrorMessages[0];
                        var stack = data.Stack;
                        HideWaitSpinner();
                        ECMNotification(msg, 3, '');
                    }
                    else {
                        if (data.Messages.InfoMessages.length > 0) {
                            HideWaitSpinner();
                            ECMNotification(data.Messages.InfoMessages[0], 2);
                        }
                        else {
                            if (!(data.Messages.InfoPopupMessages === undefined) && !(data.Messages.InfoPopupMessages == null) && data.Messages.InfoPopupMessages.length > 0) {
                                HideWaitSpinner();
                                ECMNotification(data.Messages.InfoPopupMessages[0], 4);
                            }
                            else {
                                var PreloadedWorkItemData = data.Data;
                                WorkItemDataStorage.push(PreloadedWorkItemData);

                                var itemurl = PreloadedWorkItemData.WorkItemPageURL + (PreloadedWorkItemData.WorkItemPageURL.indexOf('?') != -1 ? '&WorkInstanceID=' + PreloadedWorkItemData.WorkInstanceID : '?WorkInstanceID=' + PreloadedWorkItemData.WorkInstanceID);
                                SetBackgroundIframeContent(itemurl);
                                HideWaitSpinner();
                            }
                            preloading = false;
                        }
                    }
                },
                error: function (msg) {
                    ECMNotification(msg, 2);
                    HideWaitSpinner();
                    preloading = false;
                }
            });
        }
        , 100);

    }
}

var firstDoc = true;
function LoadWorkItemData(workInstanceID) {
    for (var i = 0; i < WorkItemDataStorage.length; i = i + 1) {
        if (WorkItemDataStorage[i].WorkInstanceID == workInstanceID) {
            if (firstDoc) {
                firstDoc = false;
                _EcmtPostMessage_WorkItem_Visible(workInstanceID);
                PreloadWorkItemData();
            }
            return WorkItemDataStorage[i];
        }
    }
    ECMNotification("Error Retrieving WorkItemData for Instance ID: " + workInstanceID, 2);
}

function LoopUntilNotLoading() {
    if (preloading || submitting) {
        setTimeout(function () {
            LoopUntilNotLoading();
        }, 50)
    }
}

function PickNextWorkItem(pickNextOption) {
    LoopUntilNotLoading();

    if (pickNextOption == 4) {
        //-- pick next from selected rather than go up to server to figure out next
        if (PreloadItems) {
            if (QueuedItemIDs.length >= 1) {
                ToggleVisibleIframes();
                _EcmtPostMessage_WorkItem_Visible(QueuedItemIDs[1]);
                QueuedItemIDs.splice(0, 1);
                if (QueuedItemIDs.length == 0) {
                    ECMNotification("No more items selected", 1);
                    SetLargeMenu();
                    ShowGrid();
                    RefreshMenuItems();
                }
                else {
                    //-- open the next item
                    PreloadWorkItemData();
                }
            }
            else {
                ECMNotification("No more items selected", 1);
                SetLargeMenu();
                ShowGrid();
                RefreshMenuItems();
            }
        }
        else {
            QueuedItemIDs.splice(0, 1);
            if (QueuedItemIDs.length == 0) {
                ECMNotification("No more items selected", 1);
                SetLargeMenu();
                ShowGrid();
                RefreshMenuItems();
            }
            else {
                //-- open the next item
                OpenWorkItem(true);
            }
        }
    }
    else {
        //Api/PickNextWorkItem/{sessionID}/{workflowID}/{queueID}/{pickNextOption}
        ShowWaitSpinner('Please Wait...');
        firstDoc = true
        $.ajax({
            url: "Api/PickNextWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + selectedQueueID + "/" + pickNextOption,
            type: "GET",
            async: true,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    HideWaitSpinner();
                    ECMNotification(msg, 3, stack);
                }
                else {
                    HideWaitSpinner();
                    if (data.Data == null) {
                        SetLargeMenu();
                        ShowGrid();
                        RefreshMenuItems();
                    }
                    else {
                        WorkItemData = data.Data;
                        ShowIframe();
                        SetLargeMenu();
                        SetIframeContent(WorkItemData.WorkItemPageURL);
                        HideWaitSpinner();
                    }
                }
            },
            error: function (msg) {
                ECMNotification(msg, 2);
                HideWaitSpinner();
            }
        });
    }
}

function LoadCommentsAndHistory(workflowID, itemID, completeMethod, event) {
    $.ajax({
        url: "Api/GetCommentsAndHistory/" + sessionID + "/" + workflowID + "/" + itemID,
        type: "GET",
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        converters: {
            "text json": function (data) {
                return $.parseJSON(data, true);
            }
        },
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                ECMNotification(msg, 3, stack);
            }
            else {
                completeMethod(data.Data, event);
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}

function AddComment(workflowID, itemID, instanceID, comment, completeMethod, event) {
    //Api/AddComment/{sessionID}/{workflowID}/{itemID}/{instanceID}
    $.ajax({
        url: "Api/AddComment/" + sessionID + "/" + workflowID + "/" + itemID + "/" + instanceID,
        type: "Post",
        data: JSON.stringify(comment),
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        converters: {
            "text json": function (data) {
                return $.parseJSON(data, true);
            }
        },
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                completeMethod(data.Data, event);
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}

var submitting = false;
function Submit(workflowID, workItemID, workInstanceID, data, Submit_Complete, event) {
    if (!submitting) {
        /*
        public class SubmitData
        {
            public short WorkflowID { get; set; }
            public long WorkInstanceID { get; set; }
            public FieldUpdate[] FieldUpdates { get; set; }
            public string SelectedSendToQueue { get; set; }
            public string SelectedSendToUser { get; set; }
            public string ButtonText { get; set; }
        }
    
        public class SubmitReturn
        {
            public bool CloseWorkItem { get; set; }
            public Javascript[] JavascriptFunctions { get; set; }
            public Popup[] Popups { get; set; }
        }
        */
        submitting = true;
        if (data.FieldUpdates === undefined || data.FieldUpdates == null || !(Object.prototype.toString.call(data.FieldUpdates) === '[object Array]')) {
            if (data.FieldUpdates === undefined || data.FieldUpdates == null) {
                data.FieldUpdates = [];
            }
            else {
                data.FieldUpdates = [data.FieldUpdates];
            }
        }

        ShowWaitSpinner('Please Wait...');
        $.ajax({
            url: "Api/SubmitWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + workItemID + "/" + workInstanceID,
            type: "Post",
            data: JSON.stringify(data),
            async: true,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    HideWaitSpinner();
                    ECMNotification(msg, 3, stack);
                }
                else {
                    HideWaitSpinner();


                    if (data.Data.JavascriptFunctions.length > 0) {
                        for (var i = 0; i < data.Data.JavascriptFunctions.length; i = i + 1) {
                            var jsf = data.Data.JavascriptFunctions[i];
                            if (jsf.RunLocation == 0) {
                                try {
                                    eval(jsf.Script);
                                }
                                catch (e) { }
                            }
                        }
                    }
                    if (data.Data.Popups.length > 0) {
                        for (var i = 0; i < data.Data.Popups.length; i = i + 1) {
                            var pop = data.Data.Popups[i];
                            if (pop.Popuptype == 2) { //New Window
                                try {
                                    var url = pop.URL + (pop.URL.indexOf('?') != -1 ? pop.URL + '&sessionID=' + sessionID : '?sessionID=' + sessionID);
                                    window.open(url, "_blank");
                                }
                                catch (e) { }
                            }
                            else if (pop.Popuptype == 1) { //Workflow Main Window
                                try {
                                    var url = pop.URL + (pop.URL.indexOf('?') != -1 ? pop.URL + '&sessionID=' + sessionID : '?sessionID=' + sessionID);
                                    ShowPopup(url);
                                }
                                catch (e) { }
                            }
                        }
                    }

                    //Call Client Side Method
                    try {
                        Submit_Complete(data.Data, event);
                    }
                    catch (e) { }



                    if (data.Data.CloseWorkItem) {
                        if (GridIsVisible || ClientSettings === undefined || ClientSettings == null || ClientSettings.PickNext === undefined || ClientSettings.PickNext == null || ClientSettings.PickNext == 0) {
                            SetLargeMenu();
                            ShowGrid();
                            RefreshMenuItems();
                        }
                        else {
                            PickNextWorkItem(ClientSettings.PickNext);
                        }
                    }

                }
                submitting = false;
            },
            error: function (msg) {
                submitting = false;
                ECMNotification(msg, 2);
                HideWaitSpinner();
            }
        });
    }
}



function EndWorkflow() {
    //public class EndWorkflowData
    //{
    //    public long[] WorkItemIDs { get; set; }
    //    public long[] WorkInstanceIDs { get; set; }
    //}
    //[Route("Api/EndWorkItemsMultiple/{sessionID}/{workflowID}")]

    var data = {
        WorkInstanceIDs: SelectedInstanceIDs
    }

    ShowWaitSpinner('Please Wait...');
    $.ajax({
        url: "Api/EndWorkItemsMultiple/" + sessionID + "/" + selectedWorkflowID,
        type: "Post",
        data: JSON.stringify(data),
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                HideWaitSpinner();
                RefreshMenuItems();
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}