﻿$.fn.ECMToolboxVirtualGrid = function (options) {
    var __ECMT_Grid = this;

    // SETTINGS
    {
        this.Settings = $.extend({
            // These are the defaults.
            showItemCounterOnScroll:true
        }, options);
    }

    // Variables
    {
        this._ScrollItemNumber = null;
        this._TotalItems = null;
        this._MaxVisibleItems = null;
        this._PixelsPerItem = null;
        this._ItemsPerPixel = null;
        this._draggingYStart = null;
        this._scrollableHeight = null;
        this._scrollerStartY = null;
        this._Columns = null;
        this._VisibleColumns = null;
        this._Data = null;
        this._CurrentDataView = null;
        this._SelectedItems = null;
        this._Sort = null;
        this._Filters = [];
        this._TimeZoneOffset = null;
    }

    // Methods
    {
        // COLUMN SETUP
        {
            this.SetColumns = function (columns) {
                __ECMT_Grid._SetTimeZoneOffset();
                __ECMT_Grid._Data = null;
                __ECMT_Grid._CurrentDataView = null;
                __ECMT_Grid._TotalItems = null;
                __ECMT_Grid._Filters = [];
                __ECMT_Grid._Sort = null;
                __ECMT_Grid._SetupColumns(columns);
                __ECMT_Grid.ResetScrollBar();
                __ECMT_Grid._SelectedItems = [];
            }
            this._SetupColumns = function (columns) {
                __ECMT_Grid._TableCols.empty();
                __ECMT_Grid._TableHeaders.empty();
                var headerRow = $('<tr></tr>');
                __ECMT_Grid._TableHeaders.append(headerRow);
                for (var i = 0; i < columns.length; i = i + 1) {
                    var columnInfo = {
                        title: '',
                        field: '',
                        width: 150,
                        type: 'string',
                        hidden: false,
                        format: '',
                        template: '',
                        encoded: false,
                        filterable: '',
                        attributes: { style: 'text-align:left' }
                    }
                    $.extend(true, columnInfo, Columns[i]);
                    if (columnInfo.field == 'IC') columnInfo.type = 'icons';
                    if (!columnInfo.hidden) {
                        switch (columnInfo.type) {
                            case 'string':
                                var header = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');
                                var headerText = $('<div class="ecmt_vg_gridheadertext"></div>');
                                var headerFilterIcon = $('<i class="fa fa-filter k-icon ecmt_vg_gridheaderfiltericon"></i>');
                                var headerSortIndicator = $('<i class="ecmt_vg_gridsort fa"></i>');
                                var headerResize = $('<div class="ecmt_vg_gridheaderresize" coldefid="col_def_' + columnInfo.field + '"></div>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                header.attr('ecmt_vg_data_field', columnInfo.field);
                                headerResize.on('mousedown', __ECMT_Grid._ColumnResize);
                                headerText.text(columnInfo.title);
                                header.append(headerResize);
                                header.append(headerFilterIcon);
                                header.append(headerSortIndicator);
                                header.append(headerText);
                                headerRow.append(header);
                                break;
                            case 'date':
                                var header = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');
                                var headerText = $('<div class="ecmt_vg_gridheadertext"></div>');
                                var headerFilterIcon = $('<i class="fa fa-filter k-icon ecmt_vg_gridheaderfiltericon"></i>');
                                var headerSortIndicator = $('<i class="ecmt_vg_gridsort fa"></i>');
                                var headerResize = $('<div class="ecmt_vg_gridheaderresize" coldefid="col_def_' + columnInfo.field + '"></div>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                header.attr('ecmt_vg_data_field', columnInfo.field);
                                headerResize.on('mousedown', __ECMT_Grid._ColumnResize);
                                headerText.text(columnInfo.title);
                                header.append(headerResize);
                                header.append(headerFilterIcon);
                                header.append(headerSortIndicator);
                                header.append(headerText);
                                headerRow.append(header);
                                break;
                            case 'number':
                                var header = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');
                                var headerText = $('<div class="ecmt_vg_gridheadertext"></div>');
                                var headerFilterIcon = $('<i class="fa fa-filter k-icon ecmt_vg_gridheaderfiltericon"></i>');
                                var headerSortIndicator = $('<i class="ecmt_vg_gridsort fa"></i>');
                                var headerResize = $('<div class="ecmt_vg_gridheaderresize" coldefid="col_def_' + columnInfo.field + '"></div>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                header.attr('ecmt_vg_data_field', columnInfo.field);
                                headerResize.on('mousedown', __ECMT_Grid._ColumnResize);
                                headerText.text(columnInfo.title);
                                header.append(headerResize);
                                header.append(headerFilterIcon);
                                header.append(headerSortIndicator);
                                header.append(headerText);
                                headerRow.append(header);
                                break;
                            case 'check':
                                var header = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');
                                var headerText = $('<div class="ecmt_vg_gridheadertext"></div>');
                                var headerFilterIcon = $('<i class="fa fa-filter k-icon ecmt_vg_gridheaderfiltericon"></i>');
                                var headerSortIndicator = $('<i class="ecmt_vg_gridsort fa"></i>');
                                var headerResize = $('<div class="ecmt_vg_gridheaderresize" coldefid="col_def_' + columnInfo.field + '"></div>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                header.attr('ecmt_vg_data_field', columnInfo.field);
                                headerResize.on('mousedown', __ECMT_Grid._ColumnResize);
                                headerText.text(columnInfo.title);
                                header.append(headerResize);
                                header.append(headerFilterIcon);
                                header.append(headerSortIndicator);
                                header.append(headerText);
                                headerRow.append(header);
                                break;
                            case 'select':
                                var header = $('<th class="ecmt_vg_gridheader" style="text-align:center !important; vertical-align:middle; margin-bottom:5px;"></th>');
                                var selectAllcb = $('<input id="ecmt_vg_checkall" type="checkbox" />');
                                var selectAlllbl = $('<label for="ecmt_vg_checkall"></label>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                selectAllcb.change(__ECMT_Grid._SelectAllChanged);
                                header.append(selectAllcb);
                                header.append(selectAlllbl);
                                headerRow.append(header);
                                break;
                            case 'icons':
                                var header = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');
                                var headerText = $('<div class="ecmt_vg_gridheadertext"></div>');
                                var headerFilterIcon = $('<i class="fa fa-filter k-icon ecmt_vg_gridheaderfiltericon"></i>');
                                var headerSortIndicator = $('<i class="ecmt_vg_gridsort fa"></i>');
                                var headerResize = $('<div class="ecmt_vg_gridheaderresize" coldefid="col_def_' + columnInfo.field + '"></div>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                header.attr('ecmt_vg_data_field', columnInfo.field);
                                headerResize.on('mousedown', __ECMT_Grid._ColumnResize);
                                headerText.text(columnInfo.title);
                                header.append(headerResize);
                                header.append(headerFilterIcon);
                                header.append(headerSortIndicator);
                                header.append(headerText);
                                headerRow.append(header);
                                break;
                            default:
                                var header = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');
                                var headerText = $('<div class="ecmt_vg_gridheadertext"></div>');
                                var headerFilterIcon = $('<i class="fa fa-filter k-icon ecmt_vg_gridheaderfiltericon"></i>');
                                var headerSortIndicator = $('<i class="ecmt_vg_gridsort fa"></i>');
                                var headerResize = $('<div class="ecmt_vg_gridheaderresize" coldefid="col_def_' + columnInfo.field + '"></div>');
                                header.css('min-width', columnInfo.width).css('max-width', columnInfo.width);
                                header.attr('ecmt_vg_data_field', columnInfo.field);
                                headerResize.on('mousedown', __ECMT_Grid._ColumnResize);
                                headerText.text(columnInfo.title);
                                header.append(headerResize);
                                header.append(headerFilterIcon);
                                header.append(headerSortIndicator);
                                header.append(headerText);
                                headerRow.append(header);
                                break;
                        }

                        
                        __ECMT_Grid._TableCols.append('<col id="col_def_' + columnInfo.field + '" style="min-width:' + columnInfo.width + 'px; max-width:' + columnInfo.width + 'px; ">');
                    }
                    Columns[i] = columnInfo;

                }
                __ECMT_Grid._Columns = columns;
                __ECMT_Grid._VisibleColumns = $.grep(__ECMT_Grid._Columns, function (e) { return (!e.hidden); });
                __ECMT_Grid._TableBody.empty();
                __ECMT_Grid._ScrollBar.hide();
                
            }
        }

        // SET DATA
        {
            this.SetData = function (data) {
                var dateColumns = [];
                var datetimeColumns = [];
                for (var i = 0; i < __ECMT_Grid._Columns.length; i = i + 1) {
                    if (__ECMT_Grid._Columns[i].type == 'date'){
                        if (__ECMT_Grid._Columns[i].format.toLowerCase().indexOf('h') != -1) {
                            datetimeColumns.push(__ECMT_Grid._Columns[i].field);
                        }
                        else {
                            dateColumns.push(__ECMT_Grid._Columns[i].field);
                        }
                    }
                }

                //all for performance... urgh
                var datalength = data.length;
                var datetypelength = dateColumns.length;
                var datetimetypelength = datetimeColumns.length;
                var dtColumnCounter = 0;
                var field = '';

                //Possibilities sorted by likelyhood
                //IF Only DateTimes
                if (datetimetypelength != 0 && datetypelength == 0) {
                    for (var i = 0; i < datalength; i = i + 1) {
                        data[i].ecmt_data_id = "ecmt_id_" + i;
                        data[i].ecmt_vg_selected = false;

                        for (dtColumnCounter = 0; dtColumnCounter < datetimetypelength; dtColumnCounter = dtColumnCounter + 1) {
                            field = datetimeColumns[dtColumnCounter];                            
                            data[i][field] = __ECMT_Grid._jsonDateTimeConverter(data[i][field]);                            
                        }
                    }
                }
                //IF No Dates or Datetimes
                else if (datetypelength == 0 && datetimetypelength == 0) { //IF No Dates or Datetimes
                    for (var i = 0; i < datalength; i = i + 1) {
                        data[i].ecmt_data_id = "ecmt_id_" + i;
                        data[i].ecmt_vg_selected = false;
                    }
                }
                //IF Only Dates
                else if (datetimetypelength == 0 && datetypelength != 0) {
                    for (var i = 0; i < datalength; i = i + 1) {
                        data[i].ecmt_data_id = "ecmt_id_" + i;
                        data[i].ecmt_vg_selected = false;

                        for (dtColumnCounter = 0; dtColumnCounter < datetypelength; dtColumnCounter = dtColumnCounter + 1) {
                            field = dateColumns[dtColumnCounter];
                            data[i][field] = __ECMT_Grid._jsonDateConverter(data[i][field]);
                        }
                    }
                }
                //IF Both Dates and DateTimes
                else {
                    for (var i = 0; i < datalength; i = i + 1) {
                        data[i].ecmt_data_id = "ecmt_id_" + i;
                        data[i].ecmt_vg_selected = false;

                        for (dtColumnCounter = 0; dtColumnCounter < datetypelength; dtColumnCounter = dtColumnCounter + 1) {
                            field = dateColumns[dtColumnCounter];
                            data[i][field] = __ECMT_Grid._jsonDateConverter(data[i][field]);
                        }
                        for (dtColumnCounter = 0; dtColumnCounter < datetimetypelength; dtColumnCounter = dtColumnCounter + 1) {
                            field = datetimeColumns[dtColumnCounter];
                            data[i][field] = __ECMT_Grid._jsonDateTimeConverter(data[i][field]);
                        }
                    }
                }

                __ECMT_Grid._Data = data;
                __ECMT_Grid._SelectedItems = [];
                __ECMT_Grid._UpdateGridView();
            }

            
        }
        
        //Date and DateTime Conversions
        {
            this._RegExUTCDate = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:[.,]\d+)?Z?/i;
            this._SetTimeZoneOffset = function () {
                if (__ECMT_Grid._TimeZoneOffset == null) {
                    var date = new Date();
                    __ECMT_Grid._TimeZoneOffset = date.getTimezoneOffset() * 60 * 1000;
                }
            }
            this._jsonDateConverter = function (value) {
                if (typeof (value) === "string") {
                    if (__ECMT_Grid._RegExUTCDate.test(value)) {
                        if (value == '0001-01-01T00:00:00') {
                            return null;
                        }
                        return new Date(value);
                    }
                    if (dateNet.test(value)) {
                        return new Date(parseInt(dateNet.exec(value)[1], 10));
                    }
                }
                return value;
            };
            this._jsonDateTimeConverter = function (value) {
                if (typeof (value) === "string") {
                    if (__ECMT_Grid._RegExUTCDate.test(value)) {
                        if (value == '0001-01-01T00:00:00') { return null; }
                        var dt = new Date(value);
                        dt = new Date(dt.valueOf() - __ECMT_Grid._TimeZoneOffset);
                        return dt;
                    }
                    if (dateNet.test(value)) {
                        return new Date(parseInt(dateNet.exec(value)[1], 10));
                    }
                }
                return value;
            };
        }
        
        // UPDATE DATA VIEW
        {
            this._UpdateGridView = function () {

                if (__ECMT_Grid._Filters.length > 0) {
                    for (var i = 0; i < __ECMT_Grid._Filters.length; i = i + 1) {
                        if (i == 0) {
                            //first time use _Data to avoid extra copy
                            switch (__ECMT_Grid._Filters[i].column.type) {
                                case 'string':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_String(__ECMT_Grid._Data, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'number':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Number(__ECMT_Grid._Data, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'date':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Date(__ECMT_Grid._Data, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'icons':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Icons(__ECMT_Grid._Data, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'check':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Check(__ECMT_Grid._Data, __ECMT_Grid._Filters[i]);
                                    break;
                            }
                        }
                        else {
                            switch (__ECMT_Grid._Filters[i].column.type) {
                                case 'string':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_String(__ECMT_Grid._CurrentDataView, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'number':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Number(__ECMT_Grid._CurrentDataView, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'date':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Date(__ECMT_Grid._CurrentDataView, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'icons':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Icons(__ECMT_Grid._CurrentDataView, __ECMT_Grid._Filters[i]);
                                    break;
                                case 'check':
                                    __ECMT_Grid._CurrentDataView = __ECMT_Grid._Filter_Type_Check(__ECMT_Grid._CurrentDataView, __ECMT_Grid._Filters[i]);
                                    break;
                            }
                        }
                    }
                }
                else {
                    __ECMT_Grid._CurrentDataView = $.grep(__ECMT_Grid._Data, function () { return true; });
                }
                if (__ECMT_Grid._Sort != null) {
                    var nullValue = ''
                    switch (__ECMT_Grid._Sort.column.type) {
                        case 'date':
                            nullValue = new Date('1/1/0001');
                            break;
                        case 'number':
                            nullValue = -99999999999;
                            break;
                        case 'bool':
                            nullValue = false;
                            break;
                        default:
                            nullValue = '';
                            break;
                    }

                    __ECMT_Grid._CurrentDataView.sort(function (a, b) {
                        return ((a[__ECMT_Grid._Sort.column.field] == null ? nullValue : a[__ECMT_Grid._Sort.column.field]) > (b[__ECMT_Grid._Sort.column.field] == null ? nullValue : b[__ECMT_Grid._Sort.column.field])) ? 1 : (((b[__ECMT_Grid._Sort.column.field] == null ? nullValue : b[__ECMT_Grid._Sort.column.field]) > (a[__ECMT_Grid._Sort.column.field] == null ? nullValue : a[__ECMT_Grid._Sort.column.field])) ? -1 : 0);
                    });
                    if (__ECMT_Grid._Sort.direction == 'DESC') __ECMT_Grid._CurrentDataView.reverse();
                }
                __ECMT_Grid._TotalItems = __ECMT_Grid._CurrentDataView.length;
                __ECMT_Grid._SetupGridArea();
                __ECMT_Grid.Scroll_Setup();
                __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber);
                __ECMT_Grid._PropagateGrid();
                __ECMT_Grid._SelectAllRowsReset();
            }
        }
        
        // RESET SELECT ALL CHECKBOX (by wayne)
        {
            this._SelectAllRowsReset = function () {
                $('#ecmt_vg_checkall').prop('checked', false);
            }
        }

        // SETUP BLANK GRID ROWS BASED ON AVAILABLE SPACE
        {
            this._SetupGridArea = function () {
                __ECMT_Grid._TableBody.empty();
                if (__ECMT_Grid._TotalItems === undefined || __ECMT_Grid._TotalItems == null || __ECMT_Grid._TotalItems == 0) {
                    __ECMT_Grid._MaxVisibleItems = 0;
                }
                else {
                    var availableHeight = __ECMT_Grid._tableContainer.innerHeight() - 15;
                    var continueAddRows = true;
                    var iRowCount = 0;
                    while (continueAddRows) {
                        var row = $('<tr></tr>');
                        for (var iCol = 0; iCol < __ECMT_Grid._Columns.length; iCol = iCol + 1) {
                            if (!__ECMT_Grid._Columns[iCol].hidden) {
                                var col = $('<td></td>')
                                col.attr('style', __ECMT_Grid._Columns[iCol].attributes.style);
                                row.append(col);
                            }
                        }
                        __ECMT_Grid._TableBody.append(row);
                        iRowCount++;
                        if (__ECMT_Grid._TotalItems == iRowCount) continueAddRows = false;
                        if (availableHeight < __ECMT_Grid._itemTable.outerHeight()) continueAddRows = false;
                    }
                    if (availableHeight < __ECMT_Grid._itemTable.outerHeight()) {
                        __ECMT_Grid._TableBody.children().last().remove();
                        iRowCount--;
                    }
                    __ECMT_Grid._MaxVisibleItems = iRowCount;
                }
            }
        }
        
        // INSERT DATA INTO AVAILABLE GRID ROWS
        {
            
            this._PropagateGrid = function () {
                if (__ECMT_Grid._VisibleColumns != null) {
                    var iCol = 0;
                    var iItem = __ECMT_Grid._ScrollItemNumber;
                    var iMax = __ECMT_Grid._ScrollItemNumber + __ECMT_Grid._MaxVisibleItems;
                    var iCol, iGridCol, iRow, iItem, item, col, gridrow, gridCell, iColumnCount;
                    
                    iColumnCount = __ECMT_Grid._VisibleColumns.length;
                    for (var iRow = 0; iItem < iMax; iRow = iRow + 1) {
                        item = __ECMT_Grid._CurrentDataView[iItem];
                        gridRow = $(__ECMT_Grid._TableBody.children()[iRow]);
                        gridRow.attr('ecmt_data_id', item.ecmt_data_id);
                        gridRow.attr('selector', 'select_' + item.ecmt_data_id);
                        if (item.BG === undefined || item.BG == null || item.BG == '') gridRow.css('background-color', '#ffffff');
                        else gridRow.css('background-color', item.BG);
                        if (item.ecmt_vg_selected) gridRow.addClass('k-state-selected');
                        else gridRow.removeClass('k-state-selected');
                        iGridCol = 0;
                        for (iCol = 0; iCol < iColumnCount; iCol = iCol + 1) {
                            col = __ECMT_Grid._VisibleColumns[iCol];
                            gridCell = $(gridRow.children()[iGridCol]);
                            if (col.type != 'select' && item[col.field] == null) gridCell.text('');
                            else {
                                switch (col.type) {
                                    case 'number':
                                        switch (col.format) {
                                            case '{0:c2}':
                                                gridCell.text('$' + parseFloat(item[col.field]).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                                                break;
                                            default:
                                                gridCell.text(item[col.field]);
                                                break;
                                        }
                                        break;
                                    case 'string':
                                        gridCell.text(item[col.field]);
                                        break;
                                    case 'icons':
                                        gridCell.html(item[col.field]);
                                        break;
                                    case 'select':
                                        gridCell.html('<input id="select_' + item.ecmt_data_id + '" class="ecmt_vg_select_cb" type="checkbox" ' + (item.ecmt_vg_selected ? 'checked="checked" ' : '') + '/>');
                                        gridCell.append('<label class="ecmt_vg_select_lbl"></label>');
                                        break;
                                    case 'date':
                                        var dt = item[col.field];
                                        switch (col.format) {
                                            case '{0: MM-dd-yyyy}':
                                                gridCell.text(FormatStandardDate(dt, 'MM-dd-yyyy'));
                                                break;
                                            case '{0: MM-dd-yyyy hh:mm tt}':
                                                gridCell.text(FormatStandardDate(dt, 'MM-dd-yyyy hh:mm tt'));
                                                break;
                                        }
                                        break;
                                    case 'check':
                                        gridCell.html('<input type="checkbox" ' + (item[col.field] ? 'checked="checked" ' : '') + '/><span></span>');
                                        break;
                                }
                            }
                            iGridCol++;
                        }
                        iItem++;
                    }

                    __ECMT_Grid.trigger('gridDataUpdated');
                }
            }
        }

        // COLUMN RESIZING
        {
            this.__ColumnResizeStartPosX = null;
            this.__ColumnResizeStartSizeX = null;
            this.__ColumnResizeDef = null;
            this.__ColumnHeader = null;
            this.__ColumnResizeDiv = null;
            this._ColumnResize = function (e) {
                e.stopPropagation();
                e.preventDefault();
                __ECMT_Grid.__ColumnResizeDiv = $(e.target);
                __ECMT_Grid.__ColumnResizeDiv.addClass('ecmt_vg_gridheaderresize_dragging');                
                document.addEventListener('mouseup', __ECMT_Grid._ColumnResize_Release);
                document.addEventListener('mousemove', __ECMT_Grid._ColumnResizing);
                var resizeDiv = $(e.target)

                var colID = resizeDiv.attr('coldefid');
                __ECMT_Grid.__ColumnResizeDef = $('#' + colID);
                __ECMT_Grid.__ColumnResizeStartPosX = e.pageX;
                __ECMT_Grid.__ColumnResizeStartSizeX = parseInt(__ECMT_Grid.__ColumnResizeDef.css('min-width').replace('px', ''))
                __ECMT_Grid.__ColumnHeader = $(e.target).parent();

            }
            this._ColumnResizing = function (e) {
                e.stopPropagation();
                e.preventDefault();
                var dif = e.pageX - __ECMT_Grid.__ColumnResizeStartPosX;
                var width = __ECMT_Grid.__ColumnResizeStartSizeX + dif;
                if (width < 50) width = 50;
                __ECMT_Grid.__ColumnResizeDef.css('min-width', width + 'px').css('max-width', width+'px');
                __ECMT_Grid.__ColumnHeader.css('min-width', width + 'px').css('max-width', width + 'px');
            }
            this._ColumnResize_Release = function (e) {
                e.stopPropagation();
                e.preventDefault();
                document.removeEventListener('mouseup', __ECMT_Grid._ColumnResize_Release);
                document.removeEventListener('mousemove', __ECMT_Grid._ColumnResizing);
                __ECMT_Grid.__ColumnResizeDiv.removeClass('ecmt_vg_gridheaderresize_dragging');
                var dif = e.pageX - __ECMT_Grid.__ColumnResizeStartPosX;
                var width = __ECMT_Grid.__ColumnResizeStartSizeX + dif;
                if (width < 50) width = 50;
                __ECMT_Grid.__ColumnResizeDef.css('min-width', width + 'px').css('max-width', width + 'px');
                __ECMT_Grid.__ColumnHeader.css('min-width', width + 'px').css('max-width', width + 'px');
                var fieldid = __ECMT_Grid.__ColumnResizeDef.attr('id')
                fieldid = fieldid.substring(8);

                var col = null;
                for (var i = 0; i < __ECMT_Grid._Columns.length; i = i + 1) {
                    if (__ECMT_Grid._Columns[i].field == fieldid) {
                        col = __ECMT_Grid._Columns[i];
                        __ECMT_Grid._Columns[i].width = width;
                        break;
                    }
                }

                var columnResizeEventArgs =
                {
                    columns: __ECMT_Grid._Columns,
                    column: col,
                    columnsize: width
                }
                __ECMT_Grid.trigger('columnResized', columnResizeEventArgs);
            }
        }

        // GRID RESIZING
        {
            this.resize = function () {
                var currItem = 0;
                if (!isNaN(__ECMT_Grid._ScrollItemNumber)) currItem = __ECMT_Grid._ScrollItemNumber;
                __ECMT_Grid._SetupGridArea();
                __ECMT_Grid.Scroll_Setup();
                __ECMT_Grid.ScrollToItem(currItem);
            }
        }
        
        // VIRTUAL SCROLLING
        {
            this.Scroll_Setup = function () {
                if (__ECMT_Grid._TotalItems == null || __ECMT_Grid._MaxVisibleItems == null || __ECMT_Grid._TotalItems == 0 || __ECMT_Grid._MaxVisibleItems == 0) { __ECMT_Grid._ScrollBar.hide(); return };
                
                if (__ECMT_Grid._TotalItems > __ECMT_Grid._MaxVisibleItems) {
                    __ECMT_Grid._ScrollBar.show();
                    var ScrollAreaHeight = __ECMT_Grid._ScrollBarScrollArea.innerHeight();
                    var ScrollerHeight = ScrollAreaHeight;
                    try { if (__ECMT_Grid._TotalItems > __ECMT_Grid._MaxVisibleItems) ScrollerHeight = __ECMT_Grid._ScrollBarScrollArea.innerHeight() / ((__ECMT_Grid._TotalItems / __ECMT_Grid._MaxVisibleItems)); } catch (ex) { ScrollerHeight = 60 }
                    if (ScrollerHeight < 60) ScrollerHeight = 60;
                    __ECMT_Grid._ScrollBarScroller.outerHeight(ScrollerHeight)
                    __ECMT_Grid._scrollableHeight = ScrollAreaHeight - ScrollerHeight;                
                    __ECMT_Grid._PixelsPerItem = __ECMT_Grid._scrollableHeight / (__ECMT_Grid._TotalItems - __ECMT_Grid._MaxVisibleItems);
                    __ECMT_Grid._ItemsPerPixel = (__ECMT_Grid._TotalItems - __ECMT_Grid._MaxVisibleItems) / __ECMT_Grid._scrollableHeight;                    
                }
                else {
                    __ECMT_Grid._ItemsPerPixel = 0;
                    __ECMT_Grid._PixelsPerItem = 0;
                    __ECMT_Grid._ScrollBar.hide();
                }
            }

            this.ResetScrollBar = function () {
                __ECMT_Grid._ScrollItemNumber = 0;
                __ECMT_Grid._ScrollBarScroller.css('top', 0);                
            }

            this.ScrollToItem = function (itemNumber) {
                if (itemNumber < 0) itemNumber = 0;
                if (itemNumber >= __ECMT_Grid._TotalItems - __ECMT_Grid._MaxVisibleItems) itemNumber = (__ECMT_Grid._TotalItems - __ECMT_Grid._MaxVisibleItems);
                __ECMT_Grid._ScrollItemNumber = itemNumber;
                var ypos = 0;
                ypos = itemNumber * __ECMT_Grid._PixelsPerItem;
                __ECMT_Grid._ScrollBarScroller.css('top', ypos);
                $('#testArea').text("Item Set To: " + itemNumber);
                __ECMT_Grid._PropagateGrid();
            }

            this.GetScrollItemNumber = function () {
                return __ECMT_Grid._ScrollItemNumber;
            }

            this.ItemCounterTag = null;
            this._Scroll_BeginMove = function (e) {
                try { e.stopPropagation(); } catch (ex) { }
                try { e.cancelBubble = true } catch (ex) { }
                __ECMT_Grid._scrollerStartY = __ECMT_Grid._ScrollBarScroller.position().top;
                __ECMT_Grid._draggingYStart = e.pageY;
                document.addEventListener('mouseup', __ECMT_Grid._Scroll_EndMove);
                document.addEventListener('mousemove', __ECMT_Grid._Scroll_Dragging);
                if (__ECMT_Grid.Settings.showItemCounterOnScroll) {
                    __ECMT_Grid.ItemCounterTag = $('<div id="itemCounterTag" class="ItemCounterTag"></div>');
                    __ECMT_Grid._ScrollBarScroller.append(__ECMT_Grid.ItemCounterTag);
                }

            }
            
            this._Scroll_Dragging = function (e) {
                var ypos = e.y - __ECMT_Grid._draggingYStart
                ypos = ypos + __ECMT_Grid._scrollerStartY;
                if (ypos < 0) ypos = 0;
                if (ypos > __ECMT_Grid._scrollableHeight) ypos = __ECMT_Grid._scrollableHeight;
                __ECMT_Grid._ScrollBarScroller.css('top', ypos);
                __ECMT_Grid._ScrollItemNumber = Math.round(__ECMT_Grid._ItemsPerPixel * ypos);
                if (__ECMT_Grid.Settings.showItemCounterOnScroll) {
                    __ECMT_Grid.ItemCounterTag.text((__ECMT_Grid._ScrollItemNumber + 1) + " to " + (__ECMT_Grid._ScrollItemNumber + __ECMT_Grid._MaxVisibleItems) + " of " + __ECMT_Grid._TotalItems)
                }
                __ECMT_Grid._PropagateGrid();
                //__ECMT_Grid._Scroll_Drag_IsDragging = true;
                //setTimeout(__ECMT_Grid._Scroll_ContinuePropogate, 50);
            }

            //this._Scroll_Drag_IsDragging = false;
            //this._Scroll_ContinuePropogate = function () {
            //    __ECMT_Grid._PropagateGrid();
            //    if (__ECMT_Grid._Scroll_Drag_IsDragging) {
            //        setTimeout(__ECMT_Grid._Scroll_ContinuePropogate, 50);
            //    }
            //}


            this._Scroll_EndMove = function (e) {
                __ECMT_Grid._Scroll_Drag_IsDragging = false;
                document.removeEventListener('mouseup', __ECMT_Grid._Scroll_EndMove)
                document.removeEventListener('mousemove', __ECMT_Grid._Scroll_Dragging);                                
                if (__ECMT_Grid.Settings.showItemCounterOnScroll) {
                    __ECMT_Grid.ItemCounterTag.remove();
                    __ECMT_Grid.ItemCounterTag = null;
                }
                __ECMT_Grid._draggingYStart = null;
            }
            


            this.__ContinueScrollAction = false;

            this._Scroll_StepUp = function () {
                __ECMT_Grid.__ContinueScrollAction = true;
                document.addEventListener('mouseup', __ECMT_Grid._Scroll_StepUp_Release);
                __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber - 1);
                setTimeout(__ECMT_Grid._PerformStepUp, 600);
            }
            this._PerformStepUp = function () {
                if (__ECMT_Grid.__ContinueScrollAction) {
                    __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber - 1);
                    setTimeout(__ECMT_Grid._PerformStepUp, 50);
                }
            }
            this._Scroll_StepUp_Release = function () {
                document.removeEventListener('mouseup', __ECMT_Grid._Scroll_StepUp_Release);
                __ECMT_Grid.__ContinueScrollAction = false;
            }

            this._Scroll_StepDown = function () {
                __ECMT_Grid.__ContinueScrollAction = true;
                document.addEventListener('mouseup', __ECMT_Grid._Scroll_StepDown_Release);
                __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber + 1);
                setTimeout(__ECMT_Grid._PerformStepDown, 600);
            }
            this._PerformStepDown = function () {
                if (__ECMT_Grid.__ContinueScrollAction) {
                    __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber + 1);
                    setTimeout(__ECMT_Grid._PerformStepDown, 50);
                }
            }
            this._Scroll_StepDown_Release = function () {
                document.removeEventListener('mouseup', __ECMT_Grid._Scroll_StepDown_Release);
                __ECMT_Grid.__ContinueScrollAction = false;
            }

            this.__ScrollAreaClickedMouseY = null;
            this._Scroll_ScrollAreaClicked = function (e) {
                __ECMT_Grid.__ContinueScrollAction = true;
                document.addEventListener('mouseup', __ECMT_Grid._Scroll_ScrollAreaClicked_Release);
                var scrollerTopY = __ECMT_Grid._ScrollBarScroller.position().top;
                var scrollerBottomY = scrollerTopY + __ECMT_Grid._ScrollBarScroller.outerHeight();
                __ECMT_Grid.__ScrollAreaClickedMouseY = e.pageY - __ECMT_Grid._ScrollBarScrollArea.offset().top;
                if (__ECMT_Grid.__ScrollAreaClickedMouseY < scrollerTopY) {
                    __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber - (__ECMT_Grid._MaxVisibleItems - 1));
                }
                if (__ECMT_Grid.__ScrollAreaClickedMouseY > scrollerBottomY) {
                    __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber + (__ECMT_Grid._MaxVisibleItems - 1));
                }
                setTimeout(__ECMT_Grid._PerformScrollAreaClicked, 600);
            }
            this._PerformScrollAreaClicked = function () {
                if (__ECMT_Grid.__ContinueScrollAction) {
                    var scrollerTopY = __ECMT_Grid._ScrollBarScroller.position().top;
                    var scrollerBottomY = scrollerTopY + __ECMT_Grid._ScrollBarScroller.outerHeight();
                    if (__ECMT_Grid.__ScrollAreaClickedMouseY < scrollerTopY) {
                        __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber - (__ECMT_Grid._MaxVisibleItems - 1));
                    }
                    if (__ECMT_Grid.__ScrollAreaClickedMouseY > scrollerBottomY) {
                        __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber + (__ECMT_Grid._MaxVisibleItems - 1));
                    }
                    setTimeout(__ECMT_Grid._PerformScrollAreaClicked, 100);
                }
            }
            this._Scroll_ScrollAreaClicked_Release = function (e) {
                document.removeEventListener('mouseup', __ECMT_Grid._Scroll_ScrollAreaClicked_Release);
                __ECMT_Grid.__ContinueScrollAction = false;
            }
            //this._ScrollBarMouseWheel = function(e){
            //    if (e.originalEvent.wheelDelta / 120 > 0) {
            //        __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber - 1);
            //    }
            //    else {
            //        __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber + 1);
            //    }
            //}
            this._ScrollBarWheel = function (e) {
                if (e.originalEvent.deltaY / 120 > 0) {
                    __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber + 1);
                }
                else {
                    __ECMT_Grid.ScrollToItem(__ECMT_Grid._ScrollItemNumber - 1);
                }
            }
        }
        
        // SELECTING
        {
            this._RowClicked = function (e) {
                var clickedRow = $(e.currentTarget);
                var dataid = $(clickedRow).attr('ecmt_data_id');
                var isSelected = false;
                var item = null;
                for (var i = 0; i < __ECMT_Grid._CurrentDataView.length; i = i + 1) {
                    if (__ECMT_Grid._CurrentDataView[i].ecmt_data_id == dataid) {
                        item = __ECMT_Grid._CurrentDataView[i];
                        isSelected = !__ECMT_Grid._CurrentDataView[i].ecmt_vg_selected;
                        __ECMT_Grid._CurrentDataView[i].ecmt_vg_selected = isSelected;
                        break;
                    }
                }
                __ECMT_Grid._SelectedItems = $.grep(__ECMT_Grid._CurrentDataView, function (item) { return item.ecmt_vg_selected; });
                if (isSelected) {
                    clickedRow.addClass('k-state-selected');
                    clickedRow.children().first().children().first().prop('checked', true);
                }
                else {
                    clickedRow.removeClass('k-state-selected');
                    clickedRow.children().first().children().first().prop('checked', false);
                }

                var selectedItemsChangedEventArgs = { SelectedItems: __ECMT_Grid._SelectedItems }
                __ECMT_Grid.trigger('selectedItemsChanged', selectedItemsChangedEventArgs);

            }

            this._SelectAllChanged = function (e) {
                e.stopPropagation();
                e.preventDefault();
                var selectAllChecked = $(e.target).is(':checked');
                for (var i = 0; i < __ECMT_Grid._CurrentDataView.length; i = i + 1) {
                    __ECMT_Grid._CurrentDataView[i].ecmt_vg_selected = selectAllChecked;
                }
                __ECMT_Grid._TableBody.find('.ecmt_vg_select_cb').prop('checked', selectAllChecked);
                if(selectAllChecked){
                    __ECMT_Grid._TableBody.find('tr').addClass('k-state-selected');
                }
                else{
                    __ECMT_Grid._TableBody.find('tr').removeClass('k-state-selected');
                }

                __ECMT_Grid._SelectedItems = $.grep(__ECMT_Grid._CurrentDataView, function (item) { return item.ecmt_vg_selected; });
                var selectedItemsChangedEventArgs = { SelectedItems: __ECMT_Grid._SelectedItems }
                __ECMT_Grid.trigger('selectedItemsChanged', selectedItemsChangedEventArgs);
            }

            this.GetSelectedItems = function () {
                return __ECMT_Grid._SelectedItems;
            }
        }

        // ROW DOUBLE CLICK
        {
            this._RowDoubleClicked = function (e) {
                var clickedRow = $(e.currentTarget);
                var dataid = $(clickedRow).attr('ecmt_data_id');

                var item = null;
                for (var i = 0; i < __ECMT_Grid._CurrentDataView.length; i = i + 1) {
                    if (__ECMT_Grid._CurrentDataView[i].ecmt_data_id == dataid) {
                        item = __ECMT_Grid._CurrentDataView[i];
                        __ECMT_Grid._CurrentDataView[i].ecmt_vg_selected = true;
                    }
                    else {
                        __ECMT_Grid._CurrentDataView[i].ecmt_vg_selected = false;
                    }
                }
                __ECMT_Grid._SelectedItems = [item];

                __ECMT_Grid._TableBody.children().removeClass('k-state-selected');
                __ECMT_Grid._TableBody.children().children().first().prop('checked', false);

                clickedRow.addClass('k-state-selected');
                clickedRow.children().children().first().prop('checked', true);

                var rowDoubleClickedEventArgs = { Item: item };
                __ECMT_Grid.trigger('rowDoubleClick', rowDoubleClickedEventArgs);
            }
        }

        // ROW CONTEXT MENU
        {
            this._RowContextMenu = function (e) {
                e.stopPropagation();
                e.preventDefault();
                var clickedRow = $(e.currentTarget);
                var dataid = $(clickedRow).attr('ecmt_data_id');
                var wasSelected = false;
                var item = null;
                for (var i = 0; i < __ECMT_Grid._CurrentDataView.length; i = i + 1) {
                    if (__ECMT_Grid._CurrentDataView[i].ecmt_data_id == dataid) {
                        item = __ECMT_Grid._CurrentDataView[i];
                        wasSelected = __ECMT_Grid._CurrentDataView[i].ecmt_vg_selected;
                        __ECMT_Grid._CurrentDataView[i].ecmt_vg_selected = true;
                        break;
                    }
                }

                if (!wasSelected) {
                    __ECMT_Grid._SelectedItems = $.grep(__ECMT_Grid._CurrentDataView, function (item) { return item.ecmt_vg_selected; });
                    clickedRow.addClass('k-state-selected');
                    clickedRow.children().first().children().first().prop('checked', true);
                }

                var rowRightClickedEventArgs = { SelectedItems: __ECMT_Grid._SelectedItems, x: e.pageX, y: e.pageY };
                __ECMT_Grid.trigger('rowRightClicked', rowRightClickedEventArgs);

            }
        }

        // SORTING
        {            
            this._ColumnSort = function (e) {
                var header = $(e.target);
                if (!header.hasClass('ecmt_vg_gridheaderresize')) {
                    if (!header.hasClass('ecmt_vg_gridheader')) header = $(header.parent());
                    var field = header.attr('ecmt_vg_data_field');
                    if (!(field == null || field == '')) {
                        e.stopPropagation();
                        e.preventDefault();
                        var column = null;
                        var direction = null;


                        for (var i = 0; i < __ECMT_Grid._Columns.length; i = i + 1) {
                            if (__ECMT_Grid._Columns[i].field == field) {
                                column = __ECMT_Grid._Columns[i];
                            }
                        }
                        if (column != null) {

                            __ECMT_Grid._TableHeaders.find('.ecmt_vg_gridsort').removeClass('fa-long-arrow-down').removeClass('fa-long-arrow-up')
                            direction = header.attr('sortDirection');
                            directionIndicator = header.find('.ecmt_vg_gridsort');
                            if (direction == null) {
                                direction = 'ASC';
                                header.attr('sortDirection', direction);
                                directionIndicator.addClass('fa-long-arrow-down');
                                __ECMT_Grid._Sort = { column: column, direction: direction };
                            }
                            else if (direction == 'ASC') {
                                direction = 'DESC';
                                header.attr('sortDirection', direction);
                                directionIndicator.addClass('fa-long-arrow-up');
                                __ECMT_Grid._Sort = { column: column, direction: direction };
                            }
                            else {
                                direction = "NONE";
                                header.removeAttr('sortDirection');
                                __ECMT_Grid._Sort = null;
                            }

                            __ECMT_Grid._UpdateGridView();

                            var gridSortedEventArgs = { name: column.title, field: column.field, direction: direction };
                            __ECMT_Grid.trigger('gridSorted', gridSortedEventArgs);
                        }
                    }
                }
            }

            this.GetSort = function () {
                if (__ECMT_Grid._Sort == null) return null;
                return { name: __ECMT_Grid._Sort.column.title, field: __ECMT_Grid._Sort.column.field, direction: __ECMT_Grid._Sort.direction }
            }

            this.SetSort = function (sort, updateView) {
                try{
                    if (updateView == null) updateView = true;
                    __ECMT_Grid._TableHeaders.find('.ecmt_vg_gridsort').removeClass('fa-long-arrow-down').removeClass('fa-long-arrow-up')
                    __ECMT_Grid._TableHeaders.find('th[sortDirection]').removeAttr('sortDirection');
                    __ECMT_Grid._Sort = null;

                    if (sort != null) {
                        var column = null;
                        for (var iCol = 0; iCol < __ECMT_Grid._Columns.length; iCol = iCol + 1) {
                            if (__ECMT_Grid._Columns[iCol].field == sort.field) {
                                column = __ECMT_Grid._Columns[iCol];
                                break;
                            }
                        }
                        if (column != null && sort.direction != null && (sort.direction == 'ASC' || sort.direction == 'DESC')) {
                            __ECMT_Grid._Sort = {
                                column: column,
                                direction: sort.direction
                            };
                            var header = $(__ECMT_Grid._TableHeaders.find('th[ecmt_vg_data_field=' + column.field + ']'));
                            header.attr('sortDirection', sort.direction);
                            header.find('.ecmt_vg_gridsort').addClass(sort.direction == "ASC" ? "fa-long-arrow-down" : "fa-long-arrow-up");                            
                        }
                    }
                    if (updateView) {
                        __ECMT_Grid = _UpdateGridView();
                    }
                }
                catch (ex) {
                    ECMNotification('Error Setting Sort: ' + ex);
                    __ECMT_Grid._TableHeaders.find('.ecmt_vg_gridsort').removeClass('fa-long-arrow-down').removeClass('fa-long-arrow-up')
                    __ECMT_Grid._TableHeaders.find('th[sortDirection]').removeAttr('sortDirection');
                    __ECMT_Grid._Sort = null;
                    try { __ECMT_Grid = _UpdateGridView(); } catch (exc) { }
                }
            }
        }

        // HEADER CONTEXT MENU
        {
            this._HeaderContextMenu = function (e) {
                e.stopPropagation();
                e.preventDefault();
                __ECMT_Grid.trigger('headerMenuOpened', __ECMT_Grid._Columns, e.pageX, e.pageY);
            }
        }

        // FILTERING
        {
            this._ShowFilter = function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(e.target).addClass('grid-filter-changing');
                var offset = $(e.target).offset();
                var field = $(e.target).parent().attr('ecmt_vg_data_field');
                var column = null;
                for (var i = 0; i < __ECMT_Grid._Columns.length; i = i + 1) {
                    if (__ECMT_Grid._Columns[i].field == field) {
                        column = __ECMT_Grid._Columns[i];
                    }
                }

                var existingFilter = null;
                for (var i = 0; i < __ECMT_Grid._Filters.length; i = i + 1) {
                    if (__ECMT_Grid._Filters[i].column.field == column.field) {
                        existingFilter = __ECMT_Grid._Filters[i];
                        break;
                    }
                }
                if (existingFilter == null) {
                    ecmt_cg_filters_ShowFilter(column, __ECMT_Grid._SetFilter, __ECMT_Grid._ClearFilter, offset.left, offset.top, []);
                }
                else {
                    ecmt_cg_filters_ShowFilter(column, __ECMT_Grid._SetFilter, __ECMT_Grid._ClearFilter, offset.left, offset.top, existingFilter.args);
                }
            }

            this._SetFilter = function (col, args) {
                var filterFound = false;
                for (var i = 0; i < __ECMT_Grid._Filters.length; i = i + 1) {
                    if (__ECMT_Grid._Filters[i].column.field == col.field) {
                        __ECMT_Grid._Filters[i].args = args;
                        filterFound = true;
                        break;
                    }
                }
                if (!filterFound) {
                    __ECMT_Grid._Filters[__ECMT_Grid._Filters.length] = { column: col, args: args };
                }
                __ECMT_Grid.ResetScrollBar();
                __ECMT_Grid._UpdateGridView();
                __ECMT_Grid._SetFilterHighlights();
                __ECMT_Grid.trigger('gridFilterChanged', { filters: __ECMT_Grid.GetFilters() });
            }
            this._ClearFilter = function (col) {
                for (var i = 0; i < __ECMT_Grid._Filters.length; i = i + 1) {
                    if (__ECMT_Grid._Filters[i].column.field == col.field) {
                        __ECMT_Grid._Filters.splice(i, 1);
                        break;
                    }
                }
                __ECMT_Grid._UpdateGridView();
                __ECMT_Grid._SetFilterHighlights();
                __ECMT_Grid.trigger('gridFilterChanged', { filters: __ECMT_Grid.GetFilters() });
            }

            this._SetFilterHighlights = function (e) {
                __ECMT_Grid._TableHeaders.children().find('.ecmt_vg_gridheaderfiltericon').removeClass('grid-filter-active');;

                for (var iFilter = 0; iFilter < __ECMT_Grid._Filters.length; iFilter = iFilter + 1) {
                    __ECMT_Grid._TableHeaders.find('th[ecmt_vg_data_field=' + __ECMT_Grid._Filters[iFilter].column.field + ']').find('.ecmt_vg_gridheaderfiltericon').addClass('grid-filter-active');
                }
            }

            this.ClearFilters = function () {
                __ECMT_Grid._Filters = [];
                __ECMT_Grid._UpdateGridView();
                __ECMT_Grid._SetFilterHighlights();
                __ECMT_Grid.trigger('gridFilterChanged', { filters: __ECMT_Grid.GetFilters() });
            }

            this.GetFilters = function () {
                var filters = []
                for (var i = 0; i < __ECMT_Grid._Filters.length; i = i + 1) {
                    filters[i] = {
                        name: __ECMT_Grid._Filters[i].column.title,
                        field: __ECMT_Grid._Filters[i].column.field,
                        values: __ECMT_Grid._Filters[i].args
                    }
                }
                return filters;
            }

            this.SetFilters = function (filters, updateView) {
                if (updateView == null) updateView = true;
                if (filters == null) filters = [];
                else if (filters.length === undefined || filters.length == null) filters = [filters];
                
                try{
                    __ECMT_Grid._Filters = [];
                    var filterCounter = 0;
                    for (var i = 0; i < filters.length; i = i + 1) {
                        var column = null;
                        for (var iCol = 0; iCol < __ECMT_Grid._Columns.length; iCol = iCol + 1) {
                            if (__ECMT_Grid._Columns[iCol].field == filters[i].field) {
                                column = __ECMT_Grid._Columns[iCol];
                                break;
                            }
                        }
                        if (column != null) {
                            __ECMT_Grid._Filters[filterCounter] = {
                                column: column,
                                args: filters[i].values
                            }
                            filterCounter++;
                        }
                    }
                    __ECMT_Grid._SetFilterHighlights();
                    if (updateView) {
                        __ECMT_Grid._UpdateGridView();
                    }
                }
                catch (e) {
                    ECMNotification('Error Setting Filters: ' + e, 3);
                    __ECMT_Grid._Filters = [];
                    try { __ECMT_Grid._UpdateGridView(); } catch (ex) { }
                }
            }

            //FilterTypes
            {
                this._Filter_Type_String = function (array, filter) {
                    var field = filter.column.field;
                    var value = filter.args[0].toUpperCase();
                    return $.grep(array, function (item) { return item[field] != null && item[field].toUpperCase().indexOf(value) != -1; });
                }
                this._Filter_Type_Number = function (array, filter) {
                    var field = filter.column.field;
                    var value = Number(filter.args[0]);
                    if (filter.args[1] == 'Equals') {
                        return $.grep(array, function (item) { return item[field] != null && item[field] == value; });
                    }
                    else if (filter.args[1] == 'Greater Than') {
                        return $.grep(array, function (item) { return item[field] != null && item[field] > value; });
                    }
                    else {
                        return $.grep(array, function (item) { return item[field] != null && item[field] < value; });
                    }
                }
                this._Filter_Type_Check = function (array, filter) {
                    var field = filter.column.field;
                    var value = filter.args[0] == 'Checked';
                    if (value) {
                        return $.grep(array, function (item) { return item[field] != null && item[field] });
                    }
                    else {
                        return $.grep(array, function (item) { return item[field] != null && !item[field] });
                    }
                }
                this._Filter_Type_Date = function (array, filter) {
                    var field = filter.column.field;
                    var from = new Date(filter.args[0]);
                    var to = new Date(filter.args[1]);
                    to.setDate(to.getDate() + 1);
                    return $.grep(array, function (item) { return item[field] != null && item[field] > from && item[field] < to });
                }
                this._Filter_Type_Icons = function (array, filter) {
                    var field = filter.column.field;
                    var args = filter.args;
                    return $.grep(array, function (item) { return item[field] != null && args.some(function (v) { return item[field].indexOf(v) >= 0 }) });
                }
            }
        }

        // REORDERING
        {
            var ecmt_vg__reorder_originalDragItem;
            var ecmt_vg_reorder_drag_location_startX;
            var ecmt_vg_reorder_drag_parentX;
            var ecmt_vg_reorder_drag_parentY;
            var ecmt_vg_reorder_isdragging;
            var ecmt_vg_reorder_hint;
            var ecmt_vg_reorder_line;
            var ecmt_vg_reorder_col_info;
            var ecmt_vg_reorder_current_col_info;
            var ecmt_vg_reorder_current_col_midX;
            var ecmt_vg_reorder_grid_container_offset;
            
            this._ColumnReorder_Begin = function (e) {
                e.stopPropagation();
                e.preventDefault();
                ecmt_vg__reorder_originalDragItem = $(e.target);
                if (!ecmt_vg__reorder_originalDragItem.hasClass('k-grid-filter')) ecmt_vg__reorder_originalDragItem = $(ecmt_vg__reorder_originalDragItem.parent());                
                ecmt_vg_reorder_drag_location_startX = e.pageX;
                ecmt_vg_reorder_drag_parentX = e.pageX - ecmt_vg__reorder_originalDragItem.offset().left;
                ecmt_vg_reorder_drag_parentY = e.pageY - ecmt_vg__reorder_originalDragItem.offset().top;
                ecmt_vg_reorder_isdragging = false;
                document.addEventListener('mouseup', __ECMT_Grid._ColumnReorder_End);
                document.addEventListener('mousemove', __ECMT_Grid._ColumnReorder_MouseMove);
            }
                        
            this._ColumnReorder_MouseMove = function (e) {
                var difX = ecmt_vg_reorder_drag_location_startX-e.pageX;
                if (!ecmt_vg_reorder_isdragging) {
                    if (difX > 20 || difX < -20) {
                        //Start Drag
                        ecmt_vg_reorder_isdragging = true;
                        var hint = $('<th class="k-grid-filter ecmt_vg_gridheader"></th>');                        
                        hint.css('min-width', ecmt_vg__reorder_originalDragItem.outerWidth()).css('min-height', ecmt_vg__reorder_originalDragItem.outerHeight());
                        hint.html(ecmt_vg__reorder_originalDragItem.html());
                        ecmt_vg_reorder_hint = $('<div class="k-grid-header-wrap grid-reorder-hint"><table><thead><tr></tr></thead></table></div>');
                        ecmt_vg_reorder_hint.css('left', e.pageX - ecmt_vg_reorder_drag_parentX).css('top', e.pageY - ecmt_vg_reorder_drag_parentY);
                        ecmt_vg_reorder_hint.children().first().children().first().children().first().append(hint);
                        
                        ecmt_vg_reorder_current_col_midX = ecmt_vg__reorder_originalDragItem.outerWidth() / 2;
                        var headers = __ECMT_Grid._TableHeaders.children().first().children();
                        ecmt_vg_reorder_col_info = [];
                        for (var i = 1; i < headers.length; i = i + 1) {
                            var h = $(headers[i]);
                            if(h.attr('ecmt_vg_data_field')==ecmt_vg__reorder_originalDragItem.attr('ecmt_vg_data_field')){
                                ecmt_vg_reorder_current_col_info = i;
                            }
                            else {
                                ecmt_vg_reorder_col_info[i - 1] = h.offset().left + (h.outerWidth() / 2);
                            }
                        }
                        $('body').append(ecmt_vg_reorder_hint);
                        ecmt_vg_reorder_line = $('<div class="grid-reorder-line"></div>');
                        ecmt_vg_reorder_line.css('height', __ECMT_Grid._itemTable.outerHeight());
                        __ECMT_Grid._gridContainer.append(ecmt_vg_reorder_line);
                        ecmt_vg_reorder_grid_container_offset = __ECMT_Grid._gridContainer.offset().left;
                    }
                }
                else {
                    ecmt_vg_reorder_hint.css('left', e.pageX - ecmt_vg_reorder_drag_parentX).css('top', e.pageY - ecmt_vg_reorder_drag_parentY);
                    var currentPositionX = e.pageX - ecmt_vg_reorder_drag_parentX + ecmt_vg_reorder_current_col_midX;
                    var headerIndex;
                    for (var i = 0; i < ecmt_vg_reorder_col_info.length; i = i + 1) {
                        if (ecmt_vg_reorder_col_info[i] > currentPositionX) {
                            headerIndex = i + 1;
                            break;
                        }
                    }
                    var x = $(__ECMT_Grid._TableHeaders.children().first().children()[headerIndex]).offset().left;
                    x = x - ecmt_vg_reorder_grid_container_offset;
                    ecmt_vg_reorder_line.css('left', x-1);
                }
            }
            this._ColumnReorder_End = function (e) {
                document.removeEventListener('mouseup', __ECMT_Grid._ColumnReorder_End);
                document.removeEventListener('mousemove', __ECMT_Grid._ColumnReorder_MouseMove);
                
                if (ecmt_vg_reorder_isdragging) {
                    ecmt_vg_reorder_hint.remove();
                    ecmt_vg_reorder_line.remove();

                    var currentPositionX = e.pageX - ecmt_vg_reorder_drag_parentX + ecmt_vg_reorder_current_col_midX;
                    var headerIndex;
                    ecmt_vg_reorder_col_info = $.grep(ecmt_vg_reorder_col_info, function (item) { return item != null; });
                    for (var i = 0; i < ecmt_vg_reorder_col_info.length; i = i + 1) {
                        if (ecmt_vg_reorder_col_info[i] > currentPositionX) {
                            headerIndex = i + 1;
                            break;
                        }
                    }
                    if (headerIndex != ecmt_vg_reorder_current_col_info) {
                        var fromcol = __ECMT_Grid._VisibleColumns.splice(ecmt_vg_reorder_current_col_info, 1)[0];
                        var tocol = __ECMT_Grid._VisibleColumns.splice(headerIndex, 1)[0];
                        var fromIndex = 0;
                        var toIndex = 0;
                        for (var i = 0; i < __ECMT_Grid._Columns.length; i = i + 1) {
                            if (__ECMT_Grid._Columns[i].field == fromcol.field && !__ECMT_Grid._Columns[i].hidden) {
                                fromIndex = i;
                                break;
                            }
                        }
                        for (var i = 0; i < __ECMT_Grid._Columns.length; i = i + 1) {
                            if (__ECMT_Grid._Columns[i].field == tocol.field && !__ECMT_Grid._Columns[i].hidden) {
                                toIndex = i;
                                break;
                            }
                        }

                        var movingcol = __ECMT_Grid._Columns.splice(fromIndex, 1)[0];
                        if (fromIndex < toIndex) toIndex = toIndex - 1;
                        __ECMT_Grid._Columns.splice(toIndex, 0, movingcol);

                        __ECMT_Grid._VisibleColumns.splice(headerIndex, 0, movingcol);
                        __ECMT_Grid._SetupColumns(__ECMT_Grid._Columns);
                        __ECMT_Grid._SetupGridArea();
                        __ECMT_Grid.Scroll_Setup();
                        __ECMT_Grid._PropagateGrid();

                        var columnsReorderedEventArgs = { column: movingcol, oldPosition: ecmt_vg_reorder_current_col_info, newPosition: headerIndex, columns: __ECMT_Grid._Columns };
                        __ECMT_Grid.trigger('columnsReordered', columnsReorderedEventArgs);
                    }
                    __ECMT_Grid._SetFilterHighlights();
                    ecmt_vg_reorder_isdragging = false;
                }
            }

            //ecmt_vg_preventSort
        }

        // EXPORT TO EXCEL
        {
            this.ExportToExcel = function() {
                if (__ECMT_Grid._SelectedItems != null && __ECMT_Grid._SelectedItems.length != 0) {
                    __ECMT_Grid._ExportToExcel(__ECMT_Grid._SelectedItems);
                }
                else if (__ECMT_Grid._CurrentDataView != null && __ECMT_Grid._CurrentDataView.length != 0) {
                    __ECMT_Grid._ExportToExcel(__ECMT_Grid._CurrentDataView);
                }
                else {
                    ECMNotification('No Data To Export', 1);
                }
            }

            this.ExportViewToExcel = function () {
                if (__ECMT_Grid._CurrentDataView != null && __ECMT_Grid._CurrentDataView.length != 0) {
                    __ECMT_Grid._ExportToExcel(__ECMT_Grid._CurrentDataView);
                }
                else {
                    ECMNotification('No Data To Export', 1);
                }                
            }

            this.ExportAllToExcel = function () {
                if (__ECMT_Grid._Data != null && __ECMT_Grid._Data.length != 0) {
                    __ECMT_Grid._ExportToExcel(__ECMT_Grid._Data);
                }
                else {
                    ECMNotification('No Data To Export', 1);
                }
            }

            this._ExportToExcel = function (data) {
                try {

                    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";

                    var exportColumns = $.grep(__ECMT_Grid._Columns, function (e) { return (!e.hidden && e.type != 'select' && e.type != 'icons'); });
                    tab_text = tab_text + '<tr>';
                    var iCol;
                    for (iCol = 0; iCol < exportColumns.length; iCol = iCol + 1) {
                        tab_text = tab_text + '<th>' + $('<div/>').text(exportColumns[iCol].title).html() + '</th>'
                    }
                    tab_text = tab_text + '</tr>';

                    var colText;
                    for (var iRow = 0; iRow < data.length; iRow = iRow + 1) {
                        tab_text = tab_text + '<tr>';
                        for (iCol = 0; iCol < exportColumns.length; iCol = iCol + 1) {
                            colText = data[iRow][exportColumns[iCol].field];
                            if (colText == null) colText = '';
                            else {
                                switch (exportColumns[iCol].type) {
                                    case 'number':
                                        switch (exportColumns[iCol].format) {
                                            case '{0:c2}':
                                                colText = '$' + parseFloat(colText).toFixed(2);
                                                break;
                                            default:
                                                colText = colText.toString();
                                                break;
                                        }
                                        break;                             
                                    case 'date':
                                        switch (exportColumns[iCol].format) {
                                            case '{0: MM-dd-yyyy}':
                                                colText = FormatStandardDate(colText, 'MM-dd-yyyy');
                                                break;
                                            case '{0: MM-dd-yyyy hh:mm tt}':
                                                colText = FormatStandardDate(colText, 'MM-dd-yyyy hh:mm tt');
                                                break;
                                        }
                                        break;
                                    case 'check':
                                        colText=colText?"True":"False";
                                        break;
                                }
                            }
                            tab_text = tab_text + '<td>' + $('<div/>').text(colText).html() + '</td>'
                        }
                        tab_text = tab_text + '</tr>';
                    }

                    tab_text = tab_text + "</table>";
                    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                    var filename = "ECMToolbox Workflow Grid Data.xlsx";                    
                    var ws_name = "WorkItem Grid Export";                    
                    var elt = $(tab_text).get(0);
                    var wb = XLSX.utils.table_to_book(elt, { sheet: ws_name });
                    XLSX.writeFile(wb, filename);
                    
                }
                catch (ex) {
                    var s = test;
                }
            }
        }
        
    } 
    
    // Base Objects and Event Handlers
    {
        this._gridContainer = $('<div class="ecmt_vg_container"></div>');
        this._tableContainer = $('<div class="ecmt_vg_tablearea k-grid-header-wrap"></div>')
        this._itemTable = $('<table border="1" class="ecmt_vg_gridtable"><colgroup id="ecmt_vg_gridColDefs"></colgroup><thead id="ecmt_vg_gridHeaders"></thead><tbody id="ecmt_vg_gridBody"></tbody></table>')
        this._ScrollBar = $('<div class="ecmt_vg_scroll_full"></div>')
        this._ScrollBarStepUp = $('<div class="ecmt_vg_scroll_step_up"></div>');
        this._ScrollBarScrollArea = $('<div class="ecmt_vg_scroll_slide_area"></div>');
        this._ScrollBarScroller = $('<div class="ecmt_vg_scroll_slider"></div>');
        this._ScrollBarStepDown = $('<div class="ecmt_vg_scroll_step_down"></div>');
        this._ExportToExcel_Iframe = $('<iframe style="display:none;"></iframe>')
        this._ScrollBarScroller.on('mousedown', __ECMT_Grid._Scroll_BeginMove);
        this._ScrollBarStepUp.on('mousedown', __ECMT_Grid._Scroll_StepUp);
        this._ScrollBarStepDown.on('mousedown', __ECMT_Grid._Scroll_StepDown);
        this._ScrollBarScrollArea.on('mousedown', __ECMT_Grid._Scroll_ScrollAreaClicked);
        


        this.append(this._gridContainer);
        this._gridContainer.append(this._ScrollBar);
        this._gridContainer.append(this._tableContainer);
        this._gridContainer.append(this._ExportToExcel_Iframe);
        this._tableContainer.append(this._itemTable);
        this._ScrollBar.append(this._ScrollBarStepUp);
        this._ScrollBarScrollArea.append(this._ScrollBarScroller);
        this._ScrollBar.append(this._ScrollBarScrollArea);
        this._ScrollBar.append(this._ScrollBarStepDown);

        this._TableCols = $('#ecmt_vg_gridColDefs');
        this._TableHeaders = $('#ecmt_vg_gridHeaders');
        this._TableBody = $('#ecmt_vg_gridBody');
        this._TableBody.delegate('tr', 'click', __ECMT_Grid._RowClicked);
        this._TableBody.delegate('tr', 'dblclick', __ECMT_Grid._RowDoubleClicked);
        this._TableBody.delegate('tr', 'contextmenu', __ECMT_Grid._RowContextMenu);
        this._TableHeaders.delegate('th', 'click', __ECMT_Grid._ColumnSort);
        this._TableHeaders.delegate('th', 'mousedown', __ECMT_Grid._ColumnReorder_Begin);
        this._TableHeaders.delegate('th', 'contextmenu', __ECMT_Grid._HeaderContextMenu);
        this._TableHeaders.delegate('th>i.ecmt_vg_gridheaderfiltericon', 'click', __ECMT_Grid._ShowFilter);

        //this._TableBody.bind('mousewheel', __ECMT_Grid._ScrollBarMouseWheel);
        this._TableBody.bind('wheel', __ECMT_Grid._ScrollBarWheel);
    }
        
    __ECMT_Grid.resize();
    return __ECMT_Grid;
}

$(document).ready(function () {
    //Load Filters HTML page from same directory as current script into div on current page
    {
        function _GetURLOfGridScript() {
            var jsfile = 'ECMToolboxVirtualGrid.js';
            var scriptElements = document.getElementsByTagName('script');
            var i, element, myfile;

            for (i = 0; element = scriptElements[i]; i++) {

                myfile = element.src;

                if (myfile.indexOf(jsfile) >= 0) {
                    var myurl = myfile.substring(0, myfile.indexOf(jsfile));

                }
            }
            return myurl;
        }
        var _FiltersDiv = $('<div style="display:none;"></div>').load(_GetURLOfGridScript() + 'ECMToolboxVirtualGrid_Filters.html');
        $('body').append(_FiltersDiv);
    }
});