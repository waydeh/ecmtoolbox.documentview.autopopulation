﻿
var grid = null;

var ShowFilterRow = false;
var GridSetup=false;
var DataLoaded=false;
var GridData = null;
var CurrentlyDisplayedWorkflow = null;
var CurrentlyDisplayedQueue = null;
var Columns = null;

function LoadGrid() {
    try{
        GridSetup = false;
        DataLoaded = false;

        LoadGridData();
            
        if (CurrentlyDisplayedWorkflow != selectedWorkflowID || CurrentlyDisplayedQueue != selectedQueueID) {
            if (grid != null) {
                $("#workItemsGrid").data("kendoGrid").destroy();
                $("#workItemsGrid").empty();
                $("#workItemsGrid").unbind();
            }
            CurrentlyDisplayedWorkflow = selectedWorkflowID;
            CurrentlyDisplayedQueue = selectedQueueID;
        

            var clientSettingColumns = null;
            if (selectedQueueID == -2) {
                Columns = WorkflowQueueColumns[selectedWorkflowID].GroupBox;
                try { clientSettingColumns =ClientSettings.GridSettings[selectedWorkflowID].GroupBox; } catch (e) { }
            }
            else if (selectedQueueID == -1) {
                Columns = WorkflowQueueColumns[selectedWorkflowID].Inbox
                try { clientSettingColumns =ClientSettings.GridSettings[selectedWorkflowID].Inbox; } catch (e) { }
            }
            else {
                Columns = WorkflowQueueColumns[selectedWorkflowID].Queues[selectedQueueID];
                try { clientSettingColumns =ClientSettings.GridSettings[selectedWorkflowID].Queues[selectedQueueID]; } catch (e) { }
            }

            var disableGridPersistance = !(ClientSettings === undefined || ClientSettings == null || ClientSettings.DisableGridPersistance === undefined || ClientSettings.DisableGridPersistance == null || ClientSettings.DisableGridPersistance == false);
            if (clientSettingColumns != null && !disableGridPersistance) {
                Columns = CombineColumnsWithClientSettings(Columns, clientSettingColumns);
            }
            
            var selectableAttribute = { template: '<input class="k-checkbox" type="checkbox"></input><label class="k-checkbox-label k-no-text"></label>', headerTemplate: '<input class="k-checkbox" id="k-selection-column" onclick="SelectAll(this);" type="checkbox"></input><label for="k-selection-column" class="k-checkbox-label k-no-text"></label>', width: 30 };
            Columns.splice(0, 0, selectableAttribute);
            
            kendoUniqueID = 0;
            grid = $("#workItemsGrid").kendoGrid({            
                columns: Columns,
                autoBind: false,
                height: '100%',
                filterable: true,              
                columnReorder: ColumnsReordered,
                columnResize: ColumnsResized,
                sortable: true,
                resizable: true,
                reorderable: true,
                //scrollable: true,
                scrollable: {
                    virtual: true
                },
                filterMenuInit: CreateSpecialFilters, 
                filterMenuOpen: ShowSpecialFilters,
                filter: onFiltering,
                dataBound: function (e) {
                    //-- reselect rows
                    var gridData = grid.data("kendoGrid");                    
                    var rows = $(gridData.tbody).find("tr");
                    var data = gridData.dataSource.view();
                    $.each(rows, function (i, row_i) {
                        var row = $(row_i);
                        var checkbox = $(row.find('input'));
                        var duid = row.attr('data-uid')
                        var item = FindDataFromUID(duid);
                        if (item!=null) {
                            row.addClass('k-state-selected');
                            checkbox.prop('checked', true);
                        }
                        else {
                            row.removeClass('k-state-selected');
                            if (data[i].BG != '') $(rows[i]).css("background-color", data[i].BG);
                            checkbox.prop('checked', false);
                        }
                    });
                    $('#workItemsGrid i[data-toggle="tooltip"]').tooltip({ container: 'body' });
                }
                //,
                //change: function (e) {
                    //var scrollContentOffset = this.element.find("tbody").offset().top;
                    //var selectContentOffset = this.select().offset().top;
                    //var distance = selectContentOffset - scrollContentOffset;

                    //this.element.find(".k-grid-content").animate({
                    //    scrollTop: distance
                    //}, 400);
                //}
            });

            

            grid.on("contextmenu", ".k-virtual-scrollable-wrap table tbody tr", function (e) {
                var targetID = e.currentTarget.getAttribute("data-uid");
                if (!IsItemSelected(e)) {
                    HandleRowClick(e);
                    DetermineActiveFunctions();
                }
                ShowContextMenu(e);
                e.preventDefault();
            });
                        
            grid.on("dblclick", ".k-virtual-scrollable-wrap table tbody tr", function (e) {
                //stops other click events from going through after this function
                e.preventDefault();
                e.stopPropagation();
                //Clears all current selections
                selectedItems = [];
                grid.find('tbody>tr').removeClass('k-state-selected');
                grid.find('tbody>tr>td>input').prop('checked', false);
                $('#k-selection-column').prop('checked', false);
                //Selects current row
                HandleRowClick(e);
                DetermineActiveFunctions();
                //Opens Item (if possible)
                Open(true);
            });

            grid.on("click", ".k-virtual-scrollable-wrap table tbody tr", function (e) {
                HandleRowClick(e);
                DetermineActiveFunctions();
            });

            //-- select all header checkbox
            grid.on("click", ".k-checkbox", function (e) {
                selectedItems = [];
                if (e.currentTarget.checked) {
                    var dataSource = grid.data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    selectedItems = query.filter(filters).data;
                }
                DetermineActiveFunctions();
            });

            //-- prevents text highlighting on rows
            grid.on("mousedown", function (e) {
                e.preventDefault();
            })

            ////replace poorly rendering png filters
            //if (!ShowFilterRow) $('.k-filter').removeClass('k-icon').removeClass('k-filter').addClass('fa').addClass('fa-filter');
            
        }
        else {
            grid.data("kendoGrid").dataSource.data([]);
        }
        if (DataLoaded) {
            SetGridData();
            DetermineActiveFunctions(null);
        }
        else {
            GridSetup = true;
        }
    }
    catch (ex) {
        ECMNotification(ex, 3);
        HideWaitSpinner();
    }
}

var IconFilterHtml = $('#iconFilter').html();
function CreateSpecialFilters(e) {
    if (e.field == 'IC') {
        var cont = $(e.container)
        cont.html(IconFilterHtml);
        var special
        var icons = LoadSetupIcons();
        if (icons.length == 0) {
            cont.find('#tdSpecialIconsSection').hide();
        }
        else {
            var specialIconsDiv = cont.find('#divSpecialIcons');
            $.each(icons, function (i, item) {
                var filterItem = $("<span></span>");
                var iteminput = $("<input type='checkbox' style='display:inline;' />");
                iteminput.attr('filterItemID', item.Name);
                iteminput.attr('name', e.field);
                iteminput.attr('value', item.Name);
                iteminput.css('width', 30);
                //iteminput.prop('checked', SelectedIconFilters.indexOf(item.Name) != -1)
                filterItem.append(iteminput);
                var icn = $(item.Html);
                icn.css('width', 30);
                filterItem.append(icn);
                filterItem.append(item.Name);
                specialIconsDiv.append(filterItem);
                specialIconsDiv.append('<br />');
            });
        }
    }
}

function ShowSpecialFilters(e) {
    if (e.field == 'IC') {

    }
}

function AddIconFilters(){
    //var filteredItems = $('input[filterItemID]:checked');
    //var values = [];
    //for (var j = 0; j < filteredItems.length; j = j + 1) {
    //    var item = $(filteredItems[j]);
    //    //SelectedIconFilters[j] = item.attr('filterItemID');
    //    values[j] = item.attr('value');
    //}

    //var currFilterObj = grid.data("kendoGrid").dataSource.filter();
    //var currentFilters = currFilterObj ? currFilterObj.filters : [];

    //if (currentFilters && currentFilters.length > 0) {
    //    for (var i = 0; i < currentFilters.length; i++) {
    //        if (currentFilters[i].field == 'IC') {
    //            currentFilters.splice(i, 1);
    //            break;
    //        }
    //    }
    //}

    //for (var i = 0; i < values.length; i = i + 1) {
    //    currentFilters.push({ field: 'IC', operator: "contains", value: values[i] });
    //}
    //grid.data("kendoGrid").dataSource.filter({ logic: "or", filters: currentFilters });
    //var IconFilter = $('#workItemsGrid th[data-field=IC] a.k-grid-filter');
    //if(hasFilters){
    //    IconFilter.addClass('k-state-active');
    //}
    //else{
    //    IconFilter.removeClass('k-state-active');
    //}
}
function ClearIconFilters() {
    $('input[filterItemID]:checked').prop('checked', false);
    //var IconFilter = $('#workItemsGrid th[data-field=IC] a.k-grid-filter');
    //IconFilter.removeClass('k-state-active');
}

//var SelectedIconFilters = [];
function onFiltering(arg) {
    if (arg.field == 'IC') {
        //var hasFilters = false;
        //arg.preventDefault();
        if (arg.filter != null) {
            for (var i = 0; i < arg.filter.filters.length; i = i + 1) {
                //hasFilters = true;
                arg.filter.filters[i].operator = "contains";
            }
        }
        //var IconFilter = $('#workItemsGrid th[data-field=IC] a.k-grid-filter');
        //if(hasFilters){
        //    IconFilter.addClass('k-state-active');
        //}
        //else{
        //    IconFilter.removeClass('k-state-active');
        //}

    }
}




var DateTypeFields = [];
function LoadGridData(){
    //Api/GetGridItems/{sessionID}/{workflowID}/{queueID}
    if (selectedQueueID == -2) {
        wfgridcols = WorkflowQueueColumns[selectedWorkflowID].GroupBox;
    }
    else if (selectedQueueID == -1) {
        wfgridcols = WorkflowQueueColumns[selectedWorkflowID].Inbox;
    }
    else {
        wfgridcols = WorkflowQueueColumns[selectedWorkflowID].Queues[selectedQueueID];
    }
    DateTypeFields = [];
    for (var i = 0; i < wfgridcols.length; i = i + 1) {
        if (wfgridcols[i].type == 'date' && wfgridcols[i].format.indexOf('h')==-1) {
            DateTypeFields[DateTypeFields.length] = wfgridcols[i].field;
        }
    }

    ShowWaitSpinner('Loading...', 'wfContent')    
    $.ajax({
        url: "Api/GetGridItems/" + sessionID + "/" + selectedWorkflowID + "/" + selectedQueueID,
        type: "Get",
        async: false,
        converters: {
            "text json": function (data) {
                return $.parseJSON(data, true);
            }
        },
        success: function (data) {
            try{
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    ECMNotification(msg, 3, stack);
                }
                else {
                    var results = data.Data;
                    IsWorkflowManager = results.IsWorkflowManager;
                    GridData = results.Data;
                    QueuePermissions = results.QueuePermissions;
                    QueueButtons = results.QueueButtons;                
                
                    if (GridSetup) {
                        SetGridData();
                        DetermineActiveFunctions(null);
                    }
                    else {
                        DataLoaded = true;
                    }
                }                
            }
            catch (ex) {
                HideWaitSpinner('wfContent');
                ECMNotification(ex, 3);
            }
        },
        error: function (msg) {
            ECMNotification(msg.statusText + ': ' + msg.responseText, 3);
            HideWaitSpinner('wfContent');
        }
    });
}


function SetGridData() {
    //-- prevent flicker if/when workitem view enters
    $('#wfIframe').attr('src', null);
    var selectedItems = [];
    grid.find('tbody>tr').removeClass('k-state-selected');
    grid.find('tbody>tr>td>input').prop('checked', false);
    $('#k-selection-column').prop('checked', false);
    grid.data("kendoGrid").dataSource.pageSize(35);
    grid.data("kendoGrid").dataSource.data(GridData);
    //grid.data("kendoGrid").data(GridData);
    KendoGridRemoveAltColumns();
    HideWaitSpinner('wfContent');
}




function hexToRgb(someString) {

    var hexStart = someString.indexOf("#");
    var hex = someString.substring(hexStart + 1, hexStart + 7);

    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    var rgb = r + "," + g + "," + b;
    var returnString = someString.replace("#" + hex, "rgb(" + rgb + ")");

    return returnString;
}

function LoadSetupIcons() {
    var icons = null;
    $.ajax({
        url: "GetWorkflowPresetIcons/" + sessionID + "/" + selectedWorkflowID,
        type: "Get",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            try {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    ECMNotification(msg, _notificationTypes.error, data.Stack);
                }
                else {
                    icons = data.Data;
                }
            }
            catch (e) {
            }
        },
        error: function (msg) {
            ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
        }
    });
    return icons;
}


function CombineColumnsWithClientSettings(cols, clicols) {
    var finalCols = [];
    for (var i = 0; i < clicols.length; i = i + 1) {
        if (clicols[i] != null) {
            var clicol = clicols[i];
            for (var iCol = 3; iCol < cols.length; iCol = iCol + 1) {
                if (!cols[iCol].hidden && cols[iCol].field == clicol.field) {
                    cols[iCol].width = clicol.width;
                    cols[iCol].copied = true;
                    finalCols[finalCols.length] = cols[iCol];
                    break;
                }
            }
        }
    }    
    for (var iCol = 3; iCol < cols.length; iCol = iCol + 1) {
        if (cols[iCol].copied != true) {
            finalCols[finalCols.length] = cols[iCol];
        }
        cols[iCol].copied = null;
    }
    return finalCols;
}

function ColumnsReordered(evt) {
    var userColumns = GetGridColumnData(evt.sender.columns);
    var movedCol = userColumns.splice(evt.oldIndex, 1);
    userColumns.splice(evt.newIndex, 0, movedCol[0]);
    UpdateGridSettings(userColumns);    
}

function ColumnsResized(evt) {
    var userColumns = GetGridColumnData(evt.sender.columns);    
    UpdateGridSettings(userColumns);
}

function GetGridColumnData(columns) {
    var colData = [];
    var counter = 0;
    for (var i = 0; i < columns.length; i = i + 1) {
        if (!columns[i].hidden) {
            colData[counter] = {
                field: columns[i].field,
                width: columns[i].width
            }
            counter++;
        }
    }
    return colData;
}

function UpdateGridSettings(userColumns) {
    if (ClientSettings === undefined || ClientSettings == null) ClientSettings = {};
    if (ClientSettings.GridSettings === undefined || ClientSettings.GridSettings == null) ClientSettings.GridSettings = [];
    if (ClientSettings.GridSettings[selectedWorkflowID] === undefined || ClientSettings.GridSettings[selectedWorkflowID] == null) {
        ClientSettings.GridSettings[selectedWorkflowID] = { Inbox: {}, GroupBox: {}, Queues: [] };
    }
    if (selectedQueueID == -2) {
        ClientSettings.GridSettings[selectedWorkflowID].GroupBox = userColumns;
    }
    else if (selectedQueueID == -1) {
        ClientSettings.GridSettings[selectedWorkflowID].Inbox = userColumns;
    }
    else {
        ClientSettings.GridSettings[selectedWorkflowID].Queues[selectedQueueID] = userColumns;
    }
    
    SaveClientSettings();
}






function SelectAll(cb){
    var checkbox = $(cb);
    if (!checkbox.is(':checked')) {
        selectedItems = [];
        grid.find('tbody>tr').removeClass('k-state-selected');
        grid.find('tbody>tr>td>input').prop('checked', false);
    }
    else {
        selectedItems = [];        
        var view = grid.data("kendoGrid").dataSource.view();
        for (var i = 0; i < view.length; i = i + 1) {
            selectedItems[selectedItems.length] = view[i];
        }
        grid.find('tbody>tr').addClass('k-state-selected');
        grid.find('tbody>tr>td>input').prop('checked', true);        
    }
    DetermineActiveFunctions();
}

function ClearAllSelected() {
    selectedItems = [];
    grid.find('tbody>tr').removeClass('k-state-selected');
    grid.find('tbody>tr>td>input').prop('checked', false);
    DetermineActiveFunctions();
}

function FindDataFromUID(uid) {
    for (var i = 0; i < selectedItems.length; i = i + 1) {
        if (selectedItems[i].uid==uid) {
            return selectedItems[i];
            break;
        }
    }
    return null;
}

var selectedItems = [];
var startItem = null;
var multiSelect = true;
function HandleRowClick(e) {

    var rowIndex = e.currentTarget.rowIndex;
    var data_uid = e.currentTarget.getAttribute("data-uid");
    var row = $(e.currentTarget);
    var checkbox = row.find('input');
    var data = grid.data("kendoGrid").dataSource.getByUid(data_uid);
    var indx = selectedItems.indexOf(data);
    if (indx == -1) {
        selectedItems[selectedItems.length] = data;
        //grid.data("kendoGrid").select(row);
        row.addClass('k-state-selected');
        checkbox.prop('checked', true);
    }
    else {
        selectedItems.splice(indx, 1);
        row.removeClass('k-state-selected');
        checkbox.prop('checked', false);
        //grid.data("kendoGrid").deselect(row);
    }
    


    ////-- is this row selected
    //var item = $.grep(selectedItems, function (e) { return e.uid == data_uid });
    //if (item.length > 0) {
    //    //-- remove 
    //    selectedItemsTemp = $.grep(selectedItems, function (e) { return e.uid != data_uid });

    //    //-- clears all, ugh
    //    grid.data("kendoGrid").clearSelection();

    //    selectedItems = selectedItemsTemp;

    //    //-- reselect rows
    //    $.each(selectedItems, function (i, item) {
    //        var row = grid.data("kendoGrid").tbody.find("tr[data-uid='" + item.uid + "']");
    //        var row = $($(item).parent());
    //        var uid = row.attr('data-uid');
    //        var indx = selectedItems.indexOf(uid);
    //        if (indx == -1) {
    //            selectedItems[selectedItems.length] = uid;
    //            //grid.data("kendoGrid").select(row);
    //            row.addClass('k-state-selected');
    //        }
    //        else {
    //            selectedItems.splice(indx, 1);
    //            row.removeClass('k-state-selected');
    //            //grid.data("kendoGrid").deselect(row);
    //        }
    //        //grid.data("kendoGrid").select(row);
    //    });
    //}
    //else {
    //    //-- select row
    //    grid.data("kendoGrid").select("tr:eq(" + rowIndex.toString() + ")");
    //}

    //e.preventDefault();
    //var targetID = e.currentTarget.getAttribute("data-uid");
    //if (targetID != null) {

    //    if ((!e.ctrlKey && !e.shiftKey) || selectedItems.length == 0 || startItem == targetID || !multiSelect) {
    //        selectedItems = [];
    //        var data = grid.data("kendoGrid").dataSource.view();
    //        for (var i = 0; i < data.length; i = i + 1) {
    //            if (data[i].uid == targetID) {
    //                selectedItems[parseInt(targetID.substring(3))] = data[i];
    //                startindex = i;
    //                break;
    //            }
    //        }
    //        startItem = targetID;
    //    }
    //    else if (e.shiftKey) {
    //        selectedItems = [];
    //        var dataSource = grid.data("kendoGrid").dataSource;
    //        var filters = dataSource.filter();
    //        var sorts = dataSource.sort();
    //        var allData = dataSource.data();
    //        var data = null;
    //        if (filters == null && sorts == null) {
    //            data = allData;
    //        }
    //        else if (filters != null && sorts != null) {
    //            var query = new kendo.data.Query(allData);
    //            data = query.filter(filters).sort(sorts).data;
    //        }
    //        else if (sorts != null) {
    //            var query = new kendo.data.Query(allData);
    //            data = query.sort(sorts).data;
    //        }
    //        else {
    //            var query = new kendo.data.Query(allData);
    //            data = query.filter(filters).data;
    //        }

    //        var startindex = -1;
    //        var endindex = -1;
    //        for (var i = 0; i < data.length; i = i + 1) {
    //            if (data[i].uid == startItem) {
    //                startindex = i;
    //                break;
    //            }
    //        }
    //        for (var i = 0; i < data.length; i = i + 1) {
    //            if (data[i].uid == targetID) {
    //                endindex = i;
    //                break;
    //            }
    //        }
    //        if (startindex != -1 && endindex != -1) {
    //            if (startindex > endindex) {
    //                var temp = endindex;
    //                endindex = startindex;
    //                startindex = temp;
    //            }
    //            for (var iselection = startindex; iselection <= endindex; iselection = iselection + 1) {
    //                selectedItems[parseInt(data[iselection].uid.substring(3))] = data[iselection];
    //            }
    //        }
    //        else throw "Invalid Selection";
    //    }
    //    else if (e.ctrlKey) {
    //        if (startItem == targetID) startItem = null;
    //        var selecteduid = parseInt(targetID.substring(3))
    //        if (selectedItems[selecteduid]) selectedItems[selecteduid] = null;
    //        else {
    //            var data = grid.data("kendoGrid").dataSource.view();
    //            for (var i = 0; i < data.length; i = i + 1) {
    //                if (data[i].uid == targetID) {
    //                    selectedItems[selecteduid] = data[i];
    //                    startindex = i;
    //                    break;
    //                }
    //            }
    //        }
    //    }
    //    UpdateItemsViewState();
    //}
}

function IsItemSelected(e) {
    var targetID = e.currentTarget.getAttribute("data-uid");
    if (targetID != null) {
        var data = grid.data("kendoGrid").dataSource.view();        
        for (var i = 0; i < data.length; i = i + 1) {
            if (data[i].uid == targetID) {
                for (var x = 0; x < selectedItems.length; x = x + 1) {
                    if (selectedItems[x] == data[i]) return true;
                }
                return false;
            }
        }
    }
    return false;
}

function UpdateItemsViewState(e) {
    var gridData = grid.data("kendoGrid");
    var rows = $(gridData.tbody).find("tr");
    var data = gridData.dataSource.view();
    for (var i = 0; i < rows.length; i++) {
        if (data[i].F != '') $(rows[i]).css("background-color", data[i].F);
        var rowID = rows[i].getAttribute("data-uid");
        if (selectedItems[parseInt(rowID.substring(3))]) {
            $(rows[i]).addClass("k-state-selected");
        }
        else if (e === undefined) {
            $(rows[i]).removeClass("k-state-selected");
        }
    }
}


function GetSelectedItemData() {
    var selectedItemsData = [];
    var selectedItemIndex = 0;
    for (var i = 0; i < selectedItems.length; i = i + 1) {
        if (selectedItems[i]) {
            selectedItemsData[selectedItemIndex] = selectedItems[i];
            selectedItemIndex = selectedItemIndex + 1;
        }
    }
    return selectedItemsData;
}

//This function will be used to replace the kendo default guid function.  
//guid results for the grid per row are extremely slow.  
//This is VERY fast.  
//Should be reset to 0 after every grid load, but doesn't have to be.
var kendoUniqueID = 0;
kendo.guid = function () {
    kendoUniqueID = kendoUniqueID + 1;
    return "kID" + kendoUniqueID;
}

function changeHighlightProperty(className, value) {
    // This function overwrites the kendo grid property .k-state-selected  making the background color !important so row background colors no longer override the row selection.
    var ss = document.styleSheets;
    for (var i = 0; i < ss.length; i++) {
        var ss = document.styleSheets;
        var rules = ss[i].cssRules || ss[i].rules;
        for (var j = 0; j < rules.length; j++) {
            if (rules[j].selectorText === ".k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-list > .k-state-selected, .k-list > .k-state-highlight, .k-panel > .k-state-selected, .k-ghost-splitbar-vertical, .k-ghost-splitbar-horizontal, .k-state-selected.k-draghandle:hover, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-state-selected.k-today, .k-marquee-color") {
                rules[j].style.backgroundColor = rules[j].style.backgroundColor + " !important";
            }
        }
    }
}
