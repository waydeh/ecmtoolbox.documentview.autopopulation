﻿var sessionID = null;
var workflowID = null;

var Workflow = null;
var WorkflowPaths = null;
var Fields = null;
var Conditions = null;
var Actions = null;
var Queues = null;

var ViewerPresets = null;

var SelectedQueueID = null;
var SelectedEventID = null;
var SelectedRuleID = null;
var SelectedConditionID = null;
var SelectedActionID = null;

var SelectedRule = null;

var buttonTextColorPicker = null;
var buttonColorPicker = null;

var SourceFields = null;

//-- instantiate toolbox object
var _toolbox = new toolbox;

var _iconsSetup = null;


$(document).ready(function () {    
    sessionID = params["sessionid"];
    workflowID = params["workflowid"];

    //-- validate session id
    if (_toolbox.engine.validSession(_toolbox, sessionID)) {

        //if ((typeof sessionID === "undefined") || (typeof workflowID === "undefined")) {
        //    //-- redirect back to login
        //    window.location.replace($(location).attr("origin") + "/login.html")
        //}

        $('#aFields').on('click', function () { window.location.href = 'Workflow_Fields.html?sessionID=' + sessionID + '&workflowID=' + workflowID });
        $('#aKickoff').on('click', function () { window.location.href = 'Workflow_Kickoff.html?sessionID=' + sessionID + '&workflowID=' + workflowID });
        $('#btnReturnToWorkflows').on('click', function () { window.location.href = 'Workflows.html?sessionID=' + sessionID + '&workflowID=' + workflowID });

        $('input[type=number]').kendoNumericTextBox({ min: 0, decimals: 0 });
        
        buttonTextColorPicker = $('#cpButtonTextColor').kendoColorPicker({
            toolIcon: "k-foreColor",
            change: function (e) {
                btnExample
                $('#btnExample').css('color', e.value);
                $('#txtButtonTextColor').val(e.value);
            }
        }).data("kendoColorPicker");

        buttonColorPicker = $('#cpButtonColor').kendoColorPicker({
            toolIcon: "k-foreColor",
            change: function (e) {
                $('#btnExample').css('background-color', e.value);
                $('#txtButtonColor').val(e.value);
            }
        }).data("kendoColorPicker");

        $('#timer_Daily_Time').kendoTimePicker();
        $('#timer_Weekly_Time').kendoTimePicker();
        $('#timer_Monthly_Time').kendoTimePicker();
        $('#divQueueFieldsScroller').slimScroll({ alwaysVisible: true, height:430 });
        //$('#divQueueSettings_PageSetup').slimScroll({ alwaysVisible: true });

        $('#divEventButtonUserSelectQueues').slimScroll({ alwaysVisible: true });
        
        buttonUserSelectRowColorPicker = $('#cpUserSelectRowColor').kendoColorPicker({
            toolIcon: "k-foreColor",
            change: function (e) {
                $('#txtUserSelectRowColor').val(e.value);
            }
        }).data("kendoColorPicker");

        $("#ddlViewerPresets").chosen({
            width: '250px',
            placeholder_text_single: "Select a Preset Viewer URL...",
            no_results_text: "No Presets Found"
        });
        $("#ddlViewerPresets").chosen().change(ViewerURLPresetDropdownChanged);

        ECMTRegisterListBoxes();
        LoadWorkflowDesignerData();

        //-- need to load the source fields for the workflow id for the action_updatesourcefields
        
        
        $(window).on('resize', function () {
            try { ApplyLines(); } catch (e) { }
        });

        $("div[id$='Settings']").draggable({
            handle: ".modal-header"
        });
        $("#windowStartRules").draggable({
            handle: ".modal-header"
        });
        
        $('#lstPresets').delegate('div', 'click', function (event) {
            var target = event.currentTarget;
            EditPreset(target);
        });
        
        //-- queue setttings - all checkboxes
        $('#cbViewAllQueue').change(function () {
            this.checked ? $('[id^=cbFieldQueueView]').prop('checked', true) : $('[id^=cbFieldQueueView]').prop('checked', false);
        });
        $('#cbViewAllWorkItem').change(function () {
            this.checked ? $('[id^=cbFieldWIView]').prop('checked', true) : $('[id^=cbFieldWIView]').prop('checked', false);
        });
        $('#cbEditAllWorkItem').change(function () {
            this.checked ? $('[id^=cbFieldWIEdit]').prop('checked', true) : $('[id^=cbFieldWIEdit]').prop('checked', false);
        });
        $('#cbRequiredAllWorkItem').change(function () {
            this.checked ? $('[id^=cbFieldWIRequired]').prop('checked', true) : $('[id^=cbFieldWIRequired]').prop('checked', false);
        });
        $('#cbImportantAllWorkItem').change(function () {
            this.checked ? $('[id^=cbFieldWIImportant]').prop('checked', true) : $('[id^=cbFieldWIImportant]').prop('checked', false);
        });

        _iconsSetup = LoadSetupIcons();
        $("#iconsSetup").html(iconsFormat(_iconsSetup));
    }
    else {
        //-- redirect back to login
        window.location.href.replace($(location).attr("origin") + "/login.html")
    }

});

//Path Hiding
{
    var __hiddenPaths = null;
    function SetupHiddenPaths() {
        if (__hiddenPaths == null) {
            if (typeof(Storage) !== "undefined") {
                var hiddenPaths = localStorage.getItem("hidden_path_ids_wf_" + workflowID);
                if (hiddenPaths == null || hiddenPaths.trim() == "") __hiddenPaths = [];
                else __hiddenPaths = hiddenPaths.split(",");
            }
            else{
                __hiddenPaths=[];
            }
        }
    }

    function HiddenPathsContains(pathID) {
        SetupHiddenPaths();
        pathID = pathID.toString().trim();
        if (pathID == "") return false;
        return (__hiddenPaths.indexOf(pathID) != -1);
    }

    function AddHiddenPath(pathID) {
        SetupHiddenPaths();
        pathID = pathID.toString().trim();
        if (pathID == "") return;
        if (__hiddenPaths.indexOf(pathID) == -1) {
            __hiddenPaths[__hiddenPaths.length] = pathID;
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem("hidden_path_ids_wf_" + workflowID, __hiddenPaths.join(','));
            }
        }
    }

    function RemoveHiddenPath(pathID) {
        SetupHiddenPaths();
        pathID = pathID.toString().trim();
        if (pathID == "") return;
        var indx = __hiddenPaths.indexOf(pathID);
        if (indx != -1) {
            __hiddenPaths.splice(indx, 1);
            if (typeof (Storage) !== "undefined") {
                if (__hiddenPaths.length == 0) {
                    localStorage.removeItem("hidden_path_ids_wf_" + workflowID);
                }
                else {
                    localStorage.setItem("hidden_path_ids_wf_" + workflowID, __hiddenPaths.join(','));
                }
            }
        }
    }
}

//Helper Functions
{

    function SetPageUnique(configPage) {
        if (configPage == null || configPage.trim() == "") {
            return "noconfigurationrequired.html";
        }
        else {
            return configPage = (configPage.indexOf('?') == -1) ? configPage + "?timestamp=\"" + Math.floor(Date.now()) + "\"" : configPage + "&timestamp=\"" + Math.floor(Date.now()) + "\"";
        }
    }

    function GetQueueByID(queueid) {
        for (var i = 0; i < Queues.length; i = i + 1) {
            if (Queues[i].QueueID == queueid) {
                return Queues[i];
            }
        }
        return null;
    }
    function GetEventByID(queue, eventid) {

        for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
            if (queue.Settings.QueueEvents[iEvent].EventID == eventid) {
                return queue.Settings.QueueEvents[iEvent];
            }
        }
        return null;
    }
    function GetEventByType(queue) {
        //-- handles both cases
        //-- queue / events / rules
        //-- start queue / events / rules grepped by workflow path id

        //-- the whole issue is start rules are not persisted at each queue, but rather in a seperate queue with queueid = -1
        //-- but from a visual perspective, start rules are display at each queue level so the start queue rules have to be grepped out
        //-- and stuffed into a phony event
        var event = null;

        if (SelectedQueueID == -1) {
            //-- start rules
            var rules = $.grep(queue.Settings.QueueEvents[0].Rules, function (e) { return e.WorkflowPathName == WorkflowPathID });
            event = {
                'EventType': 5,
                'Rules': rules
            };
        }
        else {
            //-- event button
            event = GetEventByID(queue, SelectedEventID);
        }
        return event;
    }
    function GetRuleByID(event, ruleid) {
        for (var iRule = 0; iRule < event.Rules.length; iRule = iRule + 1) {
            if (event.Rules[iRule].RuleID == ruleid) {
                return event.Rules[iRule];
            }
        }
        return null;
    }
    function GetActionByID(rule, actionid) {
        for (var iAction = 0; iAction < rule.Actions.length; iAction = iAction + 1) {
            if (rule.Actions[iAction].ActionID == actionid) {
                return rule.Actions[iAction];
            }
        }
        return null;
    }
    function GetActionByActionName(actionname) {
        for (var i = 0; i < Actions.length; i = i + 1) {
            if (Actions[i].ActionName == actionname) {
                return Actions[i];
            }
        }
        return null;
    }
    function GetConditionByID(rule, conditionid) {
        for (var iCondition = 0; iCondition < rule.Conditions.length; iCondition = iCondition + 1) {
            if (rule.Conditions[iCondition].ConditionID == conditionid) {
                return rule.Conditions[iCondition];
            }
        }
        return null;
    }
    function GetConditionByConditionName(conditionname) {
        for (var i = 0; i < Conditions.length; i = i + 1) {
            if (Conditions[i].ConditionName == conditionname) {
                return Conditions[i];
            }
        }
        return null;
    }
    function GetNextEventID(queue) {
        var eventID = 0;
        for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
            if (queue.Settings.QueueEvents[iEvent].EventID >= eventID) eventID = queue.Settings.QueueEvents[iEvent].EventID + 1;
        }
        return eventID
    }
    function GetNextRuleID(event) {

        var ruleID = 0;

        for (var iRule = 0; iRule < event.Rules.length; iRule = iRule + 1) {
            if (event.Rules[iRule].RuleID >= ruleID) ruleID = event.Rules[iRule].RuleID + 1;
        }
        return ruleID
    }
    function GetNextConditionID(rule) {

        var conditionID = 0;

        for (var icond = 0; icond < rule.Conditions.length; icond = icond + 1) {
            if (rule.Conditions[icond].ConditionID >= conditionID) {
                conditionID = rule.Conditions[icond].ConditionID + 1;
            }
        }
        return conditionID;
    }
    function GetNextActionID(rule) {

        var actionID = 0;

        for (var iact = 0; iact < rule.Actions.length; iact = iact + 1) {
            if (rule.Actions[iact].ActionID >= actionID) {
                actionID = rule.Actions[iact].ActionID + 1;
            }
        }
        return actionID;
    }
    function htmlEncode(value) {
        return $('<div/>').text(value).html();
    }

    var get_params = function (search_string) {
        var parse = function (params, pairs) {
            var pair = pairs[0];
            var parts = pair.split('=');
            var key = decodeURIComponent(parts[0]);
            var value = decodeURIComponent(parts.slice(1).join('='));
            // Handle multiple parameters of the same name
            if (typeof params[key] === "undefined") {
                params[key] = value;
            } else {
                params[key] = [].concat(params[key], value);
            }
            return pairs.length == 1 ? params : parse(params, pairs.slice(1))
        }
        // Get rid of leading ?
        return search_string.length == 0 ? {} : parse({}, search_string.substr(1).toLowerCase().split('&'));
    }
    var params = get_params(location.search);
}

function LoadWorkflowDesignerData() {
    //GetDesignerData/{sessionID}/{workflowID}
    ShowWaitSpinner('Loading Designer');
    $.ajax({
        url: "GetDesignerData/" + sessionID + "/" + workflowID,
        type: "Get",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (data) {
            try{
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    ECMNotification(msg, _notificationTypes.error, data.Stack);
                }
                else {
                    Workflow = data.Data.workflow;
                    ViewerPresets = Workflow.ViewerPresets;
                    Fields = Workflow.Fields;
                    //ECMNotification('Workflow Data and Fields Loaded.', 0);
                    Conditions = data.Data.conditions;
                    Actions = data.Data.actions;
                    WorkflowPaths = data.Data.paths;

                    $('#ddlConditionTypes').empty();
                    $('#ddlActionTypes').empty();
                                
                    var i = 0;
                    for (i = 0; i < Conditions.length; i = i + 1) {
                        $('#ddlConditionTypes').append('<option conditionIndex="' + i + '">' + Conditions[i].ConditionName + '</option>');
                        $('#ddlStartConditionTypes').append('<option conditionIndex="' + i + '">' + Conditions[i].ConditionName + '</option>');
                    }

                    $('#ddlStartActionTypes').append('<option value="NA" selected="selected">Select</option>');
                    for (i = 0; i < Actions.length; i = i + 1) {
                        if (Actions[i].ShowForActionTypeKickoff) {
                            $('#ddlStartActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                        }
                    }

                    for (i = 0; i < Fields.length; i = i + 1) {
                        var sFields = '<tr fieldid="' + Fields[i].FieldID + '">';
                        sFields = sFields + '<td>' + HtmlEncode(Fields[i].FieldName) + '</td>';
                        
                        sFields = sFields + '<td><input type="checkbox" id="cbFieldQueueView' + Fields[i].FieldID + '" fieldid="' + Fields[i].FieldID + '" /><label for="cbFieldQueueView' + Fields[i].FieldID + '" tabsindex="1"></label></td>';
                        sFields = sFields + '<td>' + (Fields[i].ColumnName.substring(0, 1) == "f" ? '<input type="checkbox" id="cbFieldWIView' + Fields[i].FieldID + '"fieldid="' + Fields[i].FieldID + '" /><label for="cbFieldWIView' + Fields[i].FieldID + '" tabindex="1"></label>' : '') + '</td>';
                        sFields = sFields + '<td>' + (Fields[i].ColumnName.substring(0, 1) == "f" ? '<input type="checkbox" id="cbFieldWIEdit' + Fields[i].FieldID + '"fieldid="' + Fields[i].FieldID + '" /><label for="cbFieldWIEdit' + Fields[i].FieldID + '" tabindex="1"></label>' : '') + '</td>';
                        sFields = sFields + '<td>' + (Fields[i].ColumnName.substring(0, 1) == "f" ? '<input type="checkbox" id="cbFieldWIRequired' + Fields[i].FieldID + '"fieldid="' + Fields[i].FieldID + '" /><label for="cbFieldWIRequired' + Fields[i].FieldID + '" tabindex="1"></label>' : '') + '</td>';
                        sFields = sFields + '<td>' + (Fields[i].ColumnName.substring(0, 1) == "f" ? '<input type="checkbox" id="cbFieldWIImportant' + Fields[i].FieldID + '"fieldid="' + Fields[i].FieldID + '" /><label for="cbFieldWIImportant' + Fields[i].FieldID + '" tabindex="1"></label>' : '') + '</td>';
                        sFields = sFields + '</tr>';
                        $('#tblQueueFields>tbody').append(sFields);
                    }
                    SetupViewerURLPresets();
                    //ECMNotification('Action and Conditions Types Loaded.', 0);

                    SourceFields = (_toolbox.engine.sourceFieldsGetAll(_toolbox, workflowID)).Data;

                    $('#workflowdesignheader').text('Workflow Settings - ' + Workflow.WorkflowName);

                    if (Workflow.HasViewerURLBuilderPage) {
                        $('#btnURLBuilder').show();
                        $('#iframeViewerStringBuilder').attr('src', "DynamicLibraries/" + SetPageUnique(Workflow.ViewerURLBuilderPage));
                    }
                    else {
                        $('#btnURLBuilder').hide();
                    }

                    HideWaitSpinner();                
                    LoadQueues();
                }
            }
            catch (e) {
                HideWaitSpinner();
            }
        },
        error: function (msg) {
            HideWaitSpinner();
            ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
        }
    });
}

//Visual Diagram
{
    var htmlQueues = $('#templateQueues').html();
    var htmlEvents = $('#templateEvents').html();
    var htmlConditions = $('#templateConditions').html();
    var htmlActions = $('#templateActions').html();

    function BuildFullPageStructure() {
        
        var sHtml = '<div id="divLineContainer"></div>';
        sHtml = sHtml + '<div id="divAddButtonContainer"></div>';        
        $('#divContentContainer').html(sHtml);

        for (var iPath = 0; iPath < WorkflowPaths.length; iPath = iPath + 1) {
            var pth = WorkflowPaths[iPath];
            var wfpath = BuildWorkflowPathsStructure(pth);
            $('#divContentContainer').append(wfpath);
        }

        for (var iQueue = Queues.length-1; iQueue >= 0 ; iQueue = iQueue -1) {
            var queue = Queues[iQueue];
            if (queue.QueueID != -1) {
                var queueContainer = BuildQueuesStructure(queue);
                queueContainer.insertAfter('.divWFPathHeader[pathid=' + queue.WorkflowPathID + ']');
                DrawQueueLinesAndShapes(queue.QueueID, queueContainer);
                if (HiddenPathsContains(queue.WorkflowPathID)) {
                    queueContainer.hide();
                }
            }
        }

        //ApplyLines();

        var ttButtons = $('[data-toggle="tooltip"]');
        ttButtons.tooltip();
    }

    function BuildWorkflowPathsStructure(pth) {            
        var html =
        '<div class="divWFPathHeader" pathid="' + pth.WorkflowPathID + '" >' +
            '<table for="tblWFPath' + pth.WorkflowPathID + '" class="tblWFPathHeader">' +
                '<tr>' +
                    '<td onclick="TogglePathView(this,' + pth.WorkflowPathID + ')" style="width:65px; cursor:pointer; color:#3E8AC4 !important;"><i class="fa fa-caret-down"></i></td>' +
                    '<td>' + HtmlEncode(pth.WorkflowPathName) +
                        '<i class="fa fa-edit" style="color: #CE9444; cursor:pointer;" onclick="ShowWorkflowPathEditor(' + pth.WorkflowPathID + ')" ></i>' +
                        '&nbsp;&nbsp;&nbsp;<button onclick="ShowStartRules(' + pth.WorkflowPathID + '); return false;" class="divAddQueueButton" type="button">Start Rules</button>' +
                    '</td>' +
                    '<td style="width:125px; text-align:right;">' +
                        '<button onclick="CreateQueue(' + pth.WorkflowPathID + ')" class="divAddQueueButton"><i class="fa fa-plus"></i>Add Queue</button>' +
                    '</td>' +
                '</tr>' +
            '</table>' +
        '</div>';
        var pathStructure = $(html);
        if (HiddenPathsContains(pth.WorkflowPathID)) {
            var icon = $(pathStructure.find('i')[0]);
            icon.removeClass('fa-caret-down');
            icon.addClass('fa-caret-right');
            pathStructure.addClass('roundBottom');
        }
        return pathStructure;
    }

    function BuildQueuesStructure(queue) {
        var queueContainer = $('<div></div>');
        queueContainer.attr('id', 'divWFTableContainer' + queue.WorkflowPathID);
        queueContainer.attr('pathid', queue.WorkflowPathID);
        queueContainer.attr('queueid', queue.QueueID);
        queueContainer.addClass('queueContainer');
        var queueTable = $('<table></table>');
        queueTable.attr('id', 'tblWFPath' + queue.WorkflowPathID);
        queueTable.attr('pathid', queue.WorkflowPathID);
        queueTable.attr('queueid', queue.QueueID);
        queueTable.addClass('designerTable');
        
        var divQueue = $(htmlQueues);
        divQueue.attr("queueid", queue.QueueID);
        divQueue.attr('onmouseup', 'Queue_MouseUp(event, this, ' + queue.QueueID + ');');
        divQueue.append(queue.QueueName);

        //Gets total rows to be spanned based on total Rules found heirarchically under this queue
        {
            var queueRowSpan = 0;
            for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
                queueRowSpan = queueRowSpan + (queue.Settings.QueueEvents[iEvent].Rules.length == 0 ? 1 : queue.Settings.QueueEvents[iEvent].Rules.length);
            }
            if (queueRowSpan == 0) queueRowSpan = 1;
        }

        var htmlDiagramRow = '<tr><td rowspan="' + queueRowSpan + '">' + divQueue.prop('outerHTML') + '</td>';

        if (queue.Settings.QueueEvents.length == 0) {
            //Queue Has No Events
            htmlDiagramRow = htmlDiagramRow + "<td></td><td></td><td></td></tr>";
        }
        else {
            for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
                var event = queue.Settings.QueueEvents[iEvent];
                var divEvent = $(htmlEvents);
                var iconClass = '';
                divEvent.attr("queueid", queue.QueueID);
                divEvent.attr("eventid", event.EventID);
                divEvent.attr("onmouseup", 'Event_MouseUp(event, this, ' + queue.QueueID + ', ' + event.EventID + ');');
                var sEventText = event.EventText.replace("Button: ", " ");
                divEvent.append(sEventText);
                if (event.EventType == 0) { //Button Only
                    divEvent.css('color', event.Settings[1] + ' !imporant');
                    divEvent.css('background-color', event.Settings[2] + ' !imporant');
                    iconClass = 'fa-youtube-play';
                    if (!(event.Settings[12] === undefined) && event.Settings[12] != null && event.Settings[12] != '') {
                        divEvent.attr('data-toggle', 'tooltip');
                        divEvent.attr('title', event.Settings[12]);
                    }
                }
                else if(event.EventType==1){
                    //All non-buttons
                    divEvent.addClass('divEvent-NonButton');
                    iconClass = 'fa-clock-o';
                }
                else{
                    //All non-buttons
                    divEvent.addClass('divEvent-NonButton');
                    iconClass = 'fa-bolt';
                }

                divEvent.prepend('<i class="fa ' + iconClass + '"></i>');

                var eventRowSpan = event.Rules.length == 0 ? 1 : event.Rules.length;
                htmlDiagramRow = htmlDiagramRow + (iEvent == 0 ? "" : "<tr>") + '<td rowspan="' + eventRowSpan + '">' + divEvent.prop('outerHTML') + '</td>';

                if (event.Rules.length == 0) {
                    htmlDiagramRow = htmlDiagramRow + "<td></td><td></td></tr>";
                }
                else {
                    for (var iRule = 0; iRule < event.Rules.length; iRule = iRule + 1) {
                        var rule = event.Rules[iRule];
                        var divConditions = $(htmlConditions);
                        divConditions.attr('queueid', queue.QueueID);
                        divConditions.attr('eventid', event.EventID);
                        divConditions.attr('ruleid', rule.RuleID);
                        var divConditionsAddLink = $($(divConditions.children()[0]).children()[0]);
                        divConditionsAddLink.attr('onclick', 'AddCondition(' + queue.QueueID + ',' + event.EventID + ',' + rule.RuleID + ');');
                        var divConditionsArea = $(divConditions.children()[1]);
                        if (rule.Conditions.length == 0) {
                            //divConditions.prepend('<label>No Conditions</label>');
                        }
                        else {
                            for (var iCond = 0; iCond < rule.Conditions.length; iCond = iCond + 1) {
                                var condition = rule.Conditions[iCond];
                                var cndtn = $('<tr><td onclick="EditCondition(' + queue.QueueID + ',' + event.EventID + ',' + rule.RuleID + ',' + condition.ConditionID + ')">' + HtmlEncode(condition.ConditionDisplayText) + '</td><td><button class="btn"  onclick="RemoveCondition(' + queue.QueueID + ',' + event.EventID + ',' + rule.RuleID + ',' + condition.ConditionID + ')"><i class="fa fa-times-rectangle"></i></button></td></tr>');
                                divConditionsArea.append(cndtn);
                            }
                        }

                        var divActions = $(htmlActions);
                        divActions.attr('queueid', queue.QueueID);
                        divActions.attr('eventid', event.EventID);
                        divActions.attr('ruleid', rule.RuleID);
                        var divActionsAddLink = $($(divActions.children()[0]).children()[0]);
                        divActionsAddLink.attr('onclick', 'AddAction(' + queue.QueueID + ',' + event.EventID + ',' + rule.RuleID + ');');
                        var divActionsArea = $(divActions.children()[1]);
                        var divProgressionArea = $(divActions.children()[2]);
                        if (rule.Actions.length == 0) {
                            //divActions.prepend('<label>No Actions</label>');
                        }
                        else {
                            for (var iAct = 0; iAct < rule.Actions.length; iAct = iAct + 1) {
                                var action = rule.Actions[iAct];

                                var actn = $('<tr><td onclick="EditAction(' + queue.QueueID + ',' + event.EventID + ',' + rule.RuleID + ',' + action.ActionID + ')">' + HtmlEncode(action.ActionDisplayText) + '</td><td><button class="btn" onclick="RemoveAction(' + queue.QueueID + ',' + event.EventID + ',' + rule.RuleID + ',' + action.ActionID + ')"><i class="fa fa-times-rectangle"></i></button></td></tr>');
                                //if (action.LinkedQueueID != null) {
                                if (action.ActionTypeName.toLowerCase() == "go to") {
                                    //This will highlight Go To Types of Actions
                                    //$(actn.children()[0]).css('font-weight', 'bold');
                                    //$(actn.children()[0]).css('color', '#2C587A');
                                    $(actn.children()[0]).prepend('<i class="fa fa-play" style="margin-right:4px;"></i>')
                                    divProgressionArea.append(actn);
                                }
                                else {
                                    divActionsArea.append(actn);
                                }
                            }
                        }

                        htmlDiagramRow = htmlDiagramRow + (iRule == 0 ? "" : "<tr>") + '<td>' + divConditions.prop('outerHTML') + '</td>' + '<td>' + divActions.prop('outerHTML') + '</td></tr>';
                    }
                }
            }
        }
        var queueStructure = queueContainer.append(queueTable.html(htmlDiagramRow));        
        return queueStructure;
    }
    
    function DrawQueueLinesAndShapes(queueid, queueStructure) {
        queueStructure.find('div[type=mapline]').remove();
        queueStructure.find('.divAddEventButton').remove();
        queueStructure.find('.divRemoveRuleButton').remove();
        var allQueues = queueStructure.find('.divQueue');
        var allEvents = queueStructure.find('.divEvent');
        var allConditions = queueStructure.find('.divConditions');
        var allActions = queueStructure.find('.divActions');

        //var queueContainerBounds = queueStructure[0].getBoundingClientRect();
        var queueBounds = GetObjectBounds(allQueues[0]);
                
        var queueStart={
            left:queueBounds.left+queueBounds.width,
            top:queueBounds.top + (queueBounds.height / 2)
        };
        AddEventButtons(queueStart.left, queueStart.top, queueid, queueStructure);

        var eventBounds = [];
        var rulebounds = [];

        for (var i = 0; i < allEvents.length; i = i + 1) {            
            var elemBounds = GetObjectBounds(allEvents[i]);            
            var eventid = parseInt(allEvents[i].getAttribute('eventid'));
            var bgColor = $(allEvents[i]).css('background-color');
            eventBounds[eventid] = {
                left: elemBounds.left + elemBounds.width,
                top: elemBounds.top + (elemBounds.height / 2)
            };
            DrawLine(queueStructure, queueStart.left, queueStart.top, elemBounds.left, elemBounds.top + (elemBounds.height / 2));
            AddRuleButtons(eventBounds[eventid].left, eventBounds[eventid].top, queueid, eventid, queueStructure, bgColor);
        }

        for (var i = 0; i < allConditions.length; i = i + 1) {
            var elemBounds = GetObjectBounds(allConditions[i]);
            var eventid = parseInt(allConditions[i].getAttribute('eventid'));
            var ruleid = allConditions[i].getAttribute('ruleid');
            rulebounds[eventid+'-'+ruleid] = {
                left: elemBounds.left + elemBounds.width,
                top: elemBounds.top + (elemBounds.height / 2)
            };
            var isStraightLine = DrawLine(queueStructure, eventBounds[eventid].left, eventBounds[eventid].top, elemBounds.left, elemBounds.top + (elemBounds.height / 2));
            AddConditionRemoveRuleButton(elemBounds.left, isStraightLine ? eventBounds[eventid].top : elemBounds.top + (elemBounds.height / 2), queueid, eventid, ruleid, queueStructure);
        }

        for (var i = 0; i < allActions.length; i = i + 1) {
            var elemBounds = GetObjectBounds(allActions[i]);
            var eventid = allConditions[i].getAttribute('eventid');
            var ruleid = allActions[i].getAttribute('ruleid');
            DrawLine(queueStructure, rulebounds[eventid + '-' + ruleid].left, rulebounds[eventid + '-' + ruleid].top, elemBounds.left, elemBounds.top + (elemBounds.height / 2));
        }
    }

    function DrawLine(lineContainer, x1, y1, x2, y2) {
        y1=y1-2; //Adjustments for size of line
        y2=y2-1;
        if (y1 - y2 <= 5 && y1 - y2 >= -5) {
            var width = x2 - x1;
            lineContainer.append('<div type="mapline" class="mapline-straight" style="top:' + y1 + 'px;left:' + x1 + 'px;width:' + width + 'px;height:0px;"></div>');
            return true;
        }
        else if (y2 > y1) {
            var width = ((x2 - x1) / 2) + 2
            var height = ((y2 - y1) / 2) + 2;
            //downward
            lineContainer.append('<div type="mapline" class="mapline-topright" style="top:' + y1 + 'px;left:' + x1 + 'px;width:' + width + 'px;height:' + height + 'px;"></div>');
            lineContainer.append('<div type="mapline" class="mapline-bottomleft" style="top:' + (y1 + height) + 'px;left:' + (x1 + width - 4) + 'px;width:' + width + 'px;height:' + height + 'px;"></div>');
            return false;
        }
        else {
            var width = ((x2 - x1) / 2) + 2;
            var height = ((y1 - y2) / 2) + 2;
            //upward
            lineContainer.append('<div type="mapline" class="mapline-bottomright" style="top:' + (y2 + height) + 'px;left:' + x1 + 'px;width:' + width + 'px;height:' + height + 'px;"></div>');
            lineContainer.append('<div type="mapline" class="mapline-topleft" style="top:' + y2 + 'px;left:' + (x1 + width - 4) + 'px;width:' + width + 'px;height:' + height + 'px;"></div>');
            return false;
        }
    }

    function GetObjectBounds(elem) {                
        var bounds = {
            left: elem.offsetLeft + elem.offsetParent.offsetLeft,
            top: elem.offsetTop + elem.offsetParent.offsetTop,
            width: elem.offsetWidth,
            height: elem.offsetHeight
        };
        return bounds;
    }
        
    function ApplyLines() {
        var allQueues = $('.queueContainer');
        for (var i = 0; i < allQueues.length; i = i + 1) {
            var queue = $(allQueues[i]);
            var isVisible = $(queue).is(':visible');
            if (!isVisible) queue.show();
            var queueid = parseInt(allQueues[i].getAttribute('queueid'));
            DrawQueueLinesAndShapes(queueid, $(allQueues[i]));
            if (!isVisible) queue.hide();
        }
    }

    var tmpltContext = $('#templateEventContext').html();
    function AddEventButtons(centerLocationX, centerLocationY, queueID, btnContainer) {        
        var item = btnContainer.append('<div class="divAddEventButton" queueid="' + queueID + '" onclick="ShowEventAddSettings(this, ' + queueID + ')" style="background-color:#2C587A; left:' + (centerLocationX - 12) + 'px;top:' + (centerLocationY - 11) + 'px;"><i class="fa fa-plus"></i></div>');
        var contextMenu = $(tmpltContext);
        contextMenu.css({ left: centerLocationX + 10, top: centerLocationY - 70 });
        contextMenu.css('z-index', 6);
        contextMenu.attr('id', 'context_add_event_' + queueID);
        btnContainer.append(contextMenu);                    
    }

    function AddRuleButtons(centerLocationX, centerLocationY, queueID, eventID, btnContainer, bgColor) {        
        btnContainer.append('<div class="divAddEventButton" queueid="' + queueID + '" eventid="' + eventID + '" onclick="AddRule(' + queueID + ', ' + eventID + ')" style="background-color:' + bgColor + '; left:' + (centerLocationX - 12) + 'px;top:' + (centerLocationY - 11) + 'px;"><i class="fa fa-plus"></i></div>');
    }

    function AddConditionRemoveRuleButton(centerLocationX, centerLocationY, queueID, eventID, ruleID, btnContainer) {
        btnContainer.append('<div class="divRemoveRuleButton" queueid="' + queueID + '" eventid="' + eventID + '" onclick="RemoveRule(' + queueID + ', ' + eventID + ',' + ruleID + ')" style="background-color:#bbbbbb; left:' + (centerLocationX - 16) + 'px;top:' + (centerLocationY - 7) + 'px;"><i class="fa fa-times"></i></div>');
    }
}


//Queues
{
    function LoadQueues() {
        ShowWaitSpinner('Updating Designer');
        //GetQueues(string sessionID, short workflowID)
        $.ajax({
            url: "GetQueues/" + sessionID + "/" + workflowID,
            type: "Get",
            async: true,
            success: function (data) {
                try{
                    if (data.Data == null) {

                    }
                    else {
                        Queues = data.Data;
                        BuildFullPageStructure();
                    }
                    HideWaitSpinner();
                }
                catch (e) {
                    HideWaitSpinner();
                }
            },
            error: function (msg) {
                ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
            }
        });
    }

    function CreateQueue(workflowPathID) {
        //AddQueue/{sessionID}/{workflowID}/{workflowPathID}
        $.ajax({
            url: "AddQueue/" + sessionID + "/" + workflowID + "/" + workflowPathID,
            contentType: "application/json",
            type: "Post",
            success: function (data) {
                if (data.Data == null) {
                    alert(data.Message);
                }
                else {
                    LoadQueues();
                    ShowQueueSettings(parseInt(data.Data));
                }
            },
            error: function (msg) {
                alert(msg);
            }
        });
    }

    function SaveQueue() {
        //UpdateQueue(string sessionID, short workflowID, short queueID, [FromBody] string allQueueSettings)

        var queue = GetQueueByID(SelectedQueueID);
        if (queue == null) {
            ECMNotification("Selected Queue Not Found.", _notificationTypes.error);
        }
        else {
            var oData = {
                QueueName: queue.QueueName,
                QueueSettings: queue.Settings
            };
            ShowWaitSpinner('Saving Workflow Definition');
            $.ajax({
                url: "UpdateQueue/" + sessionID + "/" + workflowID + "/" + SelectedQueueID,
                type: "POST",
                dataType: 'json',
                async: true,
                contentType: 'application/json',
                data: JSON.stringify(oData),
                success: function (data) {
                    HideWaitSpinner();
                    if (data.Messages.ErrorMessages.length > 0) {
                        alert(data.Messages.ErrorMessages);
                    }
                    else {
                        LoadQueues();
                    }
                },
                error: function (msg) {
                    HideWaitSpinner();
                    alert(msg);
                }
            });
        }
    }

    function ConfirmQueueDelete() {
        $('#divSettingsModal').show();
        $('#divQueueSettings').fadeOut();
        $('#divQueueDeleteConfirm').show();
    }

    function CancelDeleteQueue() {
        $('#divSettingsModal').hide();
        $('#divQueueSettings').fadeIn();
        $('#divQueueDeleteConfirm').hide();
    }

    function DeleteQueue() {        
        //DeleteQueue(string sessionID, short workflowID, short queueID)
        var queue = GetQueueByID(SelectedQueueID);
        if (queue == null) throw ("Selected Queue Not Found.");
        $.ajax({
            url: "DeleteQueue/" + sessionID + "/" + workflowID + "/" + SelectedQueueID,
            type: "DELETE",
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    ECMNotification(msg, 3, stack);
                }
                else {
                    $('#divSettingsModal').hide();
                    $('#divQueueSettings').fadeOut();
                    SelectedQueueID = null;
                    LoadQueues();
                    $('#divQueueDeleteConfirm').hide();
                }
            },
            error: function (msg) {
                ECMNotification(msg.responseJSON.Message, 3);
            }
        });
                    
    }

    function ShowQueueSettings(queueID) {

        SelectedQueueID = queueID;
        var queue = GetQueueByID(SelectedQueueID);
        $('#txtQueueName').val(queue.QueueName);
                
        if (queue.Settings.UseDefaultWorkItemPage) {
            $("#rbUseDefault").prop("checked", true);
            $("#rbUseCustom").prop("checked", false);
        }
        else {
            $("#rbUseDefault").prop("checked", false);
            $("#rbUseCustom").prop("checked", true);
        }
        $("#txtCustomPage").val(queue.Settings.WorkItemPageURL);
        if (queue.Settings.UseDefaultMobileWorkItemPage) {
            $("#rbUseMobileDefault").prop("checked", true);
            $("#rbUseMobileCustom").prop("checked", false);
        }
        else {
            $("#rbUseMobileDefault").prop("checked", false);
            $("#rbUseMobileCustom").prop("checked", true);
        }
        $("#txtMobileCustomPage").val(queue.Settings.MobileWorkItemPageURL);
        var numerictextbox = $("#txtMobileMaxPages").data("kendoNumericTextBox");
        numerictextbox.value(queue.Settings.MobileMaxPageDisplay);
        $("#txtViewerURL").val(queue.Settings.ViewerURL);
        $("#cbShowHistory").prop("checked", queue.Settings.ShowHistory);
        $("#cbShowIndexing").prop("checked", queue.Settings.ShowIndexes);
        $("#cbShowComments").prop("checked", queue.Settings.ShowComments);
        $("#txtQueueAdditionalParameters").val(queue.Settings.AdditionalParameters);
        $("#txtQueueNotes").val(queue.Settings.Notes);
        
        for (var i = 0; i < queue.Settings.FieldSettings.length; i = i + 1) {
            try {
                var fs = queue.Settings.FieldSettings[i];
                var fieldRow = $('#tblQueueFields>tbody>tr[fieldid=' + fs.WorkflowFieldID + ']');
                
                $(fieldRow[0].cells[1].firstChild).prop("checked", fs.VisibleInQueue);
                if (fieldRow[0].cells[2].firstChild != undefined) {
                    $(fieldRow[0].cells[2].firstChild).prop("checked", fs.VisibleOnOpen);
                    $(fieldRow[0].cells[3].firstChild).prop("checked", fs.EditableOnOpen);
                    $(fieldRow[0].cells[4].firstChild).prop("checked", fs.Required);
                    $(fieldRow[0].cells[5].firstChild).prop("checked", fs.Important);
                }
            }
            catch (e) { }
        }

        $('#divSettingsModal').show();
        $('#divQueueSettings').fadeIn();
    }

    function CloseQueueSettings(save) {

        if (save) {
            var queue = GetQueueByID(SelectedQueueID);
            if (queue == null) throw "Selected Queue Not Found.";
            queue.QueueName = $('#txtQueueName').val();

            //-- general settings
            queue.Settings.UseDefaultWorkItemPage = $("#rbUseDefault").prop("checked").toString();            
            queue.Settings.WorkItemPageURL = $("#txtCustomPage").val();
            queue.Settings.UseDefaultMobileWorkItemPage = $("#rbUseMobileDefault").prop("checked").toString();
            queue.Settings.MobileWorkItemPageURL = $("#txtMobileCustomPage").val();
            var numerictextbox = $("#txtMobileMaxPages").data("kendoNumericTextBox");
            queue.Settings.MobileMaxPageDisplay = numerictextbox.value();
            queue.Settings.ViewerURL = $("#txtViewerURL").val();
            queue.Settings.ShowHistory = $("#cbShowHistory").prop("checked").toString();
            queue.Settings.ShowIndexes = $("#cbShowIndexing").prop("checked").toString();
            queue.Settings.ShowComments = $("#cbShowComments").prop("checked").toString();
            queue.Settings.AdditionalParameters = $("#txtQueueAdditionalParameters").val();
            queue.Settings.Notes = $("#txtQueueNotes").val();

            //-- field settings
            var aFieldSettings = [];
            var fielditems = $('#tblQueueFields>tbody>tr');
            for (var i = 0; i < fielditems.length; i = i + 1) {
                var item = fielditems[i];
                var sfid = $(item).attr("fieldid");
                var fid = parseInt(sfid);
                var viq = $(item.cells[1].firstChild).is(":checked");                
                var voo = (item.cells[2].firstChild == null ? false : $(item.cells[2].firstChild).is(":checked"));
                var eoo = (item.cells[3].firstChild == null ? false : $(item.cells[3].firstChild).is(":checked"));
                var roo = (item.cells[4].firstChild == null ? false : $(item.cells[4].firstChild).is(":checked"));
                var ioo = (item.cells[5].firstChild == null ? false : $(item.cells[5].firstChild).is(":checked"));
                aFieldSettings[i] = {
                    WorkflowFieldID: fid,
                    VisibleInQueue: viq,
                    VisibleOnOpen: voo,
                    EditableOnOpen: eoo,
                    Required: roo,
                    Important: ioo
                };
            }
            queue.Settings.FieldSettings = aFieldSettings;

            SaveQueue();
        }
        $('#divSettingsModal').hide();
        $('#divQueueSettings').fadeOut();
    }
}


//Events
{
    var SelectedEventType = '';
    var contextMenuShowing = false;
    var cancelContextHideEvent = false;
    $('body').click(function (e) {
        if (contextMenuShowing && !cancelContextHideEvent) {
            $('.eventContextMenu').hide();
        }
        else {
            cancelContextHideEvent = false;
        }
    });

    function ShowEventAddSettings(btn, queueID) {
        $('.eventContextMenu').hide();
        contextMenuShowing = true;
        cancelContextHideEvent = true;
        SelectedQueueID = queueID;
        $('#context_add_event_' + queueID).show();
        //var posTop = 0;
        //var pos = $(btn).position();
        //var halfContextHeight = $('#eventContextMenu').outerHeight()/2
        //var scrhght = $(document).height();

        //if (pos.top + halfContextHeight > scrhght) {
        //    posTop = pos.top;
        //}
        //else{
        //    posTop = pos.top + halfContextHeight;
        //}
        //$('#eventContextMenu').css({
        //    display: "block",
        //    left: pos.left+33,
        //    top: posTop
        //});
        return false;
    }

    function AddEventType(type) {
        SelectedEventType = type;
        var queue = GetQueueByID(SelectedQueueID);
        var eventTypeID = 0;
        var args = [];

        //-- get next event id
        var eventID = GetNextEventID(queue);
        SelectedEventID = eventID;

        var button = '';
        switch (SelectedEventType) {
            case 'Button':
                eventTypeID = 0;
                args[0] = "";
                args[1] = "#ffffff";
                args[2] = "#3facdb";
                args[3] = "true";
                args[4] = "false";
                button = "divEventButtonSettings";
                //-- default button text to white and background color
                $('#txtButtonName').val('');
                $('#txtButtonTextColor').val("#ffffff");
                $('#txtButtonColor').val("#3facdb");
                var color = kendo.parseColor('#ffffff');
                var colorbg = kendo.parseColor('#3facdb');
                buttonTextColorPicker.value(color);
                buttonColorPicker.value(colorbg);
                $('#cbButtonVisibleWorkItem').prop('checked', true);
                $('#cbButtonVisibleQueue').prop('checked', true);
                $('#btnExample').text('');
                $('#btnExample').css('color', '#ffffff');
                $('#btnExample').css('background-color', '#3facdb');

                //-- user select attributes
                //-- load the workflow paths
                $('#ddlEventButtonUserSelectWFPaths').empty();
                $.each(WorkflowPaths, function (i, item) {
                    $('#ddlEventButtonUserSelectWFPaths').append('<option value="' + item.WorkflowPathID + '">' + item.WorkflowPathName + '</option>');
                });
                $("#ddlEventButtonUserSelectWFPaths").prepend("<option value='ALL'>All</option>");
                $("#ddlEventButtonUserSelectWFPaths").prepend("<option value='NA'>Select</option>");

                $('#ddlEventButtonUserSelectWFPaths').val("NA");

                //-- default the queues to empty, will populate based on user selected workflow path
                $("#divEventButtonUserSelectQueues").html("");

                $("#cbEventButtonUserSelectEndInstance").prop("checked", true);
                $('#txtButtonTooltip').val('');
                break;

            case 'Timer':
                eventTypeID = 1;
                var timerType = $('[name=rbTimerType]:checked').attr('value');
                //-- default to daily
                args[0] = 1;
                args[1] = '';
                button = "divEventTimerSettings";
                break;

            case 'On Submit':
                eventTypeID = 2;
                button = "";
                break;

            case 'On Open':
                eventTypeID = 3;
                button = "";
                break;

            case 'On Arrival':
                eventTypeID = 4;
                button = "";
                break;
        }

        //-- add the placeholder event
        var event = {
            EventID: eventID,
            EventType: eventTypeID,
            Settings: args,
            Rules: [],
            VisibleInQueue: false,
            VisibleInWorkItem: false
        }
        queue.Settings.QueueEvents[queue.Settings.QueueEvents.length] = event;
        if(button!=""){
            //$("#" + button).fadeIn();
            //$('#divTabsEvents>ul>li').removeClass('active');
            //$('#aEventButtonAttributes').addClass('active');

            //$('#divEventButtonUserSelect').removeClass('active');
            //$('#divEventButtonAttributes').removeClass('active');
            //$('#divEventButtonAttributes').addClass('active');

            ShowEventSettings(queue.QueueID, event.EventID);
        }
        else{
            SaveQueue();
        }
    }

    function ShowEventSettings(queueID, eventID) {

        //-- switch on event type and show that modal here

        SelectedQueueID = queueID;
        SelectedEventID = eventID;

        var queue = GetQueueByID(SelectedQueueID);
        var event = GetEventByID(queue, SelectedEventID);

        //-- button attributes
        SelectedEventType = "Button";
        $('#txtButtonName').val('');
        $('#txtButtonTextColor').val('#ffffff');
        $('#txtButtonColor').val('#3facdb');
        var color = kendo.parseColor('#ffffff');
        var colorbg = kendo.parseColor('#3facdb');
        buttonTextColorPicker.value(color);
        buttonColorPicker.value(colorbg);
        $('#cbButtonVisibleWorkItem').prop('checked', true);
        $('#cbButtonVisibleQueue').prop('checked', false);
        $('#btnExample').text('');
        $('#btnExample').css('color', '#ffffff');
        $('#btnExample').css('background-color', '#3facdb');

        $('[name=rbTimerType]').prop('checked', false);
        $('#timer_Timeframe_Interval').val('');
        $('#timer_Timeframe_IntervalType').val('seconds');
        $('#timer_Timeframe_Repeat').prop('checked', true);
        $('#timer_Daily_Time').val('10:00 AM');
        $('[name=timer_Weekly_Days]').prop('checked', false);
        $('#timer_Weekly_Time').val('10:00 AM');
        $('#timer_Monthly_Day>option[value=1]').prop('selected', true);
        $('#timer_Monthly_Time').val('10:00 AM');
        if (SelectedEventID == null) {
            EventTypeChanged();
        }
        else {
            switch (event.EventType) {

                case 0: //button
                    SelectedEventType = 'Button';
                    $('#txtButtonName').val(event.Settings[0]);
                    $('#txtButtonTextColor').val(event.Settings[1]);
                    $('#txtButtonColor').val(event.Settings[2]);
                    $('#cbButtonVisibleWorkItem').prop('checked', event.Settings[3] == "true");
                    $('#cbButtonVisibleQueue').prop('checked', event.Settings[4] == "true");
                    $('#txtButtonTooltip').val(event.Settings[12]);
                    try {
                        var color = kendo.parseColor(event.Settings[1]);
                        var colorbg = kendo.parseColor(event.Settings[2]);
                        buttonTextColorPicker.value(color);
                        buttonColorPicker.value(colorbg);
                    }
                    catch (e) { }
                    $('#btnExample').text(event.Settings[0]);
                    $('#btnExample').css('color', event.Settings[1]);
                    $('#btnExample').css('background-color', event.Settings[2]);

                    $('#txtButtonName').select();

                    //-- user select attributes
                    //-- load the workflow paths
                    $('#ddlEventButtonUserSelectWFPaths').empty();
                    $.each(WorkflowPaths, function (i, item) {
                        $('#ddlEventButtonUserSelectWFPaths').append('<option value="' + item.WorkflowPathID + '">' + item.WorkflowPathName + '</option>');
                    });
                    $("#ddlEventButtonUserSelectWFPaths").prepend("<option value='ALL'>All</option>");
                    $("#ddlEventButtonUserSelectWFPaths").prepend("<option value='NA'>Select</option>");

                    //-- default the queues to empty, will populate based on user selected workflow path
                    $("#divEventButtonUserSelectQueues").html("");

                    //-- try to load user select settings
                    if (event.Settings.length > 5) {
                        $("#ddlEventButtonUserSelectWFPaths").val(event.Settings[5]);
                        //-- loads the queues div of queues
                        $('#ddlEventButtonUserSelectWFPaths').trigger('change');

                        var queueIDs = event.Settings[6].split(",");
                        $.each(queueIDs, function (i, item) {
                            $("#cbUserSelect_" + item).prop("checked", true);
                        });

                        $("#cbEventButtonUserSelectSendToUser").prop("checked", event.Settings[7] == "true");
                        $("#cbEventButtonUserSelectSetColor").prop("checked", event.Settings[8] == "true");
                        $("#txtUserSelectRowColor").val(event.Settings[9]);
                        try {
                            var color = kendo.parseColor(event.Settings[9]);
                            buttonUserSelectRowColorPicker.value(color);
                        }
                        catch (e) { }
                        $("#cbEventButtonUserSelectUseCurrentIcons").prop("checked", event.Settings[10] == "true");

                        var iconNames = event.Settings[11].split(",");
                        $.each(iconNames, function (i, item) {
                            if (item != "") {
                                $("[id^='cbIcon_'][iconName='" + item + "']").prop("checked", true);
                            }
                        });

                        $("#cbEventButtonUserSelectEndInstance").prop("checked", event.Settings[13] == "true");
                    }
                    else {
                        $("#ddlEventButtonUserSelectWFPaths").val('NA');
                    }

                    $('#divTabsEvents>ul>li').removeClass('active');
                    $('#aEventButtonAttributes').addClass('active');

                    $('#divEventButtonUserSelect').removeClass('active');
                    $('#divEventButtonAttributes').removeClass('active');
                    $('#divEventButtonAttributes').addClass('active');

                    $("#divEventButtonSettings").fadeIn();
                    break;

                case 1: //timer
                    SelectedEventType = 'Timer';
                    var timerType = $('[name=rbTimerType][value=' + event.Settings[0] + ']').prop('checked', true);
                    TimerTypeChanged();
                    switch (event.Settings[0]) {
                        case '0': //Timerframe
                            $('#timer_Timeframe_Interval').val(event.Settings[1]);
                            $('#timer_Timeframe_IntervalType').val(event.Settings[2])
                            $('#timer_Timeframe_Repeat').prop('checked', event.Settings[3] == 'true');
                            break;
                        case '1': //Daily
                            $('#timer_Daily_Time').val(event.Settings[1]);
                            break;
                        case '2': //Weekly
                            if (event.Settings[1] != null && event.Settings[1] != '') {
                                var days = event.Settings[1].split('|');
                                for (var i = 0; i < days.length; i = i + 1) {
                                    $('[name=timer_Weekly_Days][value=' + days[i] + ']').prop('checked', true);
                                }
                            }
                            $('#timer_Weekly_Time').val(event.Settings[2]);
                            break;
                        case '3': //Monthly
                            $('#timer_Monthly_Day>option[value=' + event.Settings[1] + ']').prop('selected', true);
                            $('#timer_Monthly_Time').val(event.Settings[2]);
                            break;
                    }

                    $("#divEventTimerSettings").fadeIn();
                    break;

                case 2://submit
                    SelectedEventType = 'On Submit';
                    $("#divEventSubmitSettings").fadeIn();
                    break;

                case 3://open

                    SelectedEventType = 'On Open';
                    $("#divEventOpenSettings").fadeIn();
                    break;

                case 4://arrival
                    SelectedEventType = 'On Arrival';
                    $("#divEventArrivalSettings").fadeIn();
                    break;
            }
        }
        $('#divSettingsModal').show();
    }

    function CloseEventSettings(save) {

        if (save) {
            var queue = GetQueueByID(SelectedQueueID);
            var eventTypeID = 0;
            var args = [];

            if (SelectedQueueID == -1) {
                eventTypeID = 5;
            }
            else {                
                switch (SelectedEventType) {
                    case 'Button':
                        //-- you must set button name and color
                        if ($('#txtButtonName').val() == "") {
                            ECMNotification("You must set the Button Text.", 2);
                            return;
                        }
                        if ($('#txtButtonColor').val() == "") {
                            ECMNotification("You must set the Button Color.", 2);
                            return;
                        }
                        eventTypeID = 0;
                        args[0] = $('#txtButtonName').val();
                        args[1] = $('#txtButtonTextColor').val();
                        args[2] = $('#txtButtonColor').val();
                        args[3] = $('#cbButtonVisibleWorkItem').is(':checked').toString();
                        args[4] = $('#cbButtonVisibleQueue').is(':checked').toString();
                        args[5] = $("#ddlEventButtonUserSelectWFPaths").val();

                        var cbUserSelectQueues = $("[id^='cbUserSelect_'");
                        var userSelectQueues = "";
                        $.each(cbUserSelectQueues, function (i, item) {
                            if ($(item).prop("checked")) {
                                if (userSelectQueues == "") {
                                    userSelectQueues = $(item).attr("queueid");
                                }
                                else {
                                    userSelectQueues = userSelectQueues + "," + $(item).attr("queueid");
                                }
                            }
                        });
                        args[6] = userSelectQueues;
                        args[7] = ($("#cbEventButtonUserSelectSendToUser").prop("checked")).toString();
                        args[8] = ($("#cbEventButtonUserSelectSetColor").prop("checked")).toString();
                        args[9] = $("#txtUserSelectRowColor").val();
                        args[10] = ($("#cbEventButtonUserSelectUseCurrentIcons").prop("checked")).toString();

                        var cbIcons = $("[id^='cbIcon_']:checked");
                        var iconNames = "";
                        $.each(cbIcons, function (i, item) {
                            if (iconNames == "") {
                                iconNames = $(item).attr("iconName")
                            }
                            else {
                                iconNames = iconNames + "," + $(item).attr("iconName")
                            }
                        });
                        args[11] = iconNames;



                        args[12] = $('#txtButtonTooltip').val();;
                        args[13] = ($("#cbEventButtonUserSelectEndInstance").prop("checked")).toString();

                        break;

                    case 'Timer':
                        eventTypeID = 1;
                        var timerType = $('[name=rbTimerType]:checked').attr('value');
                        args[0] = timerType;
                        switch (timerType) {
                            case '0': //Timerframe
                                args[1] = $('#timer_Timeframe_Interval').val();
                                args[2] = $('#timer_Timeframe_IntervalType').val()
                                args[3] = ($('#timer_Timeframe_Repeat').prop("checked")).toString();
                                break;
                            case '1': //Daily
                                args[1] = $('#timer_Daily_Time').val();
                                break;
                            case '2': //Weekly
                                args[1] = $('[name=timer_Weekly_Days]:checked').map(function () {
                                    return $(this).attr('value');
                                }).get().join('|');
                                args[2] = $('#timer_Weekly_Time').val();
                                break;
                            case '3': //Monthly
                                args[1] = $('#timer_Monthly_Day').val();
                                args[2] = $('#timer_Monthly_Time').val();
                                break;
                        }
                        break;

                    case 'On Submit':
                        eventTypeID = 2;
                        break;

                    case 'On Open':
                        eventTypeID = 3;
                        break;

                    case 'On Arrival':
                        eventTypeID = 4;
                        break;
                }
            }

            if (SelectedEventID == null) {
                var eventID = GetNextEventID(queue);
                var event = {
                    EventID: eventID,
                    EventType: eventTypeID,
                    Settings: args,
                    Rules: []
                }
                queue.Settings.QueueEvents[queue.Settings.QueueEvents.length] = event;
            }
            else {
                //var event = GetEventByID(queue, SelectedEventID);
                var event = GetEventByType(queue);
                event.EventType = eventTypeID;
                event.Settings = args;
            }
            SaveQueue();
        }
        else {
            LoadQueues();
        }
        $('#divSettingsModal').hide();
        $("#divEventButtonSettings").hide();
        $("#divEventTimerSettings").hide();
        $("#divEventSubmitSettings").hide();
        $("#divEventArrivalSettings").hide();
        $("#divEventOpenSettings").hide();
    }

    function eventDelete() {

        if (confirm('Are you sure you want to delete?')) {

            var queue = GetQueueByID(SelectedQueueID);
            queue.Settings.QueueEvents = $.grep(queue.Settings.QueueEvents, function (e) { return e.EventID != SelectedEventID });

            $('#divSettingsModal').hide();
            $("#divEventButtonSettings").hide();
            $("#divEventTimerSettings").hide();
            $("#divEventSubmitSettings").hide();
            $("#divEventArrivalSettings").hide();
            $("#divEventOpenSettings").hide();

            SaveQueue();
        }
    }

    function TimerTypeChanged() {
        var checkedItem = $('[name=rbTimerType]:checked');
        $('[timerType]').hide();
        $('[timerType=' + checkedItem.attr('value') + ']').slideDown();
    }

    //-- user select send to
    $('#ddlEventButtonUserSelectWFPaths').change(function (e) {

        var queues = $.grep(Queues, function (e) { return e.QueueID != -1 });;
        selectedValue = parseInt($(this).val());

        if ($(this).val() != "ALL") {
            queues = $.grep(Queues, function (e) { return e.WorkflowPathID == selectedValue });
        }
        //-- load the queues
        var queuesHtml = "<table>";
        $.each(queues, function (i, item) {
            queuesHtml = queuesHtml +
                '<tr>' +
                '<td style="vertical-align:top;">' +
                '<input id="cbUserSelect_' + item.QueueID.toString() + '" queueid="' + item.QueueID + '" class="control" type="checkbox" />' +
                '<label for="cbUserSelect_' + item.QueueID.toString() + '" tabindex="1"></label>' +
                '</td>' +
                '<td>' +
                '<label class="control-label" for="cbUserSelect_' + item.QueueID.toString() + '" style="padding-left:4px; word-wrap:break-word; width:200px;">' + item.QueueName + '</label>' +
                '</td>' +
                '</tr>';
        });
        queuesHtml = queuesHtml + "</tr></table>";
        $("#divEventButtonUserSelectQueues").html(queuesHtml);
    });
}


//start rules
{
    var StartWorkflowPathID = null;
    function ShowStartRules(workflowPathID) {
        StartWorkflowPathID = workflowPathID;        
        SelectedQueueID = -1;
        SelectedEventID = 0;        
        var path = $.grep(WorkflowPaths, function (e) { return e.WorkflowPathID == StartWorkflowPathID })[0];
        $('#lblStartRulesPathHeader').text(path.WorkflowPathName);
        
        $("#divStartConfiguration").hide();
        $("#divStartConditionTypeSelection").hide();
        $("#divStartActionTypeSelection").show();        
        
        SetupStartRulesTable();

        $('#windowStartRules').fadeIn();
        $('#divSettingsModal').show();
    }

    function SetupStartRulesTable() {
        var queue = GetQueueByID(-1);
        var rules = $.grep(queue.Settings.QueueEvents[0].Rules, function (e) { return e.WorkflowPathName == StartWorkflowPathID });
        var sRules = '';
        for (var iRule = 0; iRule < rules.length; iRule = iRule + 1) {
            var rule = rules[iRule];
            var divConditions = $(htmlConditions);
            divConditions.attr('queueid', queue.QueueID);
            divConditions.attr('eventid', event.EventID);
            divConditions.attr('ruleid', rule.RuleID);
            var divConditionsAddLink = $($(divConditions.children()[0]).children()[0]);
            divConditionsAddLink.attr('onclick', 'AddStartCondition(' + rule.RuleID + ');');
            var divConditionsArea = $(divConditions.children()[1]);
            if (rule.Conditions.length == 0) {
                //divConditions.prepend('<label>No Conditions</label>');
            }
            else {
                for (var iCond = 0; iCond < rule.Conditions.length; iCond = iCond + 1) {
                    var condition = rule.Conditions[iCond];
                    var cndtn = $('<tr><td onclick="EditStartCondition(' + rule.RuleID + ',' + condition.ConditionID + ')">' + HtmlEncode(condition.ConditionDisplayText) + '</td><td><button class="btn"  onclick="RemoveStartCondition(' + rule.RuleID + ',' + condition.ConditionID + ')"><i class="fa fa-times-rectangle"></i></button></td></tr>');
                    divConditionsArea.append(cndtn);
                }
            }

            var divActions = $(htmlActions);
            divActions.attr('queueid', queue.QueueID);
            divActions.attr('eventid', event.EventID);
            divActions.attr('ruleid', rule.RuleID);
            var divActionsAddLink = $($(divActions.children()[0]).children()[0]);
            divActionsAddLink.attr('onclick', 'AddStartAction(' + rule.RuleID + ');');
            var divActionsArea = $(divActions.children()[1]);
            var divProgressionArea = $(divActions.children()[2]);
            if (rule.Actions.length == 0) {
                //divActions.prepend('<label>No Actions</label>');
            }
            else {
                for (var iAct = 0; iAct < rule.Actions.length; iAct = iAct + 1) {
                    var action = rule.Actions[iAct];

                    var actn = $('<tr><td onclick="EditStartAction(' + rule.RuleID + ',' + action.ActionID + ')">' + HtmlEncode(action.ActionDisplayText) + '</td><td><button class="btn" onclick="RemoveStartAction(' + rule.RuleID + ',' + action.ActionID + ')"><i class="fa fa-times-rectangle"></i></button></td></tr>');
                    if (action.LinkedQueueID != null) {
                        //This will highlight Send To Types of Actions
                        //$(actn.children()[0]).css('font-weight', 'bold');
                        $(actn.children()[0]).css('color', '#2C587A');
                        divProgressionArea.append(actn);
                    }
                    else {
                        divActionsArea.append(actn);
                    }
                }
            }

            sRules = sRules + '<tr><td style="padding-bottom:3px; padding-right:3px;">' + divConditions.prop('outerHTML') + '</td>' + '<td>' + divActions.prop('outerHTML') + '</td><td><button style="border:none; background-color:transparent; width:25px; color:darkred;" onclick="RemoveStartRule(' + rule.RuleID + ')"><i class="fa fa-times"></i></button></td></tr>';
        }
        $('#tblStartRules>tbody').html(sRules);
    }

    function CloseStartSettings() {
        SelectedQueueID = null;
        SelectedEventID = null;
        StartWorkflowPathID = null;
        $('#windowStartRules').fadeOut();
        $('#divSettingsModal').hide();
    }

    function AddStartRule() {
        var queue = GetQueueByID(-1);
        var event = queue.Settings.QueueEvents[0];        
        SelectedQueueID = queue.QueueID;

        var orule = {
            RuleID: GetNextRuleID(event),
            RuleName: '',
            Conditions: [],
            Actions: [],
            WorkflowPathName: StartWorkflowPathID
        };
        event.Rules[event.Rules.length] = orule;
        SaveQueue();
        SetupStartRulesTable();
    }
    function RemoveStartRule(ruleid) {
        var queue = GetQueueByID(-1);
        var event = queue.Settings.QueueEvents[0];        
        SelectedQueueID = queue.QueueID;

        for (var i = 0; i < event.Rules.length; i = i + 1) {
            if (event.Rules[i].RuleID == ruleid) {
                event.Rules.splice(i, 1);
                SaveQueue();
                SetupStartRulesTable();
                break;
            }
        }
    }
        
    var IsAction = null;
    function AddStartCondition(ruleid) {
        var queue = GetQueueByID(-1);
        SelectedQueueID = -1;
        var event = queue.Settings.QueueEvents[0];
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;
        SelectedConditionType = GetConditionByConditionName("Field Comparison");        
        SelectedConditionSettings = [];
        IsAction = false;

        $('#ddlStartConditionTypes').val("Field Comparison");
        $('#iframeStartConfig').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedConditionType.ConfigurationPage));
        $('#divStartConditionTypeSelection').show();
        $('#divStartActionTypeSelection').hide();        
        $('#divStartConfiguration').fadeIn();
    }
    function EditStartCondition(ruleid, conditionid) {
        var queue = GetQueueByID(-1);
        SelectedQueueID = -1;
        var event = queue.Settings.QueueEvents[0];
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;
        SelectedCondition = GetConditionByID(SelectedRule, conditionid);
        SelectedConditionType = GetConditionByConditionName(SelectedCondition.ConditionTypeName);
        SelectedConditionSettings = SelectedCondition.ConditionSettings;
        IsAction = false;
        
        $('#ddlStartConditionTypes').val(SelectedConditionType.ConditionName);
        $('#iframeStartConfig').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedConditionType.ConfigurationPage));
        $('#divStartConditionTypeSelection').show();
        $('#divStartActionTypeSelection').hide();
        $('#divStartConfiguration').fadeIn();
    }
    function RemoveStartCondition(ruleid, conditionid) {
        var queue = GetQueueByID(-1);        
        var event = queue.Settings.QueueEvents[0];
        var rule = GetRuleByID(event, ruleid);
        for(var i = 0; i<rule.Conditions.length; i=i+1){
            if(rule.Conditions[i].ConditionID==conditionid){
                rule.Conditions.splice(i,1);
                SelectedQueueID = -1;
                SaveQueue();
                SetupStartRulesTable();
                break;
            }
        }
    }
    function ddlStartConditionChanged(ddl){
        var selectedText = $("#ddlStartConditionTypes option:selected").text();
        SelectedConditionType = GetConditionByConditionName(selectedText);
        SelectedConditionSettings = [];
        $('#iframeStartConfig').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedConditionType.ConfigurationPage));
    }

    

    function AddStartAction(ruleid) {
        var queue = GetQueueByID(-1);
        SelectedQueueID = -1;
        var event = queue.Settings.QueueEvents[0];
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;        
        SelectedActionSettings = [];
        IsAction = true;

        $('#ddlStartActionTypes').val("NA");
        $('#iframeStartConfig').attr('src', null);        
        $('#divStartActionTypeSelection').show();
        $('#divStartConditionTypeSelection').hide();
        $('#divStartConfiguration').fadeIn();
    }
    function EditStartAction(ruleid, actionid) {
        var queue = GetQueueByID(-1);
        SelectedQueueID = -1;
        var event = queue.Settings.QueueEvents[0];
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;
        SelectedAction = GetActionByID(SelectedRule, actionid);
        SelectedActionType = GetActionByActionName(SelectedAction.ActionTypeName);
        SelectedActionSettings = SelectedAction.ActionSettings;
        IsAction = true;

        $('#ddlStartActionTypes').val(SelectedActionType.ActionName);
        $('#iframeStartConfig').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedActionType.ConfigurationPage));
        $('#divStartActionTypeSelection').show();
        $('#divStartConditionTypeSelection').hide();
        $('#divStartConfiguration').fadeIn();
    }
    function RemoveStartAction(ruleid, actionid) {
        var queue = GetQueueByID(-1);        
        var event = queue.Settings.QueueEvents[0];
        var rule = GetRuleByID(event, ruleid);
        for (var i = 0; i < rule.Actions.length;i=i+1){
            if(rule.Actions[i].ActionID==actionid){
                rule.Actions.splice(i,1);
                SelectedQueueID = -1;
                SaveQueue();
                SetupStartRulesTable();
                break;
            }
        }        
    }
    function ddlStartActionChanged(ddl) {
        var selectedText = $("#ddlStartActionTypes option:selected").text();
        SelectedActionType = GetActionByActionName(selectedText);
        SelectedActionSettings = [];
        $('#iframeStartConfig').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedActionType.ConfigurationPage));
    }

    function SaveStartActionConditionSettings(save) {
        var closeWindow = true;
        var displayName = null;
        if (save) {
            if (IsAction == false) {
                var conditionName = $("#ddlStartConditionTypes").val();
                var settings = document.getElementById("iframeStartConfig").contentWindow.GetConditionSettings();
                var settingsdata = {
                    TypeName: conditionName,
                    Settings: settings
                };
                $.ajax({
                    url: "ValidateConditionSettings/" + sessionID,
                    contentType: "application/json",
                    type: "Post",
                    data: JSON.stringify(settingsdata),
                    async: false,
                    success: function (data) {
                        if (data.Data == null) {
                            ECMNotification(data.Messages.ErrorMessages[0], 3, data.StackTrace);
                            closeWindow = false;
                        }
                        else {
                            displayName = data.Data;
                        }
                    },
                    error: function (msg) {
                        alert(msg);
                    }
                });

                if (closeWindow) {
                    if (SelectedCondition == null) {
                        var ocondition = {
                            ConditionID: GetNextConditionID(SelectedRule),
                            ConditionTypeName: conditionName,
                            ConditionSettings: settings,
                            ConditionDisplayText: displayName
                        };
                        SelectedRule.Conditions[SelectedRule.Conditions.length] = ocondition;
                    }
                    else {
                        SelectedCondition.ConditionTypeName = conditionName;
                        SelectedCondition.ConditionSettings = settings;
                    }

                    SaveQueue();
                    
                }
            }
            else {
                var actionName = $("#ddlStartActionTypes").val();
                if (actionName == "NA" || actionName == "Select") {
                    ECMNotification("You must select a Condition Type.", 2);
                    closeWindow = false;
                }
                else {
                    var settings = document.getElementById("iframeStartConfig").contentWindow.GetActionSettings();
                    var settingsdata = {
                        TypeName: actionName,
                        Settings: settings
                    };
                    $.ajax({
                        url: "ValidateActionSettings/" + sessionID,
                        contentType: "application/json",
                        type: "Post",
                        data: JSON.stringify(settingsdata),
                        async: false,
                        success: function (data) {
                            if (data.Data == null) {
                                ECMNotification(data.Messages.ErrorMessages[0], 3, data.StackTrace);
                                closeWindow = false;
                            }
                            else {
                                displayName = data.Data;
                            }
                        },
                        error: function (msg) {
                            alert(msg);
                        }
                    });

                    if (closeWindow) {
                        if (SelectedAction == null) {
                            var oaction = {
                                ActionID: GetNextActionID(SelectedRule),
                                ActionTypeName: actionName,
                                ActionSettings: settings,
                                ActionDisplayText: displayName
                            };
                            SelectedRule.Actions[SelectedRule.Actions.length] = oaction;
                        }
                        else {
                            SelectedAction.ActionTypeName = actionName;
                            SelectedAction.ActionSettings = settings;
                        }

                        SaveQueue();
                    }
                }
            }
        }
        if (closeWindow) {
            SelectedQueueID = null;
            SelectedEventID = null;
            SelectedRule = null;
            SelectedRuleID = null;
            SelectedConditionType = null;
            SelectedCondition = null;
            SelectedConditionSettings = null;
            SelectedActionType = null;
            SelectedAction = null;
            SelectedActionSettings = null;
            $('#iframeStartConfig').attr('src', null);
            $('#divStartConfiguration').fadeOut();
            SetupStartRulesTable();
        }
    }
}


//Rules
{

    function AddRule(queueid, eventid) {
        var queue = GetQueueByID(queueid);
        var event = GetEventByID(queue, eventid);
        SelectedQueueID = queue.QueueID;

        var orule = {
            RuleID: GetNextRuleID(event),
            RuleName: '',
            Conditions: [],
            Actions: [],
            WorkflowPathName: queue.WorkflowPathID
        };
        event.Rules[event.Rules.length] = orule;
        SaveQueue();
    }

    function RemoveRule(queueid, eventid, ruleid) {
        tempDelRulequeueid = queueid;
        tempDelRuleeventid = eventid;
        tempDelRuleruleid = ruleid;
        $('#divRuleDeleteConfirm').fadeIn();
        $('#divSettingsModal').show();
    }

    var tempDelRulequeueid = null;
    var tempDelRuleeventid = null;
    var tempDelRuleruleid = null;
    function DeleteRule() {
        queueid = tempDelRulequeueid;
        eventid = tempDelRuleeventid;
        ruleid = tempDelRuleruleid;
        var queue = GetQueueByID(queueid);
        var event = GetEventByID(queue, eventid);
        for (var i = 0; i < event.Rules.length; i = i + 1) {
            if (event.Rules[i].RuleID == ruleid) {                
                event.Rules.splice(i, 1);
                SelectedQueueID = queue.QueueID;
                SaveQueue();                          
                break;
            }
        }
        $('#divRuleDeleteConfirm').fadeOut();
        $('#divSettingsModal').hide();
    }

    function CancelDeleteRule() {
        $('#divRuleDeleteConfirm').fadeOut();
        $('#divSettingsModal').hide();
    }

    function RemoveRuleConfirmation(question, queue, event, i) {
        var defer = $.Deferred();
        $('<div class="center"></div>')
            .html(question)
            .dialog({
                autoOpen: true,
                modal: true,
                title: 'Confirmation',
                buttons: {
                    "Yes": function () {
                        defer.resolve("true");//this text 'true' can be anything. But for this usage, it should be true or false.
                        $(this).dialog("close");
                    },
                    "No": function () {
                        defer.resolve("false");//this text 'false' can be anything. But for this usage, it should be true or false.
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    //$(this).remove();
                    $(this).dialog('destroy').remove()
                }
            });
        return defer.promise();
    };

}


//Conditions
{
    var SelectedConditionType = null;
    var SelectedCondition = null;
    var SelectedConditionSettings = null;
    function AddCondition(queueid, eventid, ruleid) {
        var queue = GetQueueByID(queueid);
        SelectedQueueID = queue.QueueID;
        var event = GetEventByID(queue, eventid);
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;
        SelectedConditionType = GetConditionByConditionName("Field Comparison");
        SelectedConditionSettings = [];
        $('#ddlConditionTypes').val("Field Comparison");
        $('#iframeConditionSettings').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedConditionType.ConfigurationPage));

        $('#windowConditionSettings').fadeIn();
        $('#divSettingsModal').show();
    }

    function EditCondition(queueid, eventid, ruleid, conditionid) {
        var queue = GetQueueByID(queueid);
        SelectedQueueID = queue.QueueID;
        var event = GetEventByID(queue, eventid);
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;
        SelectedCondition = GetConditionByID(SelectedRule, conditionid);
        SelectedConditionType = GetConditionByConditionName(SelectedCondition.ConditionTypeName);
        SelectedConditionSettings = SelectedCondition.ConditionSettings;

        $('#ddlConditionTypes').val(SelectedConditionType.ConditionName);
        $('#iframeConditionSettings').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedConditionType.ConfigurationPage));

        $('#windowConditionSettings').fadeIn();
        $('#divSettingsModal').show();
    }

    function SaveCondition(save) {
        var closeWindow = true;
        var conditionName = $("#ddlConditionTypes").val();
        if (save) {
            if (conditionName == "NA" || conditionName == "Select") {
                ECMNotification("You must select a Condition Type.", 2);
                closeWindow = false;
            }
            else {
                var settings = document.getElementById("iframeConditionSettings").contentWindow.GetConditionSettings();                                
                var settingsdata = {
                    TypeName: conditionName,
                    Settings: settings
                };
                $.ajax({
                    url: "ValidateConditionSettings/" + sessionID,
                    contentType: "application/json",
                    type: "Post",
                    data: JSON.stringify(settingsdata),
                    async: false,
                    success: function (data) {
                        if (data.Data == null) {
                            ECMNotification(data.Messages.ErrorMessages[0], 3, data.StackTrace);
                            closeWindow = false;
                        }
                        //else {
                        //    displayName = data.Data;
                        //}
                    },
                    error: function (msg) {
                        alert(msg);
                    }
                });

                if (closeWindow) {
                    if (SelectedCondition == null) {
                        var ocondition = {
                            ConditionID: GetNextConditionID(SelectedRule),
                            ConditionTypeName: conditionName,
                            ConditionSettings: settings
                        };
                        SelectedRule.Conditions[SelectedRule.Conditions.length] = ocondition;
                    }
                    else {
                        SelectedCondition.ConditionTypeName = conditionName;
                        SelectedCondition.ConditionSettings = settings;
                    }

                    SaveQueue();
                }
            }
        }

        if (closeWindow) {
            SelectedQueueID = null;
            SelectedEventID = null;
            SelectedRule = null;
            SelectedRuleID = null;
            SelectedConditionType = null;
            SelectedCondition = null;
            SelectedConditionSettings = null;
            $('#iframeConditionSettings').attr('src', null);
            $('#windowConditionSettings').fadeOut();
            $('#divSettingsModal').hide();
        }
    }

    function RemoveCondition(queueid, eventid, ruleid, conditionid) {
        var queue = GetQueueByID(queueid);        
        var event = GetEventByID(queue, eventid);        
        var rule = GetRuleByID(event, ruleid);        
        for (var i = 0; i < rule.Conditions.length; i = i + 1) {
            if (rule.Conditions[i].ConditionID == conditionid) {
                rule.Conditions.splice(i, 1);
                break;
            }
        }
        SelectedQueueID = queue.QueueID;
        SaveQueue();

    }

    function ddlConditionTypeChange(ddl) {
        var selectedText = $("#ddlConditionTypes option:selected").text();
        SelectedConditionType = GetConditionByConditionName(selectedText);
        SelectedConditionSettings = [];
        $('#iframeConditionSettings').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedConditionType.ConfigurationPage));
    }

    function SetConditionWindowSize(dimensions) {
        $('#windowConditionSettings').css('width', dimensions.width + 57);
        $('#windowConditionSettings').css('height', dimensions.height + 175);
    }

    function LoadConditionSettings() {
        return SelectedConditionSettings;
    }
}


//Actions
{
    var SelectedActionType = null;
    var SelectedAction = null;
    var SelectedActionSettings = null;
    function AddAction(queueid, eventid, ruleid) {
        var queue = GetQueueByID(queueid);
        SelectedQueueID = queue.QueueID;
        var event = GetEventByID(queue, eventid);
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;        
        SelectedActionSettings = [];
        
        LoadActionTypesByEventType(event.EventType);

        $('#ddlActionTypes').val("NA");

        $('#windowActionSettings').fadeIn();
        $('#divSettingsModal').show();
    }

    function EditAction(queueid, eventid, ruleid, actionid) {
        var queue = GetQueueByID(queueid);
        SelectedQueueID = queue.QueueID;
        var event = GetEventByID(queue, eventid);
        SelectedEventID = event.EventID;
        SelectedRule = GetRuleByID(event, ruleid);
        SelectedRuleID = SelectedRule.RuleID;
        SelectedAction = GetActionByID(SelectedRule, actionid);
        SelectedActionType = GetActionByActionName(SelectedAction.ActionTypeName);
        SelectedActionSettings = SelectedAction.ActionSettings;

        LoadActionTypesByEventType(event.EventType);

        $('#ddlActionTypes').val(SelectedActionType.ActionName);
        $('#iframeActionSettings').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedActionType.ConfigurationPage));

        $('#windowActionSettings').fadeIn();
        $('#divSettingsModal').show();
    }

    function LoadActionTypesByEventType(eventType) {

        $('#ddlActionTypes').empty();
        $('#ddlActionTypes').append('<option value="NA" selected="selected">Select</option>');
        for (i = 0; i < Actions.length; i = i + 1) {
            switch (eventType) {
                //-- button
                case 0:
                    if (Actions[i].ShowForActionTypeButton) {
                        $('#ddlActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                    }
                    break;
                case 1:
                    if (Actions[i].ShowForActionTypeTimer) {
                        $('#ddlActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                    }
                    break;
                case 2:
                    if (Actions[i].ShowForActionTypeOnSubmit) {
                        $('#ddlActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                    }
                    break;
                case 3:
                    if (Actions[i].ShowForActionTypeOnOpen) {
                        $('#ddlActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                    }
                    break;
                case 4:
                    if (Actions[i].ShowForActionTypeOnArrival) {
                        $('#ddlActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                    }
                    break;
                case 5:
                    if (Actions[i].ShowForActionTypeKickoff) {
                        $('#ddlActionTypes').append('<option actionIndex="' + i + '">' + Actions[i].ActionName + '</option>');
                    }
                    break;
            }
        }
    }

    function SaveAction(save) {
        var closeWindow = true;
        var actionName = $("#ddlActionTypes").val();
        if (save) {
            if (actionName == "NA" || actionName == "Select") {
                ECMNotification("You must select a Condition Type.", 2);
                closeWindow = false;
            }
            else {
                var settings = document.getElementById("iframeActionSettings").contentWindow.GetActionSettings();
                var settingsdata = {
                    TypeName: actionName,
                    Settings: settings
                };
                $.ajax({
                    url: "ValidateActionSettings/" + sessionID,
                    contentType: "application/json",
                    type: "Post",
                    data: JSON.stringify(settingsdata),
                    async: false,
                    success: function (data) {
                        if (data.Data == null) {
                            ECMNotification(data.Messages.ErrorMessages[0], 3, data.StackTrace);
                            closeWindow = false;
                        }
                        //else {
                        //    displayName = data.Data;
                        //}
                    },
                    error: function (msg) {
                        alert(msg);
                    }
                });

                if (closeWindow) {
                    if (SelectedAction == null) {
                        var oaction = {
                            ActionID: GetNextActionID(SelectedRule),
                            ActionTypeName: actionName,
                            ActionSettings: settings
                        };
                        SelectedRule.Actions[SelectedRule.Actions.length] = oaction;
                    }
                    else {
                        SelectedAction.ActionTypeName = actionName;
                        SelectedAction.ActionSettings = settings;
                    }

                    SaveQueue();
                }
            }
        }

        if (closeWindow) {
            SelectedQueueID = null;
            SelectedEventID = null;
            SelectedRule = null;
            SelectedRuleID = null;
            SelectedActionType = null;
            SelectedAction = null;
            SelectedActionSettings = null;
            $('#iframeActionSettings').attr('src', null);
            $('#windowActionSettings').fadeOut();
            $('#divSettingsModal').hide();
        }
    }

    function RemoveAction(queueid, eventid, ruleid, actionid) {
        var queue = GetQueueByID(queueid);
        var event = GetEventByID(queue, eventid);
        var rule = GetRuleByID(event, ruleid);
        for (var i = 0; i < rule.Actions.length; i = i + 1) {
            if (rule.Actions[i].ActionID == actionid) {
                rule.Actions.splice(i, 1);
                break;
            }
        }
        SelectedQueueID = queue.QueueID;
        SaveQueue();
    }

    function ddlActionTypeChange(ddl) {
        var selectedText = $("#ddlActionTypes option:selected").text();
        SelectedActionType = GetActionByActionName(selectedText);
        SelectedActionSettings = [];
        $('#iframeActionSettings').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedActionType.ConfigurationPage));
    }

    function SetActionWindowSize(dimensions) {
        //$('#divActionConfig').css('width', dimensions.width + 57);
        //$('#divActionConfig').css('height', dimensions.height + 100);

        //$('#iframeEventButtonRuleAction').css('width', dimensions.width + 57);
        //$('#iframeEventButtonRuleAction').css('height', dimensions.height + 100);
    }

    function LoadActionSettings() {
        return SelectedActionSettings;
    }
}


//Icon Management
{
    function iconsFormat(iconsSetup) {

        var iconsSetupHtml = "<table>";

        if (!jQuery.isEmptyObject(iconsSetup)) {
            $.each(iconsSetup, function (i, item) {
                iconsSetupHtml = iconsSetupHtml + "<tr>";
                iconsSetupHtml = iconsSetupHtml + "<td style='width:400px;'>";
                iconsSetupHtml = iconsSetupHtml + "<input id='cbIcon_" + i.toString() + "' iconName='" + item.Name + "' class='control' type='checkbox' />";
                iconsSetupHtml = iconsSetupHtml + "<label for='cbIcon_" + i.toString() + "' tabindex='" + i.toString() + "' style='padding-left:4px;'></label>";
                iconsSetupHtml = iconsSetupHtml + "<label class='control-label' style='padding-left:2px; padding-right:2px;'>" + "&nbsp" + item.Html + "&nbsp" + item.Name + "</label>";
                iconsSetupHtml = iconsSetupHtml + "</td>";
                iconsSetupHtml = iconsSetupHtml + "</tr>";
            });
        }
        iconsSetupHtml = iconsSetupHtml + "</table>";
        return iconsSetupHtml;
    }


    function showIconManagement() {
        //-- show popup with iframe to iconmanagement.html page
        $('#iframeIconManagement').attr('src', "./" + "IconManagement.html");
        $('#windowIconManagement').fadeIn();
        $('#divSettingsModal').show();
    }

    function hideIconManagement() {
        $('#iframeIconManagement').attr('src', null);
        $('#windowIconManagement').fadeOut();
        $('#divSettingsModal').hide();
        if (SelectedActionType == null) {
            //-- returning back from button user select 
            _iconsSetup = LoadSetupIcons();
            $("#iconsSetup").html(iconsFormat(_iconsSetup));
        }
        else {
            //-- returning back from iactions
            $('#iframeActionSettings').attr('src', "DynamicLibraries/" + SetPageUnique(SelectedActionType.ConfigurationPage));
        }
    }

    function LoadSetupIcons() {
        var icons = null;
        $.ajax({
            url: "GetWorkflowPresetIcons/" + sessionID + "/" + workflowID,
            type: "Get",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                try {
                    if (data.Messages.ErrorMessages.length > 0) {
                        var msg = data.Messages.ErrorMessages[0];
                        ECMNotification(msg, _notificationTypes.error, data.Stack);
                    }
                    else {
                        icons = data.Data;
                    }
                }
                catch (e) {
                }
            },
            error: function (msg) {
                ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
            }
        });
        return icons;
    }

    function AddIconSetup(iconData) {
        var result = "";
        $.ajax({
            url: "AddWorkflowPresetIcon/" + sessionID + "/" + workflowID,
            type: "Post",
            dataType: "json",
            data: JSON.stringify(iconData),
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                try {
                    if (data.Messages.ErrorMessages.length > 0) {
                        var msg = data.Messages.ErrorMessages[0];
                        ECMNotification(msg, _notificationTypes.error, data.Stack);
                    }
                    else {
                        result = data.Data;
                    }
                }
                catch (e) {
                }
            },
            error: function (msg) {
                ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
            }
        });
        return result;
    }

    function RemoveIconSetup(iconData) {
        var result = "";
        $.ajax({
            url: "RemoveWorkflowPresetIcon/" + sessionID + "/" + workflowID,
            type: "Post",
            dataType: "json",
            data: JSON.stringify(iconData),
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                try {
                    if (data.Messages.ErrorMessages.length > 0) {
                        var msg = data.Messages.ErrorMessages[0];
                        ECMNotification(msg, _notificationTypes.error, data.Stack);
                    }
                    else {
                        result = data.Data;
                    }
                }
                catch (e) {
                }
            },
            error: function (msg) {
                ECMNotification(msg.statusText, _notificationTypes.error, msg.responseText);
            }
        });
        return result;
    }
}


//Workflow Paths
{
    var currentWFPath = null;
    function ShowWorkflowPathEditor(wfPathID) {

        if (wfPathID == null) {
            currentWFPath = null;
            $('#txtWorkflowPathName').val('');
            $('#btnDeletePath').hide();
        }
        else {
            currentWFPath = wfPathID

            for (var i = 0; i < WorkflowPaths.length; i = i + 1) {
                if (WorkflowPaths[i].WorkflowPathID == currentWFPath) {
                    $('#txtWorkflowPathName').val(WorkflowPaths[i].WorkflowPathName);
                    break;
                }
            }
            $('#btnDeletePath').show();
        }
        $('#divSettingsModal').show();
        $('#divWorkflowPathEditor').fadeIn();
        $('#txtWorkflowPathName').select();
    }

    function CloseWorkflowPathEditor(save) {
        if (save) {
            var pathName = $('#txtWorkflowPathName').val();
            if (currentWFPath == null) {
                //AddWorkflowPath/{sessionID}/{workflowID}
                $.ajax({
                    url: "AddWorkflowPath/" + sessionID + "/" + workflowID,
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify({ Data: pathName }),
                    success: function (data) {
                        if (data.Messages.ErrorMessages.length > 0) {
                            ECMNotification(data.Messages.ErrorMessages, 3, data.Stack);
                        }
                        else {
                            WorkflowPaths[WorkflowPaths.length] = { WorkflowId: workflowID, WorkflowPathName: pathName, WorkflowPathID: data.Data };
                            LoadQueues();
                        }
                    },
                    error: function (msg) {
                        alert(msg);
                    }
                });
            }
            else {
                //UpdateWorkflowPath/{sessionID}/{workflowID}/{workflowPathID}
                $.ajax({
                    url: "UpdateWorkflowPath/" + sessionID + "/" + workflowID + "/" + currentWFPath,
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify({ Data: pathName }),
                    success: function (data) {
                        if (data.Messages.ErrorMessages.length > 0) {
                            alert(data.Messages.ErrorMessages);
                        }
                        else {
                            for (var i = 0; i < WorkflowPaths.length; i = i + 1) {
                                if (WorkflowPaths[i].WorkflowPathID == currentWFPath) {
                                    WorkflowPaths[i].WorkflowPathName = pathName;
                                    break;
                                }
                            }
                            LoadQueues();
                        }
                    },
                    error: function (msg) {
                        alert(msg);
                    }
                });
            }
        }

        $('#divSettingsModal').hide();
        $('#divWorkflowPathEditor').fadeOut();
    }

    function TogglePathView(pathHeader, pathID) {
        var tbl = $(pathHeader).parent().parent().parent().attr('for');
        var icon = $(pathHeader).children()[0];
        if ($(icon).hasClass('fa-caret-right')) {
            $('.queueContainer[pathid=' + pathID + ']').slideDown();
            $(icon).removeClass('fa-caret-right');
            $(icon).addClass('fa-caret-down');
            $(pathHeader).parent().parent().parent().parent().removeClass('roundBottom');
            RemoveHiddenPath(pathID);
        }
        else {
            $('.queueContainer[pathid=' + pathID + ']').slideUp();
            $(icon).removeClass('fa-caret-down');
            $(icon).addClass('fa-caret-right');
            $(pathHeader).parent().parent().parent().parent().addClass('roundBottom');
            AddHiddenPath(pathID);
        }
        //ApplyLines();
    }

    function confirmPathDelete() {
        $('#divWorkflowPathEditor').hide();
        $('#divPathDeleteConfirm').show();
    }
    function pathDelete() {
        $('#divSettingsModal').hide();
        $('#divPathDeleteConfirm').hide();

        $.ajax({
            url: "RemoveWorkflowPath/" + sessionID + "/" + workflowID + "/" + currentWFPath,
            type: "DELETE",
            dataType: 'json',
            contentType: 'application/json',
            data: '',
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    alert(data.Messages.ErrorMessages);
                }
                else {
                    LoadWorkflowDesignerData();
                    LoadQueues();
                }
            },
            error: function (msg) {
                alert(msg);
            }
        });
    }
    function pathConfirmCancel() {
        $('#divPathDeleteConfirm').hide();
        $('#divWorkflowPathEditor').show();
    }

}


//Queue Drag and Drop Section
{
    function Queue_MouseDown(event) {
    }

    function Queue_MouseUp(event, item, queueID) {
        ShowQueueSettings(queueID);
    }
    function Queue_DragEnd(event, item, queueID) {
        //TODO: Add Queue Drag Functionality
    }
}

//Event Drag and Drop Section
{
    differenceYEvents = null;
    var OrderedEvents = null;
    var eventItemLeft = null;
    var eventItemWidth = null;
    var eventTimer = null;
    function Event_MouseDown(event, item) {
        OrderedEvents = [];
        differenceYEvents = event.pageY - $(item).offset().top;

        var orderedEvents = 0;
        var queueid = $(item).attr('queueid');
        eventItemLeft = $(item).offset().left;
        eventItemWidth = $(item).outerWidth();
        var events = $('.divEvent[queueid=' + queueid + ']');
        eventTimer = setTimeout(function () {
            events.parent().css('background-color', '#FFE4CC')
        }, 400);

        for (var i = 0; i < events.length; i = i + 1) {
            var evt = $(events[i]);
            var eventstub = {
                top: evt.offset().top,
                bottom: evt.offset().top + evt.outerHeight(),
                eventid: evt.attr('eventid')
            };
            OrderedEvents[OrderedEvents.length] = eventstub;
        }
        OrderedEvents.sort(SortByTop);
    }

    function SortByTop(a, b) {
        return ((a.top < b.top) ? -1 : ((a.top > b.top) ? 1 : 0));
    }

    function Event_MouseUp(event, item, queueID, eventID) {
        clearTimeout(eventTimer);
        ShowEventSettings(queueID, eventID);
    }

    function Event_MouseDrag(event, item) {

        var locY = event.pageY - differenceYEvents;
        var eventBefore = null;
        var eventAfter = null;

        for (var i = 0; i < OrderedEvents.length; i = i + 1) {
            var evt = OrderedEvents[i];

            if (evt.top > locY) {
                eventAfter = evt;
                if (i != 0) eventBefore = OrderedEvents[i - 1];
                break;
            }
        }
        if (eventAfter == null) eventBefore = OrderedEvents[OrderedEvents.length - 1];

        var markerTop = null;
        if (eventAfter == null) {
            markerTop = eventBefore.bottom + 3;
        }
        else if (eventBefore == null) {
            markerTop = eventAfter.top - 6;
        }
        else {
            markerTop = ((eventAfter.top - eventBefore.bottom) / 2) + eventBefore.bottom - 2;
        }

        var parentOffset = $('#divContentContainer').offset();
        var marker = $('#dropMarker');
        if (marker.length == 0) {
            marker = $('#divLineContainer').append('<div id="dropMarker" class="markerLine" style="width:' + eventItemWidth + 'px;"></div>');
        }
        marker.css('left', (eventItemLeft - parentOffset.left));
        marker.css('top', (markerTop - parentOffset.top));
    }

    function Event_DragEnd(event, item) {
        $('#dropMarker').remove();
        //TODO: Add Event Drag Functionality
        var queueid = $(item).attr('queueid');
        var eventid = $(item).attr('eventid');
        //var events = $('.divEvent[queueid=' + queueid + ']');
        var locY = event.pageY - differenceYEvents;
        var eventBefore = null;
        var eventAfter = null;

        for (var i = 0; i < OrderedEvents.length; i = i + 1) {
            var evt = OrderedEvents[i];

            if (evt.top > locY) {
                eventAfter = evt;
                if (i != 0) eventBefore = OrderedEvents[i - 1];
                break;
            }
        }
        if (eventAfter == null) eventBefore = OrderedEvents[OrderedEvents.length - 1];

        var queue = GetQueueByID(queueid);
        var indexOld = null
        var indexNew = null;

        if (eventBefore == null) {
            for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
                if (queue.Settings.QueueEvents[iEvent].EventID == eventid) {
                    indexOld = iEvent;
                }
            }
            indexNew = 0;
        }
        else if (eventAfter == null) {
            for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
                if (queue.Settings.QueueEvents[iEvent].EventID == eventid) {
                    indexOld = iEvent;
                }
            }
            indexNew = queue.Settings.QueueEvents.length;
        }
        else {
            for (var iEvent = 0; iEvent < queue.Settings.QueueEvents.length; iEvent = iEvent + 1) {
                if (queue.Settings.QueueEvents[iEvent].EventID == eventid) {
                    indexOld = iEvent;
                }
                if (queue.Settings.QueueEvents[iEvent].EventID == eventAfter.eventid) {
                    indexNew = iEvent;
                }
            }
        }

        var origQueue = queue.Settings.QueueEvents.splice(indexOld, 1);
        if (indexNew > indexOld) {
            queue.Settings.QueueEvents.splice(indexNew - 1, 0, origQueue[0]);
        }
        else {
            queue.Settings.QueueEvents.splice(indexNew, 0, origQueue[0]);
        }

        SelectedQueueID = queueid;
        SaveQueue();
    }
}


//Viewer URL Presets
{

    function SetupViewerURLPresets() {
        $("#ddlViewerPresets").empty();
        $('#lstPresets').empty();
        $("#ddlViewerPresets").append('<option value="-1">Select Preset...</option>')
        for (var i = 0; i < ViewerPresets.length; i = i + 1) {
            var preset = ViewerPresets[i];
            var optn = $('<option></option>')
            optn.attr('value', preset.ID);
            optn.text(preset.Name);
            $("#ddlViewerPresets").append(optn);

            var listItem = $('<div></div>');
            listItem.attr('value', preset.ID);
            listItem.text(preset.Name);
            $('#lstPresets').append(listItem);
        }
        $("#ddlViewerPresets").trigger("chosen:updated");
    }

    function ViewerURLPresetDropdownChanged(dropdown, evt) {
        var clickedID = $("#ddlViewerPresets").chosen().val();
        if (clickedID != '-1') {
            for (var i = 0; i < ViewerPresets.length; i = i + 1) {
                if (ViewerPresets[i].ID == clickedID) {
                    $('#txtViewerURL').val(ViewerPresets[i].ViewerURL);
                    $(dropdown).val('-1');
                    break;
                }
            }
        }
    }

    function ShowPresetWindow() {
        $('#divQueueSettings').fadeOut();
        $('#windowPresets').fadeIn();
        
    }

    function ClosePresetWindow() {
        $('#windowPresets').fadeOut();
        $('#divQueueSettings').fadeIn();
    }

    function AddPreset() {
        var newid = -1;
        for (var i = 0; i < ViewerPresets.length; i = i + 1) {
            if (ViewerPresets[i].ID > newid) {
                newid = ViewerPresets[i].ID;
            }
        }
        newid = newid + 1;
        $('#txtPresetID').val(newid);
        $('#txtPresetName').val('');
        $('#txtPresetURL').val('');
        $('#txtPresetName').removeAttr('disabled');
        $('#txtPresetURL').removeAttr('disabled');
        $('#btnSavePresets').removeAttr('disabled');
        $('#btnURLBuilder').removeAttr('disabled');
        
        
    }

    function EditPreset(targetDiv) {
        var clickedID = $(targetDiv).attr('value');
        for (var i = 0; i < ViewerPresets.length; i = i + 1) {
            if (ViewerPresets[i].ID == clickedID) {
                $('#txtPresetID').val(ViewerPresets[i].ID);
                $('#txtPresetName').val(ViewerPresets[i].Name);
                $('#txtPresetURL').val(ViewerPresets[i].ViewerURL);
                $('#txtPresetName').removeAttr('disabled');
                $('#txtPresetURL').removeAttr('disabled');
                $('#btnSavePresets').removeAttr('disabled');
                $('#btnURLBuilder').removeAttr('disabled');
                break;
            }
        }
    }

    function RemovePreset() {
        var clickedID = $('#txtPresetID').val();
        if (clickedID != '') {
            for (var i = 0; i < ViewerPresets.length; i = i + 1) {
                if (ViewerPresets[i].ID == clickedID) {
                    ViewerPresets.splice(i, 1);
                    $('#txtPresetID').val('');
                    $('#txtPresetName').val('');
                    $('#txtPresetURL').val('');
                    $('#txtPresetName').attr('disabled', 'disabled');
                    $('#txtPresetURL').attr('disabled', 'disabled');
                    $('#btnSavePresets').attr('disabled', 'disabled');
                    $('#btnURLBuilder').attr('disabled', 'disabled');
                    UpdateWorkflowPresets();
                    break;
                }
            }
        }
    }

    function SavePreset() {
        var preset = null;
        var id = $('#txtPresetID').val();
        if (id != '') {
            for (var i = 0; i < ViewerPresets.length; i = i + 1) {
                if (ViewerPresets[i].ID == id) {
                    preset = ViewerPresets[i];
                }
            }
        }

        if (preset == null) {
            var preset = {
                ID: id,
                Name: $('#txtPresetName').val(),
                ViewerURL: $('#txtPresetURL').val()
            }
            ViewerPresets[ViewerPresets.length] = preset;
        }
        else {
            preset.Name = $('#txtPresetName').val();
            preset.ViewerURL = $('#txtPresetURL').val();
        }
        $('#txtPresetID').val('');
        $('#txtPresetName').val('');
        $('#txtPresetURL').val('');
        $('#txtPresetName').attr('disabled', 'disabled');
        $('#txtPresetURL').attr('disabled', 'disabled');
        $('#btnSavePresets').attr('disabled', 'disabled');
        $('#btnURLBuilder').attr('disabled', 'disabled');
        UpdateWorkflowPresets();
    }

    function UpdateWorkflowPresets() {
        //UpdateWorkflowPresetViewerURLs/{sessionID}/{workflowID}
        $.ajax({
            url: "UpdateWorkflowPresetViewerURLs/" + sessionID + "/" + workflowID,
            type: "POST",
            dataType: 'json',
            async: false,
            contentType: 'application/json',
            data: JSON.stringify(ViewerPresets),
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    alert(data.Messages.ErrorMessages);
                }
                else {
                    SetupViewerURLPresets();
                }
            },
            error: function (msg) {
                alert(msg);
            }
        });
    }

    function OpenURLBuilder() {
        
        $('#lstPresets').fadeOut();
        $('#divVieserPresetListButtons').fadeOut();
        $('#divViewerPresetURLDetails').fadeOut();
        $('#divViewerStringBuilder').fadeIn();
    }

    function SetPresetURL(sNewURL) {
        $('#txtPresetURL').val(sNewURL);
        CloseURLBuilder();
    }

    function CloseURLBuilder() {
        $('#divViewerStringBuilder').fadeOut();
        $('#lstPresets').fadeIn();
        $('#divVieserPresetListButtons').fadeIn();
        $('#divViewerPresetURLDetails').fadeIn();
    }

}

