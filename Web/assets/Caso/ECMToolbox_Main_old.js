﻿var _toolbox = new toolbox;
var session = null;
var sessionID = null;
var selectedWorkflowID = null;
var selectedQueueID = null;
var IsWorkflowManager = null;
var QueuePermissions = [];
var QueueButtons = null;
var WorkflowQueueColumns = null;
var ClientSettings = null;
var GridIsVisible = false;

var __collapsedWorkflows = null;
var CollapsedWorkflows = {
    get: function() {
        if (__collapsedWorkflows == null) {
            if (typeof (Storage) !== "undefined") {
                var hf = localStorage.getItem("ECMTMainCollapsedWorkflow_"+_toolbox.session.userID);
                if (hf == null || hf == '') { __collapsedWorkflows = []; }
                else {
                    __collapsedWorkflows = hf.split(',');
                }
            } 
            else {
                __collapsedWorkflows = [];
            }
            
        }
        return __collapsedWorkflows;
    },
    add: function(sWorkflowID) {
        if (__collapsedWorkflows == null) CollapsedWorkflows.get();
        if (__collapsedWorkflows.indexOf(sWorkflowID) == -1) {
            __collapsedWorkflows[__collapsedWorkflows.length] = sWorkflowID;
            if (typeof (Storage) !== "undefined") {
                var scollitems = __collapsedWorkflows.join(',');
                localStorage.setItem("ECMTMainCollapsedWorkflow_" + _toolbox.session.userID, scollitems);
            }
        }
    },
    remove: function(sWorkflowID) {
        if (__collapsedWorkflows == null) CollapsedWorkflows.get();
        var indx = __collapsedWorkflows.indexOf(sWorkflowID)

        if (indx != -1) {
            __collapsedWorkflows.splice(indx, 1);
            if (typeof (Storage) !== "undefined") {
                var scollitems = __collapsedWorkflows.join(',');
                localStorage.setItem("ECMTMainCollapsedWorkflow_" + _toolbox.session.userID, scollitems);
            }
        }
    }
}


$(window).resize(function () { if (grid != null) { grid.data("kendoGrid").resize(); } });

$(document).ready(function () {

    sessionID = params["sessionid"];
    if (_toolbox.engine.validSession(_toolbox, sessionID)) {
        //-- set name to fullname at top of screen if it exists, else use username
        var un = (_toolbox.session.fullname === undefined) || _toolbox.session.fullname == null || _toolbox.session.fullName == '' ? _toolbox.session.fullName : _toolbox.session.username;
        $('#lblWelcomeUser').text(un);

        if (_toolbox.session.userType == "ECMT") $('#menuPasswordChange').show();
        if (_toolbox.session.isImpersonationEnabled) $('#menuImpersonate').show();
        ClientSettings = _toolbox.engine.getClientSettings(_toolbox);

        //-- check user setting to auto collapse left hand menu
        ECMTRegisterListBoxes();

        SetupPageContainers();

        HideTabsFromPermissions();
                
        $(window).resize(function () {
            ResizeAccordion();
        });

        LoadMenuItems();

        if (typeof (Storage) !== "undefined") {
            var lastWFSelected = parseInt(localStorage.getItem("lastWFSelected"));
            var lastQueueSelected = parseInt(localStorage.getItem("lastQueueSelected"));
            if (!(lastQueueSelected === undefined || lastQueueSelected == null || isNaN(lastQueueSelected))) {
                if (!(WorkflowQueueColumns[lastWFSelected].Queues[lastQueueSelected] === undefined || WorkflowQueueColumns[lastWFSelected].Queues[lastQueueSelected] == null)) {
                    LoadQueueItems(parseInt(localStorage.getItem("lastWFSelected")), parseInt(lastQueueSelected));
                    setBreadCrum(localStorage.getItem("lastBreadCrum"));
                }
                else {
                    SetLargeMenu();
                    SetIframeContent('WelcomePage.html');
                    ShowIframe();
                }
            }
            else {
                SetLargeMenu();
                SetIframeContent('WelcomePage.html');
                ShowIframe();
            }
        }

        //ShowGrid();
        //ShowIframe();
        //SetIframeContent('WelcomePage.html');

        ResizeAccordion();

        try{
            if (ClientSettings.CollapseLeftSide) {
                SetSmallMenu();
            }
        }
        catch(e){}

        //-- Popup drag function not working
        //$("#divPopup").draggable({
        //    handle: "#draggingarea"
        //});

        $("#ddlQueues").chosen({ width: '250px' });
        $("#ddlUsers").chosen({ width: '250px' });
        $("#ddlQueues").chosen().change(SelectedQueueChanged);

        KeepAlive();
    }
});

function SetupPageContainers() {
    var image_url = $('.page-header-logo').css('background-image');
    image_url = image_url.match(/^url\("?(.+?)"?\)$/)[1];
    $('.page-header-logo').append('<img></img>');
    $('.page-header-logo>img').attr('src', image_url);
    $('.page-header-logo').css('background-image', 'none');
    var headerHeight = $('.page-header').outerHeight();
    $('.wfSidebar-normal').css('top', headerHeight - 1);
    $('.wfSidebar-small').css('top', headerHeight - 1);
    $('.wfContent').css('top', headerHeight + 5);
}

function HideTabsFromPermissions() {
    $('#divDashboardTop').hide();
    //$('#divWorkflowTop').hide();
    
    if (_toolbox.session.isSystemAdmin || _toolbox.session.isUserManager || _toolbox.session.isWorkflowManager) {
        $('#divAdministrationTop').show();
        $('#link_Admin_Links').show();
    }
    else {
        $('#link_Admin_Links').hide();
    }

    if (_toolbox.session.isSystemAdmin) {
        $('#link_Admin_Settings').show();
        $('#link_Admin_Sessions').show();
        
    }
    else {
        $('#link_Admin_Settings').hide();
        $('#link_Admin_Sessions').hide();
    }
    if (_toolbox.session.isUserManager || _toolbox.session.isSystemAdmin) {
        $('#link_Admin_Users').show();
        $('#link_Admin_Groups').show();
    }
    else {
        $('#link_Admin_Users').hide();
        $('#link_Admin_Groups').hide();
    }
    if (_toolbox.session.isWorkflowManager || _toolbox.session.isSystemAdmin) {

        $('#link_Admin_Workflows').show();
    }
    else {

        $('#link_Admin_Workflows').hide();
    }
}

function SetSmallMenu() {
    var smallFrameSize = $('.wfSidebar-small').outerWidth();
    $('.wfContent').css('left', smallFrameSize + 5);
    $('.wfSidebar-normal').hide();
    $('.wfSidebar-small').show();
}

function SetLargeMenu() {
    var largeFrameSize = $('.wfSidebar-normal').outerWidth();
    $('.wfContent').css('left', largeFrameSize + 5);
    $('.wfSidebar-small').hide();
    $('.wfSidebar-normal').show();
}

function ShowGrid() {
    $('#divIframe').hide();
    $('#divGridArea').show();
    GridIsVisible = true;
}

function ShowIframe() {
    $('#divGridArea').hide();
    $('#divIframe').show();
    GridIsVisible = false;
}

function SetIframeContent(url) {
    url = url + (url.indexOf('sessionID=') != -1 ? '' : (url.indexOf('?') != -1 ? '&sessionID=' + sessionID : '?sessionID=' + sessionID));
    $('#wfIframe').attr('src', null);
    if (url != null && url != '') {
        //url = url.replace("trumbulldev:8081", "localhost:8083");
        $('#wfIframe').attr('src', url);
    }
}


function Logout() {
    _toolbox.engine.logout(_toolbox);
    window.location.href ='login.html?loggedout=true';
}

function ShowPopup(url) {    
    $('#txtPopupURL').val(url);
    $('#iframePopup').attr('src', url);
    $('#divPopup').fadeIn();
}

function HidePopup() {
    $('#iframePopup').attr('src', null);
    $('#divPopup').fadeOut();
}


function LoadMenuItems() {
    //  Api/GetMenuItems/{sessionID}
    ShowWaitSpinner('Loading...', 'wfSidebar-normal');
    $.ajax({
        url: "Api/GetMenuItems/" + sessionID,
        type: "Get",
        async: false,
        success: function (data) {
            try {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    ECMNotification(msg, 3, stack);
                }
                else {
                    SetupMenuItems(data.Data.WFItems);
                    SetupLinks(data.Data.Links);
                    ResizeAccordion();
                }
                HideWaitSpinner('wfSidebar-normal');
            }
            catch (e) {
                HideWaitSpinner('wfSidebar-normal');
                ECMNotification(e, 3);                
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner('wfSidebar-normal');
        }
    });
}

var template_wf = $('#template_Menu_Workflow').html();
var template_queue_container = $('#template_Menu_Queue_Container').html();
var template_inbox = $($('#template_Menu_Inbox').html()).find('tbody').html();
var template_groupbox = $($('#template_Menu_Groupbox').html()).find('tbody').html();
var template_queue = $($('#template_Menu_Queue').html()).find('tbody').html();
function SetupMenuItems(wfItems) {
    $('#divWFandQMenu').empty()
    WorkflowQueueColumns = [];
    var collapsed = CollapsedWorkflows.get();
    for (var iwf = 0; iwf < wfItems.length; iwf = iwf + 1) {
        var wf = wfItems[iwf];
        var isHidden = collapsed.indexOf(wf.WorkflowID.toString()) != -1;
        WorkflowQueueColumns[wf.WorkflowID] = { Inbox: [], GroupBox: [], Queues: [] };
        var wfitem = $(template_wf);
        wfitem.attr('workflowid', wf.WorkflowID);
        wfitem.attr('onclick', 'ToggleQueueItems(this, ' + wf.WorkflowID + ')');
        if (isHidden) {
            var icon = wfitem.find('i');
            icon.addClass('fa-caret-right');
            wfitem.attr('queueshidden', 'true');
        }
        else {
            var icon = wfitem.find('i');
            icon.addClass('fa-caret-down');
            wfitem.attr('queueshidden', 'false');
        }
        wfitem.append(wf.WorkflowName);
        $('#divWFandQMenu').append(wfitem);

        var qcont = $(template_queue_container);
        if (isHidden) {
            qcont.css('display', 'none');
        }
        qcont.attr('workflowid', wf.WorkflowID);
        $('#divWFandQMenu').append(qcont);

        var itemCountText = '';
        for (var iq = 0; iq < wf.Queues.length; iq = iq + 1) {           
            var q = wf.Queues[iq];
            if (q.QueueID == -2) {
                WorkflowQueueColumns[wf.WorkflowID].GroupBox = q.GridDefinition;
            }
            else if (q.QueueID == -1) {
                WorkflowQueueColumns[wf.WorkflowID].Inbox = q.GridDefinition;
            }
            else {
                WorkflowQueueColumns[wf.WorkflowID].Queues[q.QueueID] = q.GridDefinition;
            }

            var qItem = null;
            if (q.QueueID == -1) {
                qItem = $(template_inbox);
                if (q.TotalItems != 0) {
                    try { itemCountText = getCustomItemCount(q); }
                    catch (e) { itemCountText = q.UserOwnedItems; }
                }
            }
            else if (q.QueueID == -2) {
                qItem = $(template_groupbox);
                try { itemCountText = getCustomItemCount(q); }
                catch (e) {
                    if (!(q.UnownedItems == 0 && q.TotalItems == 0)) {
                        //   the char: '\xa0' is a non-breaking space
                        var itemCountText = q.UnownedItems + "\xa0/\xa0" + q.TotalItems;
                        if (itemCountText.length > 8) itemCountText = itemCountText.replace('\xa0/\xa0', ' ');
                    }
                }
            }
            else {
                qItem = $(template_queue);
                try { itemCountText = getCustomItemCount(q); }
                catch (e) {
                    if (!(q.UnownedItems == 0 && q.TotalItems == 0)) {
                        //   the char: '\xa0' is a non-breaking space
                        itemCountText = q.UnownedItems + "\xa0/\xa0" + q.TotalItems;
                        if (itemCountText.length > 8) itemCountText = itemCountText.replace('\xa0/\xa0', ' ');
                    }
                    else {
                        itemCountText = "";
                    }
                }
            }
            if (itemCountText == '') $(qItem.children()[2]).html('');
            else $($(qItem.children()[2]).children()[0]).text(itemCountText);

            qItem.attr('workflowid', wf.WorkflowID);
            qItem.attr('queueid', q.QueueID);
            qItem.attr('onclick', 'LoadQueueItems(' + wf.WorkflowID + ', ' + q.QueueID + '); setBreadCrum("Viewing : ' + q.QueueName + '");');

            $(qItem.children()[1]).text(q.QueueName);
            $(qcont.children()[0]).find('tbody').append(qItem);
        }        
    }
    //-- set selected menu css
    $('#divWFandQMenu>div>table>tbody>tr>td').removeClass('selected');
    $('#divWFandQMenu>div>table>tbody>tr[workflowid=' + selectedWorkflowID + '][queueid=' + selectedQueueID + ']').addClass('selected');
}

var areaMenuItemDisplayed = 'divWFandQMenu';
function ToggleMainMenuAreas(mainMenuItem) {
    var areaDiv = $(mainMenuItem).attr('for');

    if (areaDiv != areaMenuItemDisplayed) {
        $('#' + areaMenuItemDisplayed).slideUp();
        $('#' + areaDiv).slideDown();
        areaMenuItemDisplayed = areaDiv;
        ResizeAccordion();
    }
}

function SetupLinks(links) {
    $('#divLinks').empty();
    if (!(links === undefined) && links != null) {
        for (var i = 0; i < links.length; i = i + 1) {
            var link = links[i];
            var lnk = $('<div class="ecmlink" onclick="LinkClicked(this);" linkid="' + link.LinkID + '" linktype="' + link.LinkType + '">' + HtmlEncode(link.LinkName) + '</div>');
            lnk.attr('url', link.LinkURL);
            $('#divLinks').append(lnk);
            
        }
        if (links.length > 0) {
            $('#divLinksTop').show();
        }
    }
}

function LinkClicked(linkDiv) {
    var ld = $(linkDiv)
    var type = parseInt(ld.attr('linktype'));
    var name = ld.text();
    var url = ld.attr('url');
    if (type == 0) {
        url = url + (url.indexOf('sessionID=') != -1 ? '' : (url.indexOf('?') != -1 ? '&sessionID=' + sessionID : '?sessionID=' + sessionID));
        var newwindow = window.open(url, name, "width=1000,height=700,resizable=yes,toolbar=yes,menubar=no,location=yes,status=no");
        newwindow.focus();
        //Without specifications it creates a new tab, not popup window
        //window.open(url, name);
    }
    else if (type == 1) {
        SetLargeMenu();
        SetIframeContent(url);
        ShowIframe();
    }
    else if (type == 2) {
        SetSmallMenu();
        SetIframeContent(url);
        ShowIframe();
    }
}

function RefreshMenuItems() {
    selectedItems = [];
    LoadMenuItems();
    if (GridIsVisible && selectedWorkflowID != null && selectedQueueID != null && !(grid == null)) {
        LoadGrid();
    }
    DetermineActiveFunctions();
}

function DestroySlimScrolls() {
    var ss = $('.sidebar-secondary-level');
    for (var i = 0; i < ss.length; i = i + 1) {
        $(ss[i]).slimScroll({ destroy: true });
    }
}
function ResizeAccordion() {
    var primaryDivs = $('.sidebar-top-level')
    var accordionHeight = $('#wfNormalMenu').innerHeight();
    iHeight = accordionHeight;
    for (var i = 0; i < primaryDivs.length; i = i + 1) {
        if ($(primaryDivs[i]).is(':visible')) {
            iHeight = iHeight - $(primaryDivs[i]).outerHeight();
        }
    }
    $('.sidebar-secondary-level').css("height", iHeight);
    DestroySlimScrolls();
    $('#' + areaMenuItemDisplayed).slimScroll({ height: iHeight, disableFadeOut: true, autoScrolling: true });
}

function ToggleQueueItems(wfitem, wfid) {
    var queues = $('.menu-queue-container[workflowid=' + wfid + ']');
    var item = $(wfitem)
    if (item.attr('queueshidden') == 'false') {
        item.attr('queueshidden', 'true');
        queues.slideUp();
        $(item[0].firstChild).removeClass('fa-caret-down');
        $(item[0].firstChild).addClass('fa-caret-right');
        CollapsedWorkflows.add(wfid.toString());
    }
    else {
        item.attr('queueshidden', 'false');
        queues.slideDown();
        $(item[0].firstChild).removeClass('fa-caret-right');
        $(item[0].firstChild).addClass('fa-caret-down');
        CollapsedWorkflows.remove(wfid.toString());
    }
}




function LoadQueueItems(workflowID, queueID) {
    selectedWorkflowID = workflowID;
    selectedQueueID = queueID;
    selectedItems = [];
    LoadMenuItems();
    ShowGrid();
    LoadGrid();
    DetermineActiveFunctions();
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem("lastWFSelected", workflowID);
        localStorage.setItem("lastQueueSelected", queueID);
    }
}

function setBreadCrum(breadCrumHtml) {
    $('#divPageHeaderContent').html(breadCrumHtml);
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem("lastBreadCrum", breadCrumHtml);
    }
}


//function rowOpened(e) {
//    try {
//        e.preventDefault();
//        var targetID = e.currentTarget.getAttribute("data-uid");
//        var item = null;
//        if (targetID != null) {
//            var data = grid.data("kendoGrid").dataSource.view();
//            for (var i = 0; i < data.length; i = i + 1) {
//                if (data[i].uid == targetID) {
//                    item = data[i];
//                    break;
//                }
//            }

//            var currentQueueID = ActiveQueueID();
//            if (currentQueueID == "inbox") {
//                selectedGridItems = [];
//                selectedGridItems[0] = item.I;
//                Open();
//            }
//            else if (currentQueueID == "groupbox") {
//                var perm = WorkflowInfo.Perms[item.Q];
//                if (((item.O == null || item.O == "") && !(perm === undefined || perm == null) && perm.CanTakeOwnership) || item.O == StateVariables.UserID()) {
//                    selectedGridItems = [];
//                    selectedGridItems[0] = item.I;
//                    Open();
//                }
//                else {
//                    if (!(data.O == null || data.O == "")) throw "This WorkItem is already owned by another user.";
//                    else throw "You do not have permission to Open this WorkItem.";
//                }
//            }
//            else {
//                var perm = WorkflowInfo.Perms[currentQueueID];
//                if (((data.O == null || data.O == "") && perm.CanTakeOwnership) || data.O == StateVariables.UserID()) {
//                    selectedGridItems = [];
//                    selectedGridItems[0] = item.I;
//                    Open();
//                }
//                else {
//                    if (!(data.O == null || data.O == "")) throw "This WorkItem is already owned by another user.";
//                    else throw "You do not have permission to Open this WorkItem.";
//                }
//            }
//        }
//    }
//    catch (ex) {
//        ShowErrorMessage(ex, null, "rowOpened");
//    }
//}

var bOpen = $('#btnOpenWorkItem');
var bPick = $('#btnTakeOwnership');
var bAssign = $('#btnAssign');
var bUnassign = $('#btnUnassign');
var mOpen = $('#menuOpen');
var mPick = $('#menuPick');
var mAssign = $('#menuAssign');
var mUnassign = $('#menuUnAssign');
var mAdmin = $('#menuAdmin');
var mNone = $('#menuNoOptions');


var SelectedInstanceIDs = null;
var SelectedInstanceQueue = null;
var SelectedWorkItems = null;
function DetermineActiveFunctions(e) {
    $('#divGridButtons>.eventbutton').remove();
    SelectedInstanceIDs = [];
    SelectedWorkItems = GetSelectedItemData();
    SelectedInstanceQueue = null;
    var allSameQueue = true;
    var allOwnedByUser = true;
    var allUnownedOrOwnedByUser = true;
    var allOwned = true;
    var allUnowned = true;
    var queuesSelected = true;
    var showEndWorkflow = false;
    var sessionUserID = _toolbox.session.userID;
    var selectedQueues = [];
    if (SelectedWorkItems.length > 0) {


        for (var i = 0; i < SelectedWorkItems.length; i++) {
            if (IsWorkflowManager) {
                showEndWorkflow = true;
            }
            var dataRecord = SelectedWorkItems[i];
            SelectedInstanceIDs[i] = dataRecord.IN;
            if (SelectedInstanceQueue == null) SelectedInstanceQueue = dataRecord.QI;
            if (SelectedInstanceQueue != dataRecord.QI) allSameQueue = false;
            if (selectedQueues[dataRecord.QI] === undefined) selectedQueues[dataRecord.QI] = $.grep(QueuePermissions, function (item) { return item.QueueID == dataRecord.QI })[0];
            var RowOwner = dataRecord.UI;
            if (RowOwner === undefined || RowOwner == null) allOwned = false;
            if (!(RowOwner === undefined) && RowOwner != null) allUnowned = false;
            if (RowOwner != sessionUserID) allOwnedByUser = false;
            if (!(RowOwner === undefined || RowOwner == null) && RowOwner != sessionUserID) allUnownedOrOwnedByUser = false;
        }

        var canAssign = allUnowned;
        var canUnassign = allOwned;
        var canTakeOwnership = allUnowned;
        var canOpen = allUnownedOrOwnedByUser;

        for (var i = 0; i < selectedQueues.length; i = i + 1) {
            if (!(selectedQueues[i] === undefined)) {
                var permissions = selectedQueues[i];
                if (!permissions.CanAssign) canAssign = false;
                if (!permissions.CanUnassign) canUnassign = false;
                if (!permissions.CanTakeOwnership) { canTakeOwnership = false; canOpen = false; }
            }
        }
        if (allOwnedByUser) canOpen = true;


        if (canOpen) {
            bOpen.show();
            mOpen.show();
        }
        else {
            bOpen.hide();
            mOpen.hide();
        }
        if (canTakeOwnership) {
            bPick.show();
            mPick.show();
        }
        else {
            bPick.hide();
            mPick.hide();
        }
        if (canAssign) {
            bAssign.show();
            mAssign.show();
        }
        else {
            bAssign.hide();
            mAssign.hide();
        }
        if (canUnassign) {
            bUnassign.show();
            mUnassign.show();
        }
        else {
            bUnassign.hide();
            mUnassign.hide();
        }
        
        if (showEndWorkflow) {
            mAdmin.show();
        }
        else {
            mAdmin.hide();
        }

        if (canOpen || canAssign || canTakeOwnership || canUnassign) {
            mNone.hide();
        }
        else {
            mNone.show();
        }


        if (allSameQueue && canOpen) {
            $('.eventbutton').remove();
            var gridButtonsDiv = $('#divGridButtons')
            if (!(QueueButtons === undefined || QueueButtons == null)) {
                for (var i = 0; i < QueueButtons.length; i = i + 1) {
                    var btn = QueueButtons[i];
                    if (btn.QueueID == SelectedInstanceQueue) {
                        var sButton = '<button class="btn eventbutton" style="color:' + btn.ButtonTextColor + '; background-color:' + btn.ButtonBackgroundColor + '; margin:2px 2px;" onclick="Button_Clicked(this);"' + (btn.ButtonTooltip != "" ? ' data-toggle="tooltip" data-placement="bottom" title="' + btn.ButtonTooltip + '"' : '') + '>' + HtmlEncode(btn.ButtonName) + '</button>';
                        var btnObject = gridButtonsDiv.append(sButton);
                        //if (btn.ButtonTooltip != "") $(btnObject).tooltip();
                    }
                }
            }
        }
        $('[data-toggle="tooltip"]').tooltip();

        //-- uses ecmtoolbox_main_customcode.js
        try { addPreviewButton(); }
        catch(e) { }
    }
    else {
        //No items currently selected
        bOpen.hide();
        bPick.hide();
        bAssign.hide();
    }
    //-- always add export to excel button
    $('#divGridButtons').append('<button class="btn eventbutton pull-right" style="color:white; background-color:green; margin:2px 2px;" onclick="exportToExcel();"><i class="fa fa-save"></i>Export to Excel</button>');
}


//-- user grid / export to excel
function exportToExcel() {

    //-- export only selected grid items

    //-- build grid with selected items to export
    var gridExportToExcel = $("#gridExportToExcel").kendoGrid({
        excel: {
            allPages: true
        },
        columns: Columns,
        height: '100%'
    });

    try {
        if (selectedItems.length > 0) {            
            gridExportToExcel.data("kendoGrid").dataSource.data(selectedItems);
            gridExportToExcel.data("kendoGrid").saveAsExcel();
        }
        else {            
            gridExportToExcel.data("kendoGrid").dataSource.data(GridData);
            gridExportToExcel.data("kendoGrid").saveAsExcel();
        }
    }
    catch (ex) {
    }

    //-- default behavior / exports all rows
    //var grid = $("#workItemsGrid").data("kendoGrid");
    //grid.saveAsExcel();

    return false;
}

var SelectedButton = null;
function Button_Clicked(btn) {
    var btnName = $(btn).text();
    
    var iButtonCount = QueueButtons.length;
    var button = null;
    for (var i = 0; i < iButtonCount; i = i + 1) {
        button = QueueButtons[i];
        if (btnName == button.ButtonName) break;
    }
    if (button.SelectableQueues === undefined || button.SelectableQueues == null || button.SelectableQueues.length == 0) {
        //-- non user select type buttons
        SubmitButtonFunction(btnName, null, null);
    }
    else {
        var oddlQueues = $('#ddlQueues');        
        var oddlQueuesChosen = $('#ddlQueues_chosen');
        //-- user select type button
        $('#divGridButtons').hide();
        SelectedButton = button;
        oddlQueues.empty();
        for (var i = 0; i < button.SelectableQueues.length; i = i + 1) {
            var q = button.SelectableQueues[i];
            var qoption = $('<option></option>');
            qoption.attr('value', q.QueueID);
            qoption.text(q.QueueName);
            if (i == 0) qoption.attr('selected', 'selected');
            oddlQueues.append(qoption);
        }

        if (button.SelectableQueues.length != 1) {
            oddlQueuesChosen.show();
        }
        else {
            oddlQueuesChosen.hide();
        }
        $("#ddlQueues").trigger("chosen:updated");
        SelectedQueueChanged();
        $('#divGridButtons').hide();
        $('#divQueueAndUserSelections').show();
    } 
}

//-- non user select type buttons
function SubmitButtonFunction(submitName, queueSelection, userSelection) {
    BackToButtons();
    //Api/SubmitMultiple/{sessionID}/{workflowID}
    var data = [];
    for (var i = 0; i < SelectedInstanceIDs.length; i = i + 1) {                
        data[i] = {
            WorkflowID: selectedWorkflowID,
            WorkInstanceID: SelectedInstanceIDs[i],
            ButtonText: submitName,
            FieldUpdates: [],
            SelectedSendToQueueID: queueSelection,
            SelectedSendToUserID: userSelection
        }
    }
    
    //var currentSelection = $("#workItemsGrid").data("kendoGrid").select();
    //var selectedRows = [];
    //currentSelection.each(function () {
    //    var currentRowIndex = $(this).closest("tr").index();
    //    if (selectedRows.indexOf(currentRowIndex) == -1) {
    //        selectedRows.push(currentRowIndex);
    //    }
    //});

    ShowWaitSpinner('Please Wait...');
    $.ajax({
        url: "Api/SubmitMultiple/" + sessionID + "/" + selectedWorkflowID,
        type: "Post",
        data: JSON.stringify(data),
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                HideWaitSpinner();                
                RefreshMenuItems();

                if (data.Data.JavascriptFunctions.length > 0) {
                    for (var i = 0; i < data.Data.JavascriptFunctions.length; i = i + 1) {
                        var jsf = data.Data.JavascriptFunctions[i];
                        if (jsf.RunLocation == 0) {
                            try {
                                eval(jsf.Script);
                            }
                            catch (e) { }
                        }
                    }
                }
                if (data.Data.Popups.length > 0) {
                    for (var i = 0; i < data.Data.Popups.length; i = i + 1) {
                        var pop = data.Data.Popups[i];
                        if (pop.Popuptype == 2) { //New Window
                            try {
                                var url = pop.URL + (pop.URL.indexOf('?') != -1 ? pop.URL + '&sessionID=' + sessionID : '?sessionID=' + sessionID);
                                window.open(url, "_blank");
                            }
                            catch (e) { }
                        }
                        else if (pop.Popuptype == 1) { //Workflow Main Window
                            try {
                                var url = pop.URL + (pop.URL.indexOf('?') != -1 ? pop.URL + '&sessionID=' + sessionID : '?sessionID=' + sessionID);
                                ShowPopup(url);
                            }
                            catch (e) { }
                        }
                    }
                }
                //-- reset scroll to position
                //$("#workItemsGrid").data("kendoGrid").select("tr:eq(" + selectedRows[0] + ")");
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
    
}

//-- user select type buttons
function SelectedQueueChanged() {
    $('#ddlUsers').empty();
    var selectedOptionID = $('#ddlQueues').val();
    var usersList = null;
    for (var i = 0; i < SelectedButton.SelectableQueues.length; i = i + 1) {
        if (SelectedButton.SelectableQueues[i].QueueID.toString() == selectedOptionID) {
            usersList = SelectedButton.SelectableQueues[i].QueueMembers;
        }
    }
    if (usersList === undefined || usersList == null || usersList.length == 0) {
        $('#ddlUsers_chosen').hide();
    }
    else {
        for (var i = 0; i < usersList.length; i = i + 1) {
            var u = usersList[i];
            var uoption = $('<option></option>');
            uoption.attr('value', u.UserID);
            uoption.text(u.UserName + (u.FullName == null || u.FullName == "" ? "" : " (" + HtmlEncode(u.FullName) + ")"));
            if (i == 0) uoption.attr('selected', 'selected');
            $('#ddlUsers').append(uoption);
        }
        $('#ddlUsers_chosen').show();
    }

    $("#ddlUsers").trigger("chosen:updated");
    
}

function BackToButtons() {
    $('#divGridButtons').show();
    $('#divQueueAndUserSelections').hide();
}

function QueueAndUserChosen() {
    var queue = $("#ddlQueues").chosen().val();
    var user = null;
    if ($('#ddlUsers_chosen').is(':visible')) {
        user = $("#ddlUsers").chosen().val();
    }
    SubmitButtonFunction(SelectedButton.ButtonName, queue, user);
}



var ContextMenuDiv = $("#contextMenuDiv");
function ShowContextMenu(event) {
    var scrollTop = $(window).scrollTop();
    var scrollLeft = $(window).scrollLeft();
    var x = parseInt(event.pageX) + 5 + scrollTop;
    var y = parseInt(event.pageY) + 5 + scrollLeft;
    ContextMenuDiv.css("position", "fixed").css("left", x + "px").css("top", y + "px");
    ContextMenuDiv.fadeIn(300, startFocusOut());
}

function startFocusOut() {
    $(document).on("click", function () {
        ContextMenuDiv.hide(300);              // To hide the context menu
        $(document).off("click");
    });
}

function LoadQueueMembers() {
    //Api/GetQueueMembers/{sessionID}/{workflowID}/{queueID}
    ShowWaitSpinner('Loading...')
    $.ajax({
        url: "Api/GetQueueMembers/" + sessionID + "/" + selectedWorkflowID + "/" + SelectedInstanceQueue,
        type: "Get",
        async: true,
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                ECMNotification(msg, 3, stack);
            }
            else {
                var users = data.Data;                
                var lstUsers = $('#lstQueueMembers');
                lstUsers.empty();
                for (var i = 0; i < users.length; i = i + 1) {
                    var usr = users[i];
                    var dispName = HtmlEncode(usr.UserName+(usr.FullName==""?"":" ("+usr.FullName+")")  );
                    lstUsers.append('<div userid="' + usr.UserID + '">' + dispName + '</div>');
                }

                $('#windowAssignmentModal').show();
                $('#windowAssignment').fadeIn();
            }
            HideWaitSpinner();
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}

function FilterMembers(txtbx) {

    var filterText = $(txtbx).val().toLowerCase();
    var items = $('#lstQueueMembers>div');

    for (var i = 0; i < items.length; i = i + 1) {
        var item = $(items[i]);
        if (filterText !='' && item.text().toLowerCase().indexOf(filterText) == -1) {
            item.hide();
        }
        else {
            item.show();
        }
    }
}

function TakeOwnership() {
    //  Api/TakeOwnership/{sessionID}/{workflowID}
    //  data: int[]
    if (!(SelectedInstanceIDs === undefined || SelectedInstanceIDs == null || SelectedInstanceIDs.length == 0)) {
        ShowWaitSpinner('Loading...')
        $.ajax({
            url: "Api/TakeOwnership/" + sessionID + "/" + selectedWorkflowID,
            type: "Post",
            data: JSON.stringify(SelectedInstanceIDs),
            async: true,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    HideWaitSpinner();
                    ECMNotification(msg, 3, stack);
                }
                else {
                    HideWaitSpinner();
                    RefreshMenuItems();
                }                
            },
            error: function (msg) {
                ECMNotification(msg, 2);
                HideWaitSpinner();
            }
        });
    }
}

function AssignOwnership() {
    //  Api/AssignOwnership/{sessionID}/{workflowID}/{userID}
    //  data: int[]
    var items = $('#lstQueueMembers>.selected');
    if (items.length==1) {
        var userid = $(items[0]).attr('userid');
        if (!(SelectedInstanceIDs === undefined || SelectedInstanceIDs == null || SelectedInstanceIDs.length == 0)) {
            ShowWaitSpinner('Loading...')
            $.ajax({
                url: "Api/AssignOwnership/" + sessionID + "/" + selectedWorkflowID + "/" + userid,
                type: "Post",
                data: JSON.stringify(SelectedInstanceIDs),
                async: true,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data.Messages.ErrorMessages.length > 0) {
                        HideWaitSpinner();
                        var msg = data.Messages.ErrorMessages[0];
                        var stack = data.Stack;
                        ECMNotification(msg, 3, stack);
                    }
                    else {
                        HideWaitSpinner();
                        CloseAssignmentWindow();
                        RefreshMenuItems();
                    }
                    

                },
                error: function (msg) {
                    ECMNotification(msg, 2);
                    HideWaitSpinner();
                }
            });
        }
    }
    
}

function CloseAssignmentWindow() {    
    $('#windowAssignment').fadeOut();
    $('#windowAssignmentModal').hide();
}

function RemoveOwnership() {
    //  Api/RemoveOwnership/{sessionID}/{workflowID}
    //  data: int[]

    //var currentSelection = $("#workItemsGrid").data("kendoGrid").select();
    //var selectedRows = [];
    //currentSelection.each(function () {
    //    var currentRowIndex = $(this).closest("tr").index();
    //    if (selectedRows.indexOf(currentRowIndex) == -1) {
    //        selectedRows.push(currentRowIndex);
    //    }
    //});

    if (!(SelectedInstanceIDs === undefined || SelectedInstanceIDs == null || SelectedInstanceIDs.length == 0)) {
        ShowWaitSpinner('Loading...')
        $.ajax({
            url: "Api/RemoveOwnership/" + sessionID + "/" + selectedWorkflowID,
            type: "Post",
            data: JSON.stringify(SelectedInstanceIDs),
            async: true,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    HideWaitSpinner();
                    ECMNotification(msg, 3, stack);
                }
                else {
                    HideWaitSpinner();
                    RefreshMenuItems();
                }                
            },
            error: function (msg) {
                ECMNotification(msg, 2);
                HideWaitSpinner();
            }
        });
        //-- reset scroll to position
        //$("#workItemsGrid").data("kendoGrid").select("tr:eq(" + selectedRows[0] + ")");
    }
}


function Open(showIFrame) {
    if (!(SelectedInstanceIDs === undefined || SelectedInstanceIDs == null || SelectedInstanceIDs.length == 0)) {
        QueuedItemIDs = [];
        for (var i = 0; i < SelectedInstanceIDs.length; i = i + 1) {
            QueuedItemIDs[i] = SelectedInstanceIDs[i];
        }
        OpenWorkItem(showIFrame); 
    }
}

//Administrative Impersonation
{
    $(window).keydown(function (event) {
        if (event.ctrlKey && event.keyCode == 89) {
            if (_toolbox.session.isImpersonationEnabled) {
                ShowImpersonationWindow();
                event.preventDefault();
            }
        }
    });

    function ShowImpersonationWindow() {
        //Api/GetQueueMembers/{sessionID}/{workflowID}/{queueID}
        ShowWaitSpinner('Loading...')
         
        $.ajax({
            url: "GetUsers"+"/"+sessionID,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: true,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    ECMNotification(msg, 3, stack);
                }
                else {
                    var users = data.Data;
                    var lstUsers = $('#lstImpersonationUsers');
                    lstUsers.empty();
                    for (var i = 0; i < users.length; i = i + 1) {
                        var usr = users[i];
                        var dispName = HtmlEncode(usr.Username + (usr.FullName == "" ? "" : " (" + usr.FullName + ")"));
                        lstUsers.append('<div userid="' + usr.ID + '">' + dispName + '</div>');
                    }

                    $('#windowImpersonationModal').show();
                    $('#windowImpersonation').fadeIn();
                }
                HideWaitSpinner();
            },
            error: function (msg) {
                ECMNotification(msg, 2);
                HideWaitSpinner();
            }
        });
    }

    function FilterImpersonationUsers(txtbx) {

        var filterText = $(txtbx).val().toLowerCase();
        var items = $('#lstImpersonationUsers>div');

        for (var i = 0; i < items.length; i = i + 1) {
            var item = $(items[i]);
            if (filterText != '' && item.text().toLowerCase().indexOf(filterText) == -1) {
                item.hide();
            }
            else {
                item.show();
            }
        }
    }

    function ImpersonateUser() {
        var items = $('#lstImpersonationUsers>.selected');
        if (items.length == 1) {
            var userid = $(items[0]).attr('userid');
            _toolbox.engine.impersonate(_toolbox, userid);
            //_toolbox.session.impersonate(_toolbox, userid);
            location.reload();
        }
    }

    function CloseImpersonationWindow() {
        $('#lstImpersonationUsers').empty();
        $('#windowImpersonationModal').hide();
        $('#windowImpersonation').fadeOut();
    }

    function ShowPasswordChangeWindow() {
        $('#txtPasswordChange').val('');
        $('#txtPasswordChange2').val('');
        $('#windowPWChangeModal').show();
        $('#windowPWChange').fadeIn();
    }

    function ChangePassword() {
        var pw1 = $('#txtPasswordChange').val();        
        var pw2 = $('#txtPasswordChange2').val();
        if (pw1 != pw2) {
            ECMNotification("Passwords Do Not Match.", 2)
        }
        else {
            //ChangePassword/{sessionID}
            var pwchange = {
                Password: pw1
            };
            ShowWaitSpinner('Loading...')
            $.ajax({
                url: "ChangePassword/" + sessionID,
                type: "Post",
                data: JSON.stringify(pwchange),
                async: true,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data.Messages.ErrorMessages.length > 0) {
                        var msg = data.Messages.ErrorMessages[0];
                        var stack = data.Stack;
                        HideWaitSpinner();
                        ECMNotification(msg, 3, stack);
                    }
                    else {
                        HideWaitSpinner();
                        ECMNotification("Password Updated Successfully", 0);
                        $('#windowPWChangeModal').hide();
                        $('#windowPWChange').fadeOut();
                    }
                },
                error: function (msg) {
                    ECMNotification(msg, 2);
                    HideWaitSpinner();
                }
            });
        }
    }

    function ClosePasswordChangeWindow() {
        $('#windowPWChangeModal').hide();
        $('#windowPWChange').fadeOut();
    }


}

function SaveClientSettings() {
    _toolbox.engine.saveClientSettings(_toolbox, ClientSettings);
}

function SessionTimedOut() {
    window.location.href = 'login.html?sessionexpired=true';
}

function KeepAlive() {
    setInterval(
        function ()
        {
            if (!_toolbox.engine.pingSession(_toolbox, sessionID)) { SessionTimedOut(); }
        }
    , 25000);
}

var get_params = function (search_string) {
    var parse = function (params, pairs) {
        var pair = pairs[0];
        var parts = pair.split('=');
        var key = decodeURIComponent(parts[0]);
        var value = decodeURIComponent(parts.slice(1).join('='));
        // Handle multiple parameters of the same name
        if (typeof params[key] === "undefined") {
            params[key] = value;
        } else {
            params[key] = [].concat(params[key], value);
        }
        return pairs.length == 1 ? params : parse(params, pairs.slice(1))
    }
    // Get rid of leading ?
    return search_string.length == 0 ? {} : parse({}, search_string.substr(1).toLowerCase().split('&'));
}
var params = get_params(location.search);