﻿




var QueuedItemIDs = null;
function OpenWorkItem(showIFrame) {

    if (showIFrame == null) { showIFrame = true; }

    var itemID = QueuedItemIDs[0];
    $.ajax({
        url: "Api/OpenWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + itemID,
        type: "GET",        
        async: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                WorkItemData = data.Data;
                if (showIFrame) { SetLargeMenu(); SetIframeContent(WorkItemData.WorkItemPageURL); ShowIframe(); }
                HideWaitSpinner();
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}

var WorkItemData = null;
function LoadWorkItemData() {
    return WorkItemData;
}

function LoadCommentsAndHistory(workflowID, itemID, completeMethod) {
    $.ajax({
        url: "Api/GetCommentsAndHistory/" + sessionID + "/" + workflowID + "/" + itemID,
        type: "GET",
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        converters: {
            "text json": function (data) {
                return $.parseJSON(data, true);
            }
        },
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                ECMNotification(msg, 3, stack);
            }
            else {
                completeMethod(data.Data);
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}

function AddComment(workflowID, itemID, instanceID, comment, completeMethod) {
    //Api/AddComment/{sessionID}/{workflowID}/{itemID}/{instanceID}
    $.ajax({
        url: "Api/AddComment/" + sessionID + "/" + workflowID + "/" + itemID + "/" + instanceID,
        type: "Post",
        data: JSON.stringify(comment),
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        converters: {
            "text json": function (data) {
                return $.parseJSON(data, true);
            }
        },
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                completeMethod(data.Data);
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}


function Submit(workflowID, workItemID, workInstanceID, data, Submit_Complete) {
    /*
    public class SubmitData
    {
        public short WorkflowID { get; set; }
        public long WorkInstanceID { get; set; }
        public FieldUpdate[] FieldUpdates { get; set; }
        public string SelectedSendToQueue { get; set; }
        public string SelectedSendToUser { get; set; }
        public string ButtonText { get; set; }
    }

    public class SubmitReturn
    {
        public bool CloseWorkItem { get; set; }
        public Javascript[] JavascriptFunctions { get; set; }
        public Popup[] Popups { get; set; }
    }
    */
    ShowWaitSpinner('Please Wait...');
    $.ajax({
        url: "Api/SubmitWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + workItemID + "/" + workInstanceID,
        type: "Post",
        data: JSON.stringify(data),
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                HideWaitSpinner();


                if (data.Data.JavascriptFunctions.length > 0) {
                    for (var i = 0; i < data.Data.JavascriptFunctions.length; i = i + 1) {
                        var jsf = data.Data.JavascriptFunctions[i];
                        if (jsf.RunLocation == 0) {
                            try {
                                eval(jsf.Script);
                            }
                            catch (e) { }
                        }
                    }
                }
                if (data.Data.Popups.length > 0) {
                    for (var i = 0; i < data.Data.Popups.length; i = i + 1) {
                        var pop = data.Data.Popups[i];
                        if (pop.Popuptype == 2) { //New Window
                            try {
                                var url = pop.URL + (pop.URL.indexOf('?') != -1 ? pop.URL + '&sessionID=' + sessionID : '?sessionID=' + sessionID);
                                window.open(url, "_blank");
                            }
                            catch (e) { }
                        }
                        else if (pop.Popuptype == 1) { //Workflow Main Window
                            try {
                                var url = pop.URL + (pop.URL.indexOf('?') != -1 ? pop.URL + '&sessionID=' + sessionID : '?sessionID=' + sessionID);
                                ShowPopup(url);
                            }
                            catch (e) { }
                        }
                    }
                }

                //Call Client Side Method
                try {
                    Submit_Complete(data.Data);
                }
                catch (e) { }


                if (data.Data.CloseWorkItem) {
                    if (GridIsVisible || ClientSettings === undefined || ClientSettings == null || ClientSettings.PickNext === undefined || ClientSettings.PickNext == null || ClientSettings.PickNext == 0) {
                        SetLargeMenu();
                        ShowGrid();
                        RefreshMenuItems();
                    }
                    else {
                        PickNextWorkItem(ClientSettings.PickNext);
                    }
                }

            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}

function PickNextWorkItem(pickNextOption) {

    if (pickNextOption == 4) {
        //-- pick next from selected rather than go up to server to figure out next

        //-- remove element 0 first
        QueuedItemIDs.splice(0, 1);
        if (QueuedItemIDs.length == 0) {
            ECMNotification("No more items selected", 1);
            SetLargeMenu();
            ShowGrid();
            RefreshMenuItems();
        }
        else {
            //-- open the next item
            OpenWorkItem(true);
        }
    }
    else {
        //Api/PickNextWorkItem/{sessionID}/{workflowID}/{queueID}/{pickNextOption}
        ShowWaitSpinner('Please Wait...');
        $.ajax({
            url: "Api/PickNextWorkItem/" + sessionID + "/" + selectedWorkflowID + "/" + selectedQueueID + "/" + pickNextOption,
            type: "GET",
            async: true,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    HideWaitSpinner();
                    ECMNotification(msg, 3, stack);
                }
                else {
                    HideWaitSpinner();
                    if (data.Data == null) {
                        SetLargeMenu();
                        ShowGrid();
                        RefreshMenuItems();
                    }
                    else {
                        WorkItemData = data.Data;
                        ShowIframe();
                        SetLargeMenu();
                        SetIframeContent(WorkItemData.WorkItemPageURL);
                        HideWaitSpinner();
                    }
                }
            },
            error: function (msg) {
                ECMNotification(msg, 2);
                HideWaitSpinner();
            }
        });
    }
}

function EndWorkflow() {
    //public class EndWorkflowData
    //{
    //    public long[] WorkItemIDs { get; set; }
    //    public long[] WorkInstanceIDs { get; set; }
    //}
    //[Route("Api/EndWorkItemsMultiple/{sessionID}/{workflowID}")]

    var data = {
        WorkInstanceIDs: SelectedInstanceIDs
    }

    ShowWaitSpinner('Please Wait...');
    $.ajax({
        url: "Api/EndWorkItemsMultiple/" + sessionID + "/" + selectedWorkflowID,
        type: "Post",
        data: JSON.stringify(data),
        async: true,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.Messages.ErrorMessages.length > 0) {
                var msg = data.Messages.ErrorMessages[0];
                var stack = data.Stack;
                HideWaitSpinner();
                ECMNotification(msg, 3, stack);
            }
            else {
                HideWaitSpinner();
                RefreshMenuItems();
            }
        },
        error: function (msg) {
            ECMNotification(msg, 2);
            HideWaitSpinner();
        }
    });
}