﻿

$(document).ready(function () {

    window.addEventListener('message', function (event) {
        var eventOrigin = event.origin;
        var msg = null;
        var isECMTMessage = false;
        try{
            msg = JSON.parse(event.data);
            if (msg === undefined || msg == null || msg.MessageType === undefined || msg.MessageType == null || msg.MessageType.trim() == '') {
                throw "Message Format Incorrect.  Message does not contain MessageType";
            }
            else {
                isECMTMessage = true;
            }

        }
        catch(ex){

        }
        try {
            if (isECMTMessage) {
                switch (msg.MessageType.toUpperCase()) {
                    case 'ECMT_GETWORKITEMDATA':                        
                        var returnData = {
                            MessageType: "ECMT_GETWORKITEMDATA_RETURN",
                            WorkItemData: LoadWorkItemData(msg.WorkInstanceID)
                        };
                        var jsonWorkItemData = JSON.stringify(returnData);
                        event.source.postMessage(jsonWorkItemData, event.origin);
                        break;
                    case 'ECMT_GETHISTORYANDCOMMENTS':
                        LoadCommentsAndHistory(msg.workflowID, msg.itemID, _EcmtPostMessage_History_And_Comments_Completed, event);
                        break;
                    case 'ECMT_ADDCOMMENT':
                        AddComment(msg.WorkflowID, msg.WorkItemID, msg.WorkInstanceID, msg.comment, _EcmtPostMessage_AddComment_Completed, event);
                        break;
                    case 'ECMT_SUBMIT':
                        Submit(msg.WorkflowID, msg.WorkItemID, msg.WorkInstanceID, msg.data, _EcmtPostMessage_Submit_Completed, event);
                        break;
                    case 'ECMT_CLOSE':
                        SetLargeMenu();
                        ShowGrid();
                        RefreshMenuItems();
                        break;
                }
            }
        }
        catch (ex) {
            ECMNotification('Invalid PostMessage Received: '+ex);
        }
    });
});

function _EcmtPostMessage_History_And_Comments_Completed(HandCData, event) {
    var returnData = {
        MessageType: "ECMT_GETHISTORYANDCOMMENTS_RETURN",
        CommentsAndHistory: HandCData
    };
    var jsonData = JSON.stringify(returnData);
    event.source.postMessage(jsonData, event.origin);
}

function _EcmtPostMessage_AddComment_Completed(HandCData, event) {
    var returnData = {
        MessageType: "ECMT_ADDCOMMENT_RETURN",
        CommentsAndHistory: HandCData
    };
    var jsonData = JSON.stringify(returnData);
    event.source.postMessage(jsonData, event.origin);
}

function _EcmtPostMessage_Submit_Completed(data, event) {
    var returnData = {
        MessageType: "ECMT_SUBMIT_RETURN",
        Data: data
    };
    var jsonData = JSON.stringify(returnData);
    event.source.postMessage(jsonData, event.origin);
}

function _EcmtPostMessage_WorkItem_Visible(workInstanceID) {
    var returnData = {
        MessageType: "ECMT_WORKITEM_VISIBLE",
        WorkInstanceID: workInstanceID
    };
    var jsonData = JSON.stringify(returnData);
    iframe1Receiver.postMessage(jsonData, '*');
    iframe2Receiver.postMessage(jsonData, '*');
}