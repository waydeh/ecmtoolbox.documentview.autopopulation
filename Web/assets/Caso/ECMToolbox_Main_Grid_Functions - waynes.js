﻿
var grid = null;

var ShowFilterRow = false;
var GridSetup=false;
var DataLoaded=false;
var GridData = null;
var CurrentlyDisplayedWorkflow = null;
var CurrentlyDisplayedQueue = null;
var Columns = null;

function LoadGrid() {
    try{
        GridSetup = false;
        DataLoaded = false;

        LoadGridData();
            
        if (CurrentlyDisplayedWorkflow != selectedWorkflowID || CurrentlyDisplayedQueue != selectedQueueID) {
            if (grid != null) {
                $("#workItemsGrid").data("kendoGrid").destroy();
                $("#workItemsGrid").empty();
                $("#workItemsGrid").unbind();
            }
            CurrentlyDisplayedWorkflow = selectedWorkflowID;
            CurrentlyDisplayedQueue = selectedQueueID;
        

            var clientSettingColumns = null;
            if (selectedQueueID == -2) {
                Columns = WorkflowQueueColumns[selectedWorkflowID].GroupBox;
                try { clientSettingColumns =ClientSettings.GridSettings[selectedWorkflowID].GroupBox; } catch (e) { }
            }
            else if (selectedQueueID == -1) {
                Columns = WorkflowQueueColumns[selectedWorkflowID].Inbox
                try { clientSettingColumns =ClientSettings.GridSettings[selectedWorkflowID].Inbox; } catch (e) { }
            }
            else {
                Columns = WorkflowQueueColumns[selectedWorkflowID].Queues[selectedQueueID];
                try { clientSettingColumns =ClientSettings.GridSettings[selectedWorkflowID].Queues[selectedQueueID]; } catch (e) { }
            }

            var disableGridPersistance = !(ClientSettings === undefined || ClientSettings == null || ClientSettings.DisableGridPersistance === undefined || ClientSettings.DisableGridPersistance == null || ClientSettings.DisableGridPersistance == false);
            if (clientSettingColumns != null && !disableGridPersistance) {
                Columns = CombineColumnsWithClientSettings(Columns, clientSettingColumns);
            }

            Columns = IconColumnFilter(Columns);
            
            var selectableAttribute = { selectable: "row", title: "", width: 30 };
            Columns.splice(0, 0, selectableAttribute);
            
            kendoUniqueID = 0;
            grid = $("#workItemsGrid").kendoGrid({            
                columns: Columns,
                autoBind: false,
                height: '100%',
                //filterable: {
                //    mode: ShowFilterRow ? "row" : "menu",
                //    //extra: false,
                //    operators: {
                //        string: {
                //            startswith: "Starts with",
                //            eq: "Is equal to",
                //            neq: "Is not equal to"
                //        },
                //        date: {
                //            lte: "Before",
                //            gte: "After"
                //        }
                //    }
                //},
                //change: onChange,
                filterable: true,
                filter: onFiltering,
                dataBound: SetItemBackgroundColor,
                columnReorder: ColumnsReordered,
                columnResize: ColumnsResized,
                sortable: true,
                resizable: true,
                reorderable: true,
                //selectable: "row",
                //pageable: {
                //    refresh: true,
                //    pageSizes: true,
                //    buttonCount: 5
                //},
                scrollable: {
                    virtual: true
                },
                dataBound: function (e) {
                    //-- reselect rows
                    $.each(selectedItems, function (i, item) {
                        var row = grid.data("kendoGrid").tbody.find("tr[data-uid='" + item.uid + "']");
                        grid.data("kendoGrid").select(row);
                    });
                }
            });

            //grid.on("contextmenu", ".k-virtual-scrollable-wrap table tbody tr", function (e) {
            //    var targetID = e.currentTarget.getAttribute("data-uid");
            //    if (!IsItemSelected(e)) {
            //        HandleRowClick(e);
            //        DetermineActiveFunctions();
            //    }
            //    ShowContextMenu(e);
            //    e.preventDefault();
            //});

            grid.on("dblclick", ".k-virtual-scrollable-wrap table tbody tr", function (e) {
                var rowIndex = e.currentTarget.rowIndex;
                grid.data("kendoGrid").select("tr:eq(" + rowIndex.toString() + ")");
                Open(true);
            });

            grid.on("click", ".k-virtual-scrollable-wrap table tbody tr", function (e) {
                HandleRowClick(e);
                DetermineActiveFunctions();
            });

            //-- select all header checkbox
            grid.on("click", ".k-checkbox", function (e) {
                selectedItems = [];
                if (e.currentTarget.checked) {
                    var dataSource = grid.data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    selectedItems = query.filter(filters).data;
                }
                DetermineActiveFunctions();
            });

            //-- prevents text highlighting on rows
            grid.on("mousedown", function (e) {
                e.preventDefault();
            })

            ////replace poorly rendering png filters
            //if (!ShowFilterRow) $('.k-filter').removeClass('k-icon').removeClass('k-filter').addClass('fa').addClass('fa-filter');
            
        }
        else {
            grid.data("kendoGrid").dataSource.data([]);
        }
        if (DataLoaded) {
            SetGridData();
            DetermineActiveFunctions(null);
        }
        else {
            GridSetup = true;
        }
    }
    catch (ex) {
        ECMNotification(ex, 3);
        HideWaitSpinner();
    }
}
//function onChange(arg) {
    
//    var selectedItemsNew = $.map(this.select(), function (item) {

//        var uid = $(item).attr('data-uid');
//        return $.grep(arg.sender._data, function (e) { return e.uid == $(item).attr('data-uid') });
//    });

//    selectedItemsNew = $.grep(selectedItemsNew, function (e) { return e.uid != selectedItems.uid });
//    selectedItems = selectedItems.concat(selectedItemsNew);

//    DetermineActiveFunctions();
//}

function onFiltering(arg) {
    if (arg.field == 'IC') {
        if (arg.filter != null) {
            for (var i = 0; i < arg.filter.filters.length; i = i + 1) {
                arg.filter.filters[i].operator = "contains";
            }
        }
    }
}

function LoadGridData(){
    //Api/GetGridItems/{sessionID}/{workflowID}/{queueID}
    ShowWaitSpinner('Loading...', 'wfContent')    
    $.ajax({
        url: "Api/GetGridItems/" + sessionID + "/" + selectedWorkflowID + "/" + selectedQueueID,
        type: "Get",
        async: true,
        converters: {
            "text json": function (data) {
                return $.parseJSON(data, true);
            }
        },
        success: function (data) {
            try{
                if (data.Messages.ErrorMessages.length > 0) {
                    var msg = data.Messages.ErrorMessages[0];
                    var stack = data.Stack;
                    ECMNotification(msg, 3, stack);
                }
                else {
                    var results = data.Data;
                    GridData = results.Data;
                    QueuePermissions = results.QueuePermissions;
                    QueueButtons = results.QueueButtons;                
                
                    if (GridSetup) {
                        SetGridData();
                        DetermineActiveFunctions(null);
                    }
                    else {
                        DataLoaded = true;
                    }
                }                
            }
            catch (ex) {
                HideWaitSpinner('wfContent');
                ECMNotification(ex, 3);
            }
        },
        error: function (msg) {
            ECMNotification(msg.statusText + ': ' + msg.responseText, 3);
            HideWaitSpinner('wfContent');
        }
    });
}


function SetGridData() {
    grid.data("kendoGrid").dataSource.pageSize(35);
    grid.data("kendoGrid").dataSource.data(GridData);
    //grid.data("kendoGrid").data(GridData);
    KendoGridRemoveAltColumns();
    HideWaitSpinner('wfContent');
}

function IconColumnFilter(cols) {
    for (var i = 0; i < cols.length; i = i + 1) {
        if (cols[i].field == 'IC') {
            cols[i].filterable = {
                multi: true,
                dataSource: [{ IC: 'foo' }],
                itemTemplate: function (e) {
                    if (e.field == "all") {
                        //-- handle the check-all checkbox template
                        return "<div><label><strong><input type='checkbox' />#= all#</strong></label></div>";
                    } else {
                        //-- handle the other checkboxes
                        return $('#template_Grid_Icon_Filter').html();
                    }
                }
            };
            cols[i].encoded = false;
        }
    }
    return cols;
}

function CombineColumnsWithClientSettings(cols, clicols) {
    var finalCols = [];
    for (var i = 0; i < clicols.length; i = i + 1) {
        var clicol = clicols[i];        
        for (var iCol = 3; iCol < cols.length; iCol = iCol + 1) {
            if (!cols[iCol].hidden && cols[iCol].field == clicol.field) {
                cols[iCol].width = clicol.width;
                cols[iCol].copied = true;
                finalCols[finalCols.length] = cols[iCol];
                break;
            }
        }
    }    
    for (var iCol = 3; iCol < cols.length; iCol = iCol + 1) {
        if (cols[iCol].copied != true) {
            finalCols[finalCols.length] = cols[iCol];
        }
        cols[iCol].copied = null;
    }
    return finalCols;
}

function ColumnsReordered(evt) {
    var userColumns = GetGridColumnData(evt.sender.columns);
    var movedCol = userColumns.splice(evt.oldIndex, 1);
    userColumns.splice(evt.newIndex, 0, movedCol[0]);
    UpdateGridSettings(userColumns);    
}

function ColumnsResized(evt) {
    var userColumns = GetGridColumnData(evt.sender.columns);    
    UpdateGridSettings(userColumns);
}

function GetGridColumnData(columns) {
    var colData = [];
    var counter = 0;
    for (var i = 0; i < columns.length; i = i + 1) {
        if (!columns[i].hidden) {
            colData[counter] = {
                field: columns[i].field,
                width: columns[i].width
            }
            counter++;
        }
    }
    return colData;
}

function UpdateGridSettings(userColumns) {
    if (ClientSettings === undefined || ClientSettings == null) ClientSettings = {};
    if (ClientSettings.GridSettings === undefined || ClientSettings.GridSettings == null) ClientSettings.GridSettings = [];
    if (ClientSettings.GridSettings[selectedWorkflowID] === undefined || ClientSettings.GridSettings[selectedWorkflowID] == null) {
        ClientSettings.GridSettings[selectedWorkflowID] = { Inbox: {}, GroupBox: {}, Queues: [] };
    }
    if (selectedQueueID == -2) {
        ClientSettings.GridSettings[selectedWorkflowID].GroupBox = userColumns;
    }
    else if (selectedQueueID == -1) {
        ClientSettings.GridSettings[selectedWorkflowID].Inbox = userColumns;
    }
    else {
        ClientSettings.GridSettings[selectedWorkflowID].Queues[selectedQueueID] = userColumns;
    }
    
    SaveClientSettings();
}








var selectedItems = [];
var startItem = null;
var multiSelect = true;
function HandleRowClick(e) {

    e.preventDefault();
    //-- this ensures the header checkbox is reset to unchecked
    $('.k-checkbox').prop('checked', false);

    var rowIndex = e.currentTarget.rowIndex;
    var data_uid = e.currentTarget.getAttribute("data-uid");

    var item = $.grep(selectedItems, function (e) { return e.uid == data_uid });
    if (item.length > 0) {
        //-- remove 
        selectedItems = $.grep(selectedItems, function (e) { return e.uid != data_uid });
        $(grid.data("kendoGrid").tbody.find("tr[data-uid='" + data_uid + "']")).removeClass("k-state-selected");
        $(grid.data("kendoGrid").tbody.find("tr[data-uid='" + data_uid + "']")).find('td:first-child input').prop('checked', false);
    }
    else {
        //-- add
        var data = grid.data("kendoGrid").dataSource.view();
        selectedItems = selectedItems.concat($.grep(data, function (e) { return e.uid == data_uid }));
        $(grid.data("kendoGrid").tbody.find("tr[data-uid='" + data_uid + "']")).addClass("k-state-selected");
        $(grid.data("kendoGrid").tbody.find("tr[data-uid='" + data_uid + "']")).find('td:first-child input').prop('checked', true);
    }

    //e.preventDefault();
    //var targetID = e.currentTarget.getAttribute("data-uid");
    //if (targetID != null) {

    //    if ((!e.ctrlKey && !e.shiftKey) || selectedItems.length == 0 || startItem == targetID || !multiSelect) {
    //        selectedItems = [];
    //        var data = grid.data("kendoGrid").dataSource.view();
    //        for (var i = 0; i < data.length; i = i + 1) {
    //            if (data[i].uid == targetID) {
    //                selectedItems[parseInt(targetID.substring(3))] = data[i];
    //                startindex = i;
    //                break;
    //            }
    //        }
    //        startItem = targetID;
    //    }
    //    else if (e.shiftKey) {
    //        selectedItems = [];
    //        var dataSource = grid.data("kendoGrid").dataSource;
    //        var filters = dataSource.filter();
    //        var sorts = dataSource.sort();
    //        var allData = dataSource.data();
    //        var data = null;
    //        if (filters == null && sorts == null) {
    //            data = allData;
    //        }
    //        else if (filters != null && sorts != null) {
    //            var query = new kendo.data.Query(allData);
    //            data = query.filter(filters).sort(sorts).data;
    //        }
    //        else if (sorts != null) {
    //            var query = new kendo.data.Query(allData);
    //            data = query.sort(sorts).data;
    //        }
    //        else {
    //            var query = new kendo.data.Query(allData);
    //            data = query.filter(filters).data;
    //        }

    //        var startindex = -1;
    //        var endindex = -1;
    //        for (var i = 0; i < data.length; i = i + 1) {
    //            if (data[i].uid == startItem) {
    //                startindex = i;
    //                break;
    //            }
    //        }
    //        for (var i = 0; i < data.length; i = i + 1) {
    //            if (data[i].uid == targetID) {
    //                endindex = i;
    //                break;
    //            }
    //        }
    //        if (startindex != -1 && endindex != -1) {
    //            if (startindex > endindex) {
    //                var temp = endindex;
    //                endindex = startindex;
    //                startindex = temp;
    //            }
    //            for (var iselection = startindex; iselection <= endindex; iselection = iselection + 1) {
    //                selectedItems[parseInt(data[iselection].uid.substring(3))] = data[iselection];
    //            }
    //        }
    //        else throw "Invalid Selection";
    //    }
    //    else if (e.ctrlKey) {
    //        if (startItem == targetID) startItem = null;
    //        var selecteduid = parseInt(targetID.substring(3))
    //        if (selectedItems[selecteduid]) selectedItems[selecteduid] = null;
    //        else {
    //            var data = grid.data("kendoGrid").dataSource.view();
    //            for (var i = 0; i < data.length; i = i + 1) {
    //                if (data[i].uid == targetID) {
    //                    selectedItems[selecteduid] = data[i];
    //                    startindex = i;
    //                    break;
    //                }
    //            }
    //        }
    //    }
    //    UpdateItemsViewState();
    //}
}

function IsItemSelected(e) {
    var targetID = e.currentTarget.getAttribute("data-uid");
    if (targetID != null) {
        var data = grid.data("kendoGrid").dataSource.view();        
        for (var i = 0; i < data.length; i = i + 1) {
            if (data[i].uid == targetID) {
                for (var x = 0; x < selectedItems.length; x = x + 1) {
                    if (selectedItems[x] == data[i]) return true;
                }
                return false;
            }
        }
    }
    return false;
}

function UpdateItemsViewState(e) {
    var gridData = grid.data("kendoGrid");
    var rows = $(gridData.tbody).find("tr");
    var data = gridData.dataSource.view();
    for (var i = 0; i < rows.length; i++) {
        if (data[i].F != '') $(rows[i]).css("background-color", data[i].F);
        var rowID = rows[i].getAttribute("data-uid");
        if (selectedItems[parseInt(rowID.substring(3))]) {
            $(rows[i]).addClass("k-state-selected");
        }
        else if (e === undefined) {
            $(rows[i]).removeClass("k-state-selected");
        }
    }
}


function GetSelectedItemData() {
    var selectedItemsData = [];
    var selectedItemIndex = 0;
    for (var i = 0; i < selectedItems.length; i = i + 1) {
        if (selectedItems[i]) {
            selectedItemsData[selectedItemIndex] = selectedItems[i];
            selectedItemIndex = selectedItemIndex + 1;
        }
    }
    return selectedItemsData;
}

//This function will be used to replace the kendo default guid function.  
//guid results for the grid per row are extremely slow.  
//This is VERY fast.  
//Should be reset to 0 after every grid load, but doesn't have to be.
var kendoUniqueID = 0;
kendo.guid = function () {
    kendoUniqueID = kendoUniqueID + 1;
    return "kID" + kendoUniqueID;
}

function changeHighlightProperty(className, value) {
    // This function overwrites the kendo grid property .k-state-selected  making the background color !important so row background colors no longer override the row selection.
    var ss = document.styleSheets;
    for (var i = 0; i < ss.length; i++) {
        var ss = document.styleSheets;
        var rules = ss[i].cssRules || ss[i].rules;
        for (var j = 0; j < rules.length; j++) {
            if (rules[j].selectorText === ".k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-list > .k-state-selected, .k-list > .k-state-highlight, .k-panel > .k-state-selected, .k-ghost-splitbar-vertical, .k-ghost-splitbar-horizontal, .k-state-selected.k-draghandle:hover, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-state-selected.k-today, .k-marquee-color") {
                rules[j].style.backgroundColor = rules[j].style.backgroundColor + " !important";
            }
        }
    }
}
