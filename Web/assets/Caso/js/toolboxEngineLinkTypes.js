﻿engine.prototype.linkTypesRead = function (toolbox, options) {

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isAdmin
        };

        $.ajax({
            url: "LinkTypesGet/" + toolbox.session.sessionID,
            data: "",
            dataType: "json",
            type: "GET",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                    _linkTypes = data.Data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in linkTypesRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in linkTypesRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
