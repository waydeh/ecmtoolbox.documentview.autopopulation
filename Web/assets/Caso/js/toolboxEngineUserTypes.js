﻿//----------------------------------------------
//-- user types functions
//----------------------------------------------
//--
//-- user types read
//--

engine.prototype.userTypesGet = function (toolbox) {

    var userTypes = null;

    try {
        $.ajax({
            url: "UserTypesGet/" + toolbox.session.sessionID,
            data: "",
            dataType: "json",
            type: "GET",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    userTypes = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in userTypesRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in userTypesRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return userTypes;
}

engine.prototype.userTypesRead = function (toolbox, options) {

    try {
        $.ajax({
            url: "UserTypesGet/" + toolbox.session.sessionID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                    
                    //dataSource.fetch();
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in userTypesRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in userTypesRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
