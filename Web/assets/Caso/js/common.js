﻿function getUrlParameter(name) {

    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');

    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);

    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

//-- scrollables
$('.slim-scroll').each(function () {
    var $this = $(this);
    $this.slimScroll({
        height: $this.data('height') || 100,
        railVisible: true
    });
});

