﻿
//----------------------------------------------
//-- groups table functions
//----------------------------------------------
//----------------------------------------------
//--
//-- groups table load
//--
engine.prototype.groupGetByGroupID = function (toolbox, groupID) {

    var group = null;

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isSystemAdmin
        };

        $.ajax({
            url: "GroupsGetByID/" + toolbox.session.sessionID + "/" + groupID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    group = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupGetByGroupID : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupGetByGroupID : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return group;
}

engine.prototype.groupsInfoUpdateUsers = function (toolbox, group) {

    var valid = true;

    try {
        $.ajax({
            url: "GroupInfoUpdateUsers/" + toolbox.session.sessionID,
            data: JSON.stringify(group.Data),
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsInfoUpdateUsers : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
                valid = false;
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsInfoUpdateUsers : " + e.message, toolbox.loader.notifier.notificationType.error);
        valid = false;
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

engine.prototype.groupsInfoUpdateLinks = function (toolbox, group) {

    var valid = true;

    try {
        $.ajax({
            url: "GroupInfoUpdateLinks/" + toolbox.session.sessionID,
            data: JSON.stringify(group.Data),
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsInfoUpdateUsers : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
                valid = false;
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsInfoUpdateUsers : " + e.message, toolbox.loader.notifier.notificationType.error);
        valid = false;
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

engine.prototype.groupsInfoUpdateWorkflowPermissions = function (toolbox, group) {

    var valid = true;

    try {
        $.ajax({
            url: "GroupInfoUpdateWorkflowPermissions/" + toolbox.session.sessionID,
            data: JSON.stringify(group.Data),
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsInfoUpdateWorkflowPermissions : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
                valid = false;
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsInfoUpdateWorkflowPermissions : " + e.message, toolbox.loader.notifier.notificationType.error);
        valid = false;
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}


engine.prototype.groupsGetAll = function (toolbox) {

    var groups = null;

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isSystemAdmin
        };

        $.ajax({
            url: "GroupsGet/" + toolbox.session.sessionID,
            data: parameters,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    groups = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsGetAll : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsGetAll : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return groups;
}

engine.prototype.groupsAdd = function (toolbox, groupName) {

    var valid = true;

    try {

        var parameters = {
            GroupID: 0,
            GroupName: groupName
        };

        $.ajax({
            url: "GroupsCreate/" + toolbox.session.sessionID,
            data: JSON.stringify(parameters),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsAdd : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsAdd : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

engine.prototype.groupsEdit = function (toolbox, groupID, groupName) {

    var valid = true;

    try {
        var parameters = {
            GroupID: groupID,
            GroupName: groupName
        };

        $.ajax({
            url: "GroupsUpdate/" + toolbox.session.sessionID,
            data: JSON.stringify(parameters),
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsEdit : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsEdit : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

engine.prototype.groupsDelete = function (toolbox, groupID) {

    var valid = true;

    try {

        var parameters = {
            GroupID: groupID,
            GroupName: ""
        };

        $.ajax({
            url: "GroupsDelete/" + toolbox.session.sessionID,
            data: JSON.stringify(parameters),
            dataType: "json",
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in groupsDelete : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in groupsDelete : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}
//--
//----------------------------------------------


