﻿//----------------------------------------------
//-- sourceFields functions
//----------------------------------------------
//--
//-- get all
//--
engine.prototype.sourceFieldsGetAll = function (toolbox, workflowID) {

    //-- gets all source fields

    var sourceFields = null;

    try {
        $.ajax({
            url: "GetSourceFields/" + toolbox.session.sessionID + "/" + workflowID,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    sourceFields = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in sourceFieldsGetAll : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in sourceFieldsGetAll : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return sourceFields;
}
//--
//--
//----------------------------------------------
//--
//-- sourceFields table CRUD read
//--
//engine.prototype.sourceFieldsRead = function (toolbox, workflowID, options) {
engine.prototype.sourceFieldsRead = function (toolbox, workflowID) {

    //-- only gets source fields not already in use in the workflow fields

    var sourceFields = null;

    try {
        $.ajax({
            url: "SourceFieldsGetNotInUse/" + toolbox.session.sessionID + "/" + workflowID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    sourceFields = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in sourceFieldsRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in sourceFieldsRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return sourceFields;
}

engine.prototype.sourceFieldsUpdate = function (toolbox, workflowID, checkedIDs) {

    var result = false;

    try {

        //JSON.stringify(checkedIDs)
        $.ajax({
            url: "SourceFieldsPut/" + toolbox.session.sessionID + "/" + workflowID,
            data: JSON.stringify(checkedIDs),
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    result = true;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in sourceFieldsUpdate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in sourceFieldsUpdate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return result;
}

