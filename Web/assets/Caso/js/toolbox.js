﻿//----------------------------------------------
//-- toolbox object
//----------------------------------------------
//--
function toolbox() {
    this.uri = '../api';

    this.session = new session;

    this.loader = new loader;

    this.engine = new engine;

    this.adminTableType = new adminTableTypes;
}
//--
//----------------------------------------------




//----------------------------------------------
//-- loader object
//----------------------------------------------
//--
function loader() {
    this.notifier = new notifier;
}

loader.prototype.showWaitDiv = function (someDiv, someMessage) {

    ShowWaitSpinner(someMessage);
    //$("#" + someDiv).block({
    //    message: '<i class="icon-spinner9 spinner"></i>' + '<br />' + someMessage,
    //    overlayCSS: {
    //        backgroundColor: '#1B2024',
    //        opacity: 0.85,
    //        cursor: 'wait'
    //    },
    //    css: {
    //        border: 0,
    //        padding: 0,
    //        backgroundColor: 'none',
    //        color: '#fff'
    //    }
    //});
}

loader.prototype.hideWaitDiv = function (someDiv) {

    HideWaitSpinner();
    //window.setTimeout(function () {
    //    $("#" + someDiv).unblock();
    //}, 500);

    //-- check for messages that might need to be displayed
    if (this.notifier.messages) {

        if (this.notifier.messages.FailureMessages.length > 0) {
            this.notifier.showAlert("Errors", this.notifier.messages.FailureMessages, this.notifier.notificationType.failure);
        }

        if (this.notifier.messages.ErrorMessages.length > 0) {
            this.notifier.showAlert("Errors", this.notifier.messages.ErrorMessages, this.notifier.notificationType.error);
        }

        if (this.notifier.messages.InfoMessages.length > 0) {
            this.notifier.showAlert("Info", this.notifier.messages.InfoMessages, this.notifier.notificationType.info);
        }

        if (this.notifier.messages.SuccessMessages.length > 0) {
            this.notifier.showAlert("Success", this.notifier.messages.SuccessMessages, this.notifier.notificationType.success);
        }

        //-- reset to null
        this.notifier.messages = null;
    }
}
//--
//----------------------------------------------


//----------------------------------------------
//-- admininstartion table types
//----------------------------------------------
//--
function adminTableTypes() {
    this.users = 'users.html';
    this.groups = 'groups.html';
    this.links = 'links.html';
    this.workflows = 'workflows.html';
    this.systemSettings = 'systemSettings.html';
}
//--
//----------------------------------------------

