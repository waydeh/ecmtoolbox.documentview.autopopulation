﻿engine.prototype.linksGetAll = function (toolbox) {

    var links = null;

    try {
        $.ajax({
            url: "LinksGetAll/" + toolbox.session.sessionID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    links = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in linksGetAll : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in linksGetAll : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return links;
}

//----------------------------------------------
//-- links table functions
//----------------------------------------------
//--
//-- links table CRUD read
//--
engine.prototype.linksRead = function (toolbox, options) {

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isAdmin
        };

        $.ajax({
            url: "LinksGet/" + toolbox.session.sessionID,
            data: parameters,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in linksRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in linksRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- links table CRUD create
//--
engine.prototype.linksCreate = function (toolbox, options) {

    try {
        $.ajax({
            url: "LinksCreate/" + toolbox.session.sessionID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in linksCreate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in linksCreate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- links table CRUD update
//--
engine.prototype.linksUpdate = function (toolbox, options) {

    try {
        $.ajax({
            url: "LinksUpdate/" + toolbox.session.sessionID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in linksUpdate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in linksUpdate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- links table CRUD destroy
//--
engine.prototype.linksDestroy = function (toolbox, options) {

    try {
        $.ajax({
            url: "LinksDelete/" + toolbox.session.sessionID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in linksDestroy : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in linksDestroy : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//----------------------------------------------

