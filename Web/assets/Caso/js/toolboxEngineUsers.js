﻿
engine.prototype.usersGetAllExternalUsers = function (toolbox, userType) {

    var users = null;

    try {
        $.ajax({
            url: "UsersGetAllExternalUsers/" + toolbox.session.sessionID + "/" + userType,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    users = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersGetAllExternalUsers : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersGetAllExternalUsers : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return users;
}

engine.prototype.usersAddExternalUsers = function (toolbox, users, userType) {

    var valid = true;

    try {
        $.ajax({
            url: "UsersAddExternalUsers/" + toolbox.session.sessionID + "/" + userType,
            data: JSON.stringify(users),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersAddExternalUsers : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersAddExternalUsers : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

engine.prototype.usersAddECMTUsers = function (toolbox, users) {

    var valid = true;

    try {
        $.ajax({
            url: "UsersAddECMTUsers/" + toolbox.session.sessionID,
            data: JSON.stringify(users),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersAddECMTUsers : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersAddECMTUsers : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

engine.prototype.usersGetAll = function (toolbox) {

    var users = null;

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isSystemAdmin
        };
         
        $.ajax({
            url: "GetUsers" + "/" + toolbox.session.sessionID,
            data: parameters,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    users = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersGetAll : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersGetAll : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return users;
}

engine.prototype.usersGetByUserID = function (toolbox, userID) {

    var user = null;

    try {
        $.ajax({
            url: "UsersGetByUserID/" + toolbox.session.sessionID + "/" + userID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    user = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersGetByUserID : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersGetByUserID : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return user;
}

engine.prototype.usersCreateDSL = function (toolbox, dslList, userID) {

    var user = null;

    try {
        $.ajax({
            url: "UsersCreateDSL/" + toolbox.session.sessionID + "/" + userID,
            //data: "bar",
            //dataType: "text",
            //data: JSON.stringify({ "dsl": JSON.stringify(dslList) }),
            data: JSON.stringify(dslList),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    user = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersCreateDSL : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersCreateDSL : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return user;
}

//----------------------------------------------
//-- users table functions
//----------------------------------------------
//--
//-- users table CRUD read
//--
engine.prototype.usersRead = function (toolbox, options) {

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isAdmin
        };

        $.ajax({
            url: "GetUsers" + "/" + toolbox.session.sessionID,
            data: parameters,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- users table CRUD create
//--
engine.prototype.usersCreate = function (toolbox, options) {

    try {
        $.ajax({
            url: "CreateUsers/" + toolbox.session.sessionID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersCreate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersCreate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- users table CRUD update
//--
engine.prototype.usersUpdate = function (toolbox, options) {

    try {
        $.ajax({
            url: "UpdateUsers/" + toolbox.session.sessionID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersUpdate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersUpdate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- users table CRUD destroy
//--
engine.prototype.usersDestroy = function (toolbox, options) {

    try {
        $.ajax({
            url: "DeleteUsers/" + toolbox.session.sessionID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in usersDestroy : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in usersDestroy : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//----------------------------------------------
