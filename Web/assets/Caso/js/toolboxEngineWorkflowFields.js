﻿//----------------------------------------------
//-- workflowFields functions
//----------------------------------------------
//--
//-- get all
//--
engine.prototype.workflowFieldsGetAll = function (toolbox, workflowID) {

    var workflowFields = null;

    try {
        $.ajax({
            url: "GetWorkflowFields/" + toolbox.session.sessionID + "/" + workflowID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    workflowFields = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsGetAll : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsGetAll : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return workflowFields;
}
//--
//--
//----------------------------------------------
//--
//-- workflowFields table CRUD read
//--
engine.prototype.workflowFieldsRead = function (toolbox, options, workflowID) {

    try {

        $.ajax({
            url: "GetWorkflowFields/" + toolbox.session.sessionID + "/" + workflowID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;                
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsRead : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsRead : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- workflowFields table CRUD create
//--
engine.prototype.workflowFieldsCreate = function (toolbox, options, workflowID) {

    try {
        $.ajax({
            url: "WorkflowFieldsPost/" + toolbox.session.sessionID + "/" + workflowID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsCreate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsCreate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- workflowFields table CRUD update
//--
engine.prototype.workflowFieldsUpdate = function (toolbox, options, workflowID) {

    try {
        $.ajax({
            url: "WorkflowFieldsPut/" + toolbox.session.sessionID + "/" + workflowID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "PUT",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsUpdate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error inworkflowFieldsUpdate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//-- workflowFields table CRUD destroy
//--
engine.prototype.workflowFieldsDestroy = function (toolbox, options, workflowID) {

    try {
        $.ajax({
            url: "WorkflowFieldsDelete/" + toolbox.session.sessionID + "/" + workflowID,
            data: JSON.stringify(options.data.models),
            dataType: "json",
            type: "DELETE",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    options.success(data);
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsDestroy : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowFieldsDestroy : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}
//--
//----------------------------------------------
