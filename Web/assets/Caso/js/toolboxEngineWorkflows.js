﻿//----------------------------------------------
//-- workflows table functions
//----------------------------------------------
//----------------------------------------------
//--

//-- workflow kickoff
engine.prototype.workflowGetKickoffData = function (toolbox, workflowID) {

    var workflowKickoffData = null;

    try {
        $.ajax({
            url: "WorkflowGetKickoffData/" + toolbox.session.sessionID + "/" + workflowID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    workflowKickoffData = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowGetKickoffData : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowGetKickoffData : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return workflowKickoffData;
}

engine.prototype.workflowSetKickoffData = function (toolbox, data) {

    var valid = true;

    try {
        $.ajax({
            url: "WorkflowSetKickoffData/" + toolbox.session.sessionID,
            data: data,
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in WorkflowSetKickoffData : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in WorkflowSetKickoffData : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}


engine.prototype.workflowsGetAll = function (toolbox) {

    var workflows = null;

    try {
        var parameters = {
            SessionID: toolbox.session.sessionID,
            IsAdmin: toolbox.session.isSystemAdmin
        };

        $.ajax({
            url: "GetWorkflows/" + toolbox.session.sessionID,
            data: parameters,
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    workflows = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowsGetAll : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowsGetAll : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return workflows;
}


engine.prototype.workflowsGetIntegrationTypes = function (toolbox) {

    var integrationTypes = null;

    try {
        $.ajax({
            url: "WorkflowsGetIntegrationTypes/" + toolbox.session.sessionID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    integrationTypes = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowsGetIntegrationTypes : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowsGetIntegrationTypes : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return integrationTypes;
}


engine.prototype.workflowCreate = function (toolbox, workflowName, integrationType, enabled, mobile, integrationSettings) {

    var workflowID = null;
    var data = {
        WorkflowName: workflowName,
        Enabled: enabled,
        Mobile: mobile,
        IntegrationType:integrationType,
        IntegrationSettings: integrationSettings
    };
    try {
        $.ajax({
            url: "WorkflowCreate/" + toolbox.session.sessionID,
            data: JSON.stringify(data),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    workflowID = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowCreate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowCreate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return workflowID;
}


engine.prototype.workflowUpdate = function (toolbox, workflowID, workflowName, enabled, mobile) {

    var data = {
        WorkflowName: workflowName,
        Enabled: enabled,
        Mobile:mobile
    };
    var valid = true;
    try {
        $.ajax({
            url: "WorkflowUpdate/" + toolbox.session.sessionID + "/" + workflowID,
            data: JSON.stringify(data),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                    valid = false;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowUpdate : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowUpdate : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}


engine.prototype.workflowGetByWorkflowID = function (toolbox, workflowID) {

    var workflow = null;

    try {
        $.ajax({
            url: "WorkflowGetByWorkflowID/" + toolbox.session.sessionID + "/" + workflowID,
            data: "",
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    workflow = data;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowGetByWorkflowID : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowGetByWorkflowID : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return workflow;
}


engine.prototype.workflowDeleteByWorkflowID = function (toolbox, workflowID) {

    var valid = false;

    try {
        $.ajax({
            url: "WorkflowDeleteByWorkflowID/" + toolbox.session.sessionID + "/" + workflowID,
            data: "",
            dataType: "json",
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    valid = true;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in workflowDeleteByWorkflowID : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in workflowDeleteByWorkflowID : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return valid;
}

