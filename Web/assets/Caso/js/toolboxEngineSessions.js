﻿//----------------------------------------------
//-- session object
//----------------------------------------------
//--
function session() {

    this.sessionID = '';
    this.userID = 0;
    this.username = '';
    this.fullName = '';
    this.userType = '';
    this.isSystemAdmin = false;
    this.isUserManager = false;
    this.isWorkflowManager = false;
    this.isImpersonationEnabled = false;
}

//----------------------------------------------
//-- validate session function
//----------------------------------------------
//--
engine.prototype.validSession = function (toolbox, sessionID) {

    var validSession = false;

    try {
        var parameters = { "sessionID": sessionID };

        $.ajax({
            type: "POST",
            url: "ValidateSession/" + sessionID,
            data: "", //JSON.stringify(parameters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    //-- persist sessionid locally
                    toolbox.session.sessionID = data.Session.SessionID;
                    toolbox.session.userID = data.Session.UserID;
                    toolbox.session.isSystemAdmin = data.Session.IsSystemAdmin;
                    toolbox.session.isUserManager = data.Session.IsUserManager;
                    toolbox.session.isWorkflowManager = data.Session.IsWorkflowManager;
                    toolbox.session.fullName = data.Session.FullName;
                    toolbox.session.isImpersonationEnabled = data.Session.ImpersonationEnabled;
                    toolbox.session.userType = data.Session.UserType;

                    validSession = true;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in ValidateSession : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in ValidateSession : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return validSession;
}
//--
//----------------------------------------------

//----------------------------------------------
//-- validate session function
//----------------------------------------------
//--
engine.prototype.pingSession = function (toolbox, sessionID) {

    var validSession = false;

    try {        
        $.ajax({
            type: "POST",
            url: "SessionKeepAlive/" + sessionID,
            data: "", //JSON.stringify(parameters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    //-- persist sessionid locally
                    toolbox.session.sessionID = data.Session.SessionID;
                    toolbox.session.userID = data.Session.UserID;
                    toolbox.session.isSystemAdmin = data.Session.IsSystemAdmin;
                    toolbox.session.isUserManager = data.Session.IsUserManager;
                    toolbox.session.isWorkflowManager = data.Session.IsWorkflowManager;
                    toolbox.session.fullName = data.Session.FullName;
                    toolbox.session.isImpersonationEnabled = data.Session.ImpersonationEnabled;
                    toolbox.session.userType = data.Session.UserType;

                    validSession = true;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in ValidateSession : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in ValidateSession : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return validSession;
}
//--
//----------------------------------------------




//----------------------------------------------
//-- autologin function
//----------------------------------------------
//--
engine.prototype.autoLogin = function (toolbox) {

    var validLogin = false;

    //-- try autologin
    try {
        $.ajax({
            type: "POST",
            url: "AutoLogin",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    //-- if info message then user didn't auto log in
                    if (data.Messages.InfoMessages.length > 0) {
                    }
                    else {
                        //-- persist sessionid locally
                        toolbox.session.sessionID = data.Session.SessionID;
                        validLogin = true;
                    }
                }
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in AutoLogin : " + textStatus + " : " + error, toolbox.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Invalid Username or Password.", toolbox.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return validLogin;
}
//--
//----------------------------------------------


//----------------------------------------------
//-- login function
//----------------------------------------------
//--
engine.prototype.login = function (toolbox, userName, password) {

    var validLogin = false;

    //-- try login
    try {
        $.ajax({
            type: "POST",
            url: "Login/" + userName + "/" + password,
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    //-- persist sessionid locally
                    toolbox.session.sessionID = data.Session.SessionID;

                    validLogin = true;
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Invalid Username or Password.", toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in Login : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return validLogin;
}
//--
//----------------------------------------------


//----------------------------------------------
//-- logout function
//----------------------------------------------
//--
engine.prototype.logout = function (toolbox) {

    var validLogin = false;

    //-- try login
    try {
        $.ajax({
            type: "POST",
            url: "Logout/" + toolbox.session.sessionID,
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    //-- valid logout
                }
                toolbox.loader.notifier.messages = data.Messages;
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in Login : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in Login : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return validLogin;
}
//--
//----------------------------------------------


engine.prototype.impersonate = function (toolbox, userid) {
    //ImpersonateUser/{sessionID}/{userID}
    var validLogin = false;

    try {
        $.ajax({
            type: "POST",
            url: "ImpersonateUser/" + toolbox.session.sessionID + "/" + userid,
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                }
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in Impersonation : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in Impersonation : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return validLogin;
}

engine.prototype.getClientSettings = function (toolbox) {
    //ImpersonateUser/{sessionID}/{userID}
    var clientSettings = null;

    try {
        $.ajax({
            type: "GET",
            url: "GetClientSettings/" + toolbox.session.sessionID,
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                    try { clientSettings = JSON.parse(data.Data); } catch (excpt) { }
                }
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in Getting Client Settings : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in Getting Client Settings : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }

    return clientSettings;
}

engine.prototype.saveClientSettings = function (toolbox, settings) {    
    try {
        $.ajax({
            type: "POST",
            url: "SaveClientSettings/" + toolbox.session.sessionID,
            data: JSON.stringify({Settings:JSON.stringify(settings)}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.Messages.ErrorMessages.length > 0) {
                }
                else {
                }
            },
            error: function (xhr, textStatus, error) {
                toolbox.loader.notifier.showAlert("Error", "Error in Saving Client Settings : " + textStatus + " : " + error, toolbox.loader.notifier.notificationType.error);
            }
        });
    }
    catch (e) {
        toolbox.loader.notifier.showAlert("Error", "Error in Saving Client Settings : " + e.message, toolbox.loader.notifier.notificationType.error);
    }
    finally {
        toolbox.loader.hideWaitDiv("body");
    }
}